/**
 * This file is part of Special K.
 *
 * Special K is free software : you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by The Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Special K is distributed in the hope that it will be useful,
 *
 * But WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Special K.
 *
 *   If not, see <http://www.gnu.org/licenses/>.
 *
**/

#define _CRT_SECURE_NO_WARNINGS
#define NOMINMAX

#include <Windows.h>

#include <SpecialK/diagnostics/compatibility.h>

#include <SpecialK/core.h>
#include <SpecialK/hooks.h>
#include <SpecialK/command.h>
#include <SpecialK/config.h>
#include <SpecialK/dxgi_backend.h>
#include <SpecialK/render_backend.h>
#include <SpecialK/log.h>
#include <SpecialK/utility.h>

extern LARGE_INTEGER SK_QueryPerf (void);
#include <SpecialK/framerate.h>

#include <atlbase.h>

#include <d3d11.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>

#include <algorithm>


// For texture caching to work correctly ...
//   DarkSouls3 seems to underflow references on occasion!!!
#define DS3_REF_TWEAK

namespace SK
{
  namespace DXGI
  {
    struct PipelineStatsD3D11
    {
      struct StatQueryD3D11  
      {
        ID3D11Query* async  = nullptr;
        bool         active = false;
      } query;

      D3D11_QUERY_DATA_PIPELINE_STATISTICS
                 last_results = { 0 };
    } pipeline_stats_d3d11;
  };
};

typedef HRESULT (WINAPI *D3D11CreateDevice_pfn)(
  _In_opt_                            IDXGIAdapter         *pAdapter,
                                      D3D_DRIVER_TYPE       DriverType,
                                      HMODULE               Software,
                                      UINT                  Flags,
  _In_opt_                      const D3D_FEATURE_LEVEL    *pFeatureLevels,
                                      UINT                  FeatureLevels,
                                      UINT                  SDKVersion,
  _Out_opt_                           ID3D11Device        **ppDevice,
  _Out_opt_                           D3D_FEATURE_LEVEL    *pFeatureLevel,
  _Out_opt_                           ID3D11DeviceContext **ppImmediateContext);

typedef HRESULT (WINAPI *D3D11CreateDeviceAndSwapChain_pfn)(
  _In_opt_                             IDXGIAdapter*,
                                       D3D_DRIVER_TYPE,
                                       HMODULE,
                                       UINT,
  _In_reads_opt_ (FeatureLevels) CONST D3D_FEATURE_LEVEL*,
                                       UINT FeatureLevels,
                                       UINT,
  _In_opt_                       CONST DXGI_SWAP_CHAIN_DESC*,
  _Out_opt_                            IDXGISwapChain**,
  _Out_opt_                            ID3D11Device**,
  _Out_opt_                            D3D_FEATURE_LEVEL*,
  _Out_opt_                            ID3D11DeviceContext**);


extern void WaitForInitDXGI (void);

HMODULE SK::DXGI::hModD3D11 = 0;

volatile LONG SK_D3D11_tex_init = FALSE;
volatile LONG  __d3d11_ready    = FALSE;

void WaitForInitD3D11 (void)
{
  while (! InterlockedCompareExchange (&__d3d11_ready, FALSE, FALSE))
    Sleep (config.system.init_delay);
}


typedef enum D3DX11_IMAGE_FILE_FORMAT {
  D3DX11_IFF_BMP          = 0,
  D3DX11_IFF_JPG          = 1,
  D3DX11_IFF_PNG          = 3,
  D3DX11_IFF_DDS          = 4,
  D3DX11_IFF_TIFF         = 10,
  D3DX11_IFF_GIF          = 11,
  D3DX11_IFF_WMP          = 12,
  D3DX11_IFF_FORCE_DWORD  = 0x7fffffff
} D3DX11_IMAGE_FILE_FORMAT, *LPD3DX11_IMAGE_FILE_FORMAT;

typedef struct D3DX11_IMAGE_INFO {
  UINT                     Width;
  UINT                     Height;
  UINT                     Depth;
  UINT                     ArraySize;
  UINT                     MipLevels;
  UINT                     MiscFlags;
  DXGI_FORMAT              Format;
  D3D11_RESOURCE_DIMENSION ResourceDimension;
  D3DX11_IMAGE_FILE_FORMAT ImageFileFormat;
} D3DX11_IMAGE_INFO, *LPD3DX11_IMAGE_INFO;


typedef struct D3DX11_IMAGE_LOAD_INFO {
  UINT              Width;
  UINT              Height;
  UINT              Depth;
  UINT              FirstMipLevel;
  UINT              MipLevels;
  D3D11_USAGE       Usage;
  UINT              BindFlags;
  UINT              CpuAccessFlags;
  UINT              MiscFlags;
  DXGI_FORMAT       Format;
  UINT              Filter;
  UINT              MipFilter;
  D3DX11_IMAGE_INFO *pSrcInfo;
} D3DX11_IMAGE_LOAD_INFO, *LPD3DX11_IMAGE_LOAD_INFO;

typedef HRESULT (WINAPI *D3DX11CreateTextureFromFileW_pfn)(
  _In_  ID3D11Device           *pDevice,
  _In_  LPCWSTR                pSrcFile,
  _In_  D3DX11_IMAGE_LOAD_INFO *pLoadInfo,
  _In_  IUnknown               *pPump,
  _Out_ ID3D11Resource         **ppTexture,
  _Out_ HRESULT                *pHResult
);

interface ID3DX11ThreadPump;

typedef HRESULT (WINAPI *D3DX11GetImageInfoFromFileW_pfn)(
  _In_  LPCWSTR           pSrcFile,
  _In_  ID3DX11ThreadPump *pPump,
  _In_  D3DX11_IMAGE_INFO *pSrcInfo,
  _Out_ HRESULT           *pHResult
);

typedef void (WINAPI *D3D11_UpdateSubresource1_pfn)(
  _In_           ID3D11DeviceContext1 *This,
  _In_           ID3D11Resource       *pDstResource,
  _In_           UINT                  DstSubresource,
  _In_opt_ const D3D11_BOX            *pDstBox,
  _In_     const void                 *pSrcData,
  _In_           UINT                  SrcRowPitch,
  _In_           UINT                  SrcDepthPitch,
  _In_           UINT                  CopyFlags
);


void  __stdcall SK_D3D11_TexCacheCheckpoint    ( void);
bool  __stdcall SK_D3D11_TextureIsCached       ( ID3D11Texture2D*     pTex );
void  __stdcall SK_D3D11_UseTexture            ( ID3D11Texture2D*     pTex );
void  __stdcall SK_D3D11_RemoveTexFromCache    ( ID3D11Texture2D*     pTex );

void  __stdcall SK_D3D11_UpdateRenderStats     ( IDXGISwapChain*      pSwapChain );

bool __stdcall SK_D3D11_TextureIsCached    (ID3D11Texture2D* pTex);
void __stdcall SK_D3D11_RemoveTexFromCache (ID3D11Texture2D* pTex);

//#define FULL_RESOLUTION

D3DX11CreateTextureFromFileW_pfn D3DX11CreateTextureFromFileW = nullptr;
D3DX11GetImageInfoFromFileW_pfn  D3DX11GetImageInfoFromFileW  = nullptr;
HMODULE                          hModD3DX11_43                = nullptr;

#ifdef NO_TLS
std::set <DWORD> texinject_tids;
CRITICAL_SECTION cs_texinject;
#else
#include <SpecialK/tls.h>
#endif

bool SK_D3D11_IsTexInjectThread (DWORD dwThreadId = GetCurrentThreadId ())
{
#ifdef NO_TLS
  bool bRet = false;

  EnterCriticalSection (&cs_texinject);
  bRet = (texinject_tids.count (dwThreadId) > 0);
  LeaveCriticalSection (&cs_texinject);

  return bRet;
#else
  UNREFERENCED_PARAMETER (dwThreadId);

  SK_TLS* pTLS = SK_GetTLS ();

  if (pTLS != nullptr)
    return pTLS->d3d11.texinject_thread;
  else {
    dll_log.Log (L"[ SpecialK ] >> Thread-Local Storage is BORKED! <<");
    return false;
  }
#endif
}

void
SK_D3D11_ClearTexInjectThread ( DWORD dwThreadId = GetCurrentThreadId () )
{
#ifdef NO_TLS
  EnterCriticalSection (&cs_texinject);
  texinject_tids.erase (dwThreadId);
  LeaveCriticalSection (&cs_texinject);
#else
  UNREFERENCED_PARAMETER (dwThreadId);

  SK_TLS* pTLS = SK_GetTLS ();

  if (pTLS != nullptr)
    pTLS->d3d11.texinject_thread = false;
  else
    dll_log.Log (L"[ SpecialK ] >> Thread-Local Storage is BORKED! <<");
#endif
}

void
SK_D3D11_SetTexInjectThread ( DWORD dwThreadId = GetCurrentThreadId () )
{
#ifdef NO_TLS
  EnterCriticalSection (&cs_texinject);
  texinject_tids.insert (dwThreadId);
  LeaveCriticalSection (&cs_texinject);
#else
  UNREFERENCED_PARAMETER (dwThreadId);

  SK_TLS* pTLS = SK_GetTLS ();

  if (pTLS != nullptr)
    pTLS->d3d11.texinject_thread = true;
  else
    dll_log.Log (L"[ SpecialK ] >> Thread-Local Storage is BORKED! <<");
#endif
}

typedef ULONG (WINAPI *IUnknown_Release_pfn) (IUnknown* This);
typedef ULONG (WINAPI *IUnknown_AddRef_pfn)  (IUnknown* This);

IUnknown_Release_pfn IUnknown_Release_Original = nullptr;
IUnknown_AddRef_pfn  IUnknown_AddRef_Original  = nullptr;

__declspec (noinline,nothrow)
ULONG
WINAPI
IUnknown_Release (IUnknown* This)
{
  if (! SK_D3D11_IsTexInjectThread ())
  {
    ID3D11Texture2D* pTex = nullptr;
    if (SUCCEEDED (This->QueryInterface (IID_PPV_ARGS (&pTex))))
    {
      ULONG count = IUnknown_Release_Original (pTex);

      // If count is == 0, something's screwy
      if (pTex != nullptr && count <= 1)
        SK_D3D11_RemoveTexFromCache (pTex);
    }
  }

  return IUnknown_Release_Original (This);
}

__declspec (noinline,nothrow)
ULONG
WINAPI
IUnknown_AddRef (IUnknown* This)
{
  if (! SK_D3D11_IsTexInjectThread ())
  {
    ID3D11Texture2D* pTex = (ID3D11Texture2D *)This;//nullptr;

    // This would cause the damn thing to recurse infinitely...
    //if (SUCCEEDED (This->QueryInterface (IID_PPV_ARGS (&pTex)))) {
      if (pTex != nullptr && SK_D3D11_TextureIsCached (pTex))
        SK_D3D11_UseTexture (pTex);
    //}
  }

  return IUnknown_AddRef_Original (This);
}

ID3D11Device* g_pD3D11Dev = nullptr;

unsigned int __stdcall HookD3D11 (LPVOID user);

struct d3d11_caps_t {
  struct {
    bool d3d11_1         = false;
  } feature_level;
} d3d11_caps;

volatile D3D11CreateDeviceAndSwapChain_pfn D3D11CreateDeviceAndSwapChain_Import = nullptr;
volatile D3D11CreateDevice_pfn             D3D11CreateDevice_Import             = nullptr;

void
SK_D3D11_SetDevice ( ID3D11Device           **ppDevice,
                     D3D_FEATURE_LEVEL        FeatureLevel )
{
  if ( ppDevice != nullptr )
  {
    if ( *ppDevice != g_pD3D11Dev )
    {
      dll_log.Log ( L"[  D3D 11  ] >> Device = %ph (Feature Level:%s)",
                      *ppDevice,
                        SK_DXGI_FeatureLevelsToStr ( 1,
                                                      (DWORD *)&FeatureLevel
                                                   ).c_str ()
                  );
      g_pD3D11Dev = *ppDevice;
    }

    if (config.render.dxgi.exception_mode != -1)
      g_pD3D11Dev->SetExceptionMode (config.render.dxgi.exception_mode);

    CComPtr <IDXGIDevice>  pDXGIDev = nullptr;
    CComPtr <IDXGIAdapter> pAdapter = nullptr;

    HRESULT hr =
      (*ppDevice)->QueryInterface ( IID_PPV_ARGS (&pDXGIDev) );

    if ( SUCCEEDED ( hr ) )
    {
      hr =
        pDXGIDev->GetParent ( IID_PPV_ARGS (&pAdapter) );

      if ( SUCCEEDED ( hr ) )
      {
        if ( pAdapter == nullptr )
          return;

        const int iver =
          SK_GetDXGIAdapterInterfaceVer ( pAdapter );

        // IDXGIAdapter3 = DXGI 1.4 (Windows 10+)
        if ( iver >= 3 )
        {
          SK::DXGI::StartBudgetThread ( &pAdapter );
        }
      }
    }
  }
}

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11CreateDeviceAndSwapChain_Detour (IDXGIAdapter          *pAdapter,
                                      D3D_DRIVER_TYPE        DriverType,
                                      HMODULE                Software,
                                      UINT                   Flags,
 _In_reads_opt_ (FeatureLevels) CONST D3D_FEATURE_LEVEL     *pFeatureLevels,
                                      UINT                   FeatureLevels,
                                      UINT                   SDKVersion,
 _In_opt_                       CONST DXGI_SWAP_CHAIN_DESC  *pSwapChainDesc,
 _Out_opt_                            IDXGISwapChain       **ppSwapChain,
 _Out_opt_                            ID3D11Device         **ppDevice,
 _Out_opt_                            D3D_FEATURE_LEVEL     *pFeatureLevel,
 _Out_opt_                            ID3D11DeviceContext  **ppImmediateContext)
{
  // Even if the game doesn't care about the feature level, we do.
  D3D_FEATURE_LEVEL ret_level  = D3D_FEATURE_LEVEL_11_1;
  ID3D11Device*     ret_device = nullptr;

  // Allow override of swapchain parameters
  DXGI_SWAP_CHAIN_DESC* swap_chain_desc     = (DXGI_SWAP_CHAIN_DESC *)pSwapChainDesc;
  DXGI_SWAP_CHAIN_DESC  swap_chain_override = { 0 };

  DXGI_LOG_CALL_0 (L"D3D11CreateDeviceAndSwapChain");

  dll_log.LogEx ( true,
                    L"[  D3D 11  ]  <~> Preferred Feature Level(s): <%u> - %s\n",
                      FeatureLevels,
                        SK_DXGI_FeatureLevelsToStr (
                          FeatureLevels,
                            (DWORD *)pFeatureLevels
                        ).c_str ()
                );

  // Optionally Enable Debug Layer
  if (InterlockedAdd (&__d3d11_ready, 0))
  {
    if (config.render.dxgi.debug_layer && (! (Flags & D3D11_CREATE_DEVICE_DEBUG)))
    {
      SK_LOG0 ( ( L" ==> Enabling D3D11 Debug layer" ),
                  L"  D3D 11  " );
      Flags |= D3D11_CREATE_DEVICE_DEBUG;
    }
  }

  else
  {
    if (SK_GetCallingDLL () != SK_GetDLL ())
      WaitForInitDXGI ();
  }

  //
  // DXGI Adapter Override (for performance)
  //

  SK_DXGI_AdapterOverride ( &pAdapter, &DriverType );

  if (swap_chain_desc != nullptr)
  {
    dll_log.LogEx ( true,
                      L"[   DXGI   ]  SwapChain: (%lux%lu@%4.1f Hz - Scaling: %s) - "
                      L"[%lu Buffers] :: Flags=0x%04X, SwapEffect: %s\n",
                        swap_chain_desc->BufferDesc.Width,
                        swap_chain_desc->BufferDesc.Height,
                        swap_chain_desc->BufferDesc.RefreshRate.Denominator > 0 ?
                 (float)swap_chain_desc->BufferDesc.RefreshRate.Numerator /
                 (float)swap_chain_desc->BufferDesc.RefreshRate.Denominator :
                 (float)swap_chain_desc->BufferDesc.RefreshRate.Numerator,
                        swap_chain_desc->BufferDesc.Scaling == DXGI_MODE_SCALING_UNSPECIFIED ?
                          L"Unspecified" :
                          swap_chain_desc->BufferDesc.Scaling == DXGI_MODE_SCALING_CENTERED  ?
                            L"Centered" :
                            L"Stretched",
                        swap_chain_desc->BufferCount,
                        swap_chain_desc->Flags,
                        swap_chain_desc->SwapEffect == 0         ?
                          L"Discard" :
                          swap_chain_desc->SwapEffect == 1       ?
                            L"Sequential" :
                            swap_chain_desc->SwapEffect == 2     ?
                              L"<Unknown>" :
                              swap_chain_desc->SwapEffect == 3   ?
                                L"Flip Sequential" :
                                swap_chain_desc->SwapEffect == 4 ?
                                  L"Flip Discard" :
                                  L"<Unknown>");

    swap_chain_override = *swap_chain_desc;
    swap_chain_desc     = &swap_chain_override;

    if ( config.render.dxgi.scaling_mode      != -1 &&
          swap_chain_desc->BufferDesc.Scaling !=
            (DXGI_MODE_SCALING)config.render.dxgi.scaling_mode )
    {
      dll_log.Log ( L"[  D3D 11  ]  >> Scaling Override "
                    L"(Requested: %s, Using: %s)",
                      SK_DXGI_DescribeScalingMode (
                        swap_chain_desc->BufferDesc.Scaling
                      ),
                        SK_DXGI_DescribeScalingMode (
                          (DXGI_MODE_SCALING)config.render.dxgi.scaling_mode
                        )
                  );

      swap_chain_desc->BufferDesc.Scaling =
        (DXGI_MODE_SCALING)config.render.dxgi.scaling_mode;
    }

    if (! config.window.res.override.isZero ())
    {
      swap_chain_desc->BufferDesc.Width  = config.window.res.override.x;
      swap_chain_desc->BufferDesc.Height = config.window.res.override.y;
    }

    else
    {
      SK_DXGI_BorderCompensation (
        swap_chain_desc->BufferDesc.Width,
          swap_chain_desc->BufferDesc.Height
      );
    }
  }

  HRESULT res;

  DXGI_CALL(res, 
    D3D11CreateDeviceAndSwapChain_Import (pAdapter,
                                          DriverType,
                                          Software,
                                          Flags,
                                          pFeatureLevels,
                                          FeatureLevels,
                                          SDKVersion,
                                          swap_chain_desc,
                                          ppSwapChain,
                                          &ret_device,
                                          &ret_level,
                                          ppImmediateContext));

  if (SUCCEEDED (res))
  {
    if (swap_chain_desc != nullptr)
    {
      if ( dwRenderThread == 0x00 ||
           dwRenderThread == GetCurrentThreadId () )
      {
        if ( hWndRender                   != 0 &&
             swap_chain_desc->OutputWindow != 0 &&
             swap_chain_desc->OutputWindow != hWndRender )
          dll_log.Log (L"[  D3D 11  ] Game created a new window?!");

        //if (hWndRender == nullptr || (! IsWindow (hWndRender))) {
          //hWndRender       = swap_chain_desc->OutputWindow;
          //game_window.hWnd = hWndRender;
        //}
      }
    }

    // Assume the first thing to create a D3D11 render device is
    //   the game and that devices never migrate threads; for most games
    //     this assumption holds.
    if ( dwRenderThread == 0x00 ||
         dwRenderThread == GetCurrentThreadId () ) {
      dwRenderThread = GetCurrentThreadId ();
    }

    SK_D3D11_SetDevice ( &ret_device, ret_level );
  }

  if (ppDevice != nullptr)
    *ppDevice = ret_device;

  if (pFeatureLevel != nullptr)
    *pFeatureLevel = ret_level;

  return res;
}

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11CreateDevice_Detour (
  _In_opt_                            IDXGIAdapter         *pAdapter,
                                      D3D_DRIVER_TYPE       DriverType,
                                      HMODULE               Software,
                                      UINT                  Flags,
  _In_opt_                      const D3D_FEATURE_LEVEL    *pFeatureLevels,
                                      UINT                  FeatureLevels,
                                      UINT                  SDKVersion,
  _Out_opt_                           ID3D11Device        **ppDevice,
  _Out_opt_                           D3D_FEATURE_LEVEL    *pFeatureLevel,
  _Out_opt_                           ID3D11DeviceContext **ppImmediateContext)
{
  DXGI_LOG_CALL_0 (L"D3D11CreateDevice");

  return D3D11CreateDeviceAndSwapChain_Detour (pAdapter, DriverType, Software, Flags, pFeatureLevels, FeatureLevels, SDKVersion, nullptr, nullptr, ppDevice, pFeatureLevel, ppImmediateContext);
}


__declspec (noinline)
HRESULT
WINAPI
D3D11Dev_CreateTexture2D_Override (
_In_            ID3D11Device           *This,
_In_      const D3D11_TEXTURE2D_DESC   *pDesc,
_In_opt_  const D3D11_SUBRESOURCE_DATA *pInitialData,
_Out_opt_       ID3D11Texture2D        **ppTexture2D );

D3D11Dev_CreateBuffer_pfn              D3D11Dev_CreateBuffer_Original              = nullptr;
D3D11Dev_CreateTexture2D_pfn           D3D11Dev_CreateTexture2D_Original           = nullptr;
D3D11Dev_CreateRenderTargetView_pfn    D3D11Dev_CreateRenderTargetView_Original    = nullptr;
D3D11Dev_CreateShaderResourceView_pfn  D3D11Dev_CreateShaderResourceView_Original  = nullptr;

D3D11Dev_CreateVertexShader_pfn        D3D11Dev_CreateVertexShader_Original        = nullptr;
D3D11Dev_CreatePixelShader_pfn         D3D11Dev_CreatePixelShader_Original         = nullptr;
D3D11Dev_CreateGeometryShader_pfn      D3D11Dev_CreateGeometryShader_Original      = nullptr;
D3D11Dev_CreateHullShader_pfn          D3D11Dev_CreateHullShader_Original          = nullptr;
D3D11Dev_CreateDomainShader_pfn        D3D11Dev_CreateDomainShader_Original        = nullptr;
D3D11Dev_CreateComputeShader_pfn       D3D11Dev_CreateComputeShader_Original       = nullptr;

D3D11_RSSetScissorRects_pfn            D3D11_RSSetScissorRects_Original            = nullptr;
D3D11_RSSetViewports_pfn               D3D11_RSSetViewports_Original               = nullptr;
D3D11_VSSetConstantBuffers_pfn         D3D11_VSSetConstantBuffers_Original         = nullptr;
D3D11_VSSetShaderResources_pfn         D3D11_VSSetShaderResources_Original         = nullptr;
D3D11_PSSetShaderResources_pfn         D3D11_PSSetShaderResources_Original         = nullptr;
D3D11_GSSetShaderResources_pfn         D3D11_GSSetShaderResources_Original         = nullptr;
D3D11_HSSetShaderResources_pfn         D3D11_HSSetShaderResources_Original         = nullptr;
D3D11_DSSetShaderResources_pfn         D3D11_DSSetShaderResources_Original         = nullptr;
D3D11_CSSetShaderResources_pfn         D3D11_CSSetShaderResources_Original         = nullptr;
D3D11_UpdateSubresource_pfn            D3D11_UpdateSubresource_Original            = nullptr;
D3D11_DrawIndexed_pfn                  D3D11_DrawIndexed_Original                  = nullptr;
D3D11_Draw_pfn                         D3D11_Draw_Original                         = nullptr;
D3D11_DrawAuto_pfn                     D3D11_DrawAuto_Original                     = nullptr;
D3D11_DrawIndexedInstanced_pfn         D3D11_DrawIndexedInstanced_Original         = nullptr;
D3D11_DrawIndexedInstancedIndirect_pfn D3D11_DrawIndexedInstancedIndirect_Original = nullptr;
D3D11_DrawInstanced_pfn                D3D11_DrawInstanced_Original                = nullptr;
D3D11_DrawInstancedIndirect_pfn        D3D11_DrawInstancedIndirect_Original        = nullptr;
D3D11_Dispatch_pfn                     D3D11_Dispatch_Original                     = nullptr;
D3D11_DispatchIndirect_pfn             D3D11_DispatchIndirect_Original             = nullptr;
D3D11_Map_pfn                          D3D11_Map_Original                          = nullptr;

D3D11_VSSetShader_pfn                  D3D11_VSSetShader_Original                  = nullptr;
D3D11_PSSetShader_pfn                  D3D11_PSSetShader_Original                  = nullptr;
D3D11_GSSetShader_pfn                  D3D11_GSSetShader_Original                  = nullptr;
D3D11_HSSetShader_pfn                  D3D11_HSSetShader_Original                  = nullptr;
D3D11_DSSetShader_pfn                  D3D11_DSSetShader_Original                  = nullptr;
D3D11_CSSetShader_pfn                  D3D11_CSSetShader_Original                  = nullptr;

D3D11_CopyResource_pfn          D3D11_CopyResource_Original       = nullptr;
D3D11_UpdateSubresource1_pfn    D3D11_UpdateSubresource1_Original = nullptr;

__declspec (noinline,nothrow)
HRESULT
STDMETHODCALLTYPE
D3D11Dev_CreateRenderTargetView_Override (
  _In_            ID3D11Device                   *This,
  _In_            ID3D11Resource                 *pResource,
  _In_opt_  const D3D11_RENDER_TARGET_VIEW_DESC  *pDesc,
  _Out_opt_       ID3D11RenderTargetView        **ppRTView )
{
  return D3D11Dev_CreateRenderTargetView_Original (
           This, pResource,
             pDesc, ppRTView );
}

enum class SK_D3D11_ShaderType {
  Vertex   =  1,
  Pixel    =  2,
  Geometry =  4,
  Domain   =  8,
  Hull     = 16,
  Compute  = 32,

  Invalid  = MAXINT
};

uint32_t current_vs = 0x0;
uint32_t current_ps = 0x0;
uint32_t current_gs = 0x0;
uint32_t current_hs = 0x0;
uint32_t current_ds = 0x0;
uint32_t current_cs = 0x0;

std::unordered_set <uint32_t> SK_D3D11_Blacklist_VS;
std::unordered_set <uint32_t> SK_D3D11_Blacklist_PS;
std::unordered_set <uint32_t> SK_D3D11_Blacklist_GS;
std::unordered_set <uint32_t> SK_D3D11_Blacklist_HS;
std::unordered_set <uint32_t> SK_D3D11_Blacklist_DS;
std::unordered_set <uint32_t> SK_D3D11_Blacklist_CS;

struct SK_D3D11_ShaderDesc
{
  SK_D3D11_ShaderType type   = SK_D3D11_ShaderType::Invalid;
  uint32_t            crc32c = 0;

  std::vector <BYTE>  bytecode;

  struct
  {
    uint32_t   last_frame;
    __time64_t last_time;
    ULONG      refs  = 0;
  } usage;
};

#include <map>

struct SK_D3D11_KnownShaders
{

  template <typename _T> 
  struct ShaderRegistry
  {
    std::multimap      <uint32_t,                 _T*>    all;
    std::unordered_map <uint32_t,                 _T*>    newest;

    std::unordered_map <uint32_t, SK_D3D11_ShaderDesc>    descs;

  //std::unordered_map <UINT, ID3D11ShaderResourceView *> resources;
    ULONG                                                 changes_last_frame;
  };

  ShaderRegistry <ID3D11PixelShader>    pixel;
  ShaderRegistry <ID3D11VertexShader>   vertex;
  ShaderRegistry <ID3D11GeometryShader> geometry;
  ShaderRegistry <ID3D11HullShader>     hull;
  ShaderRegistry <ID3D11DomainShader>   domain;
  ShaderRegistry <ID3D11ComputeShader>  compute;

} SK_D3D11_Shaders;


struct shader_tracking_s
{
  void clear (void)
  {
    //active    = false;

    num_draws = 0;

    for ( auto it : used_views )
      it->Release ();

    used_views.clear ();

    //used_textures.clear ();

    //for (int i = 0; i < 16; i++)
      //current_textures [i] = 0x00;
  }

  void use (IUnknown* pShader);

  uint32_t                      crc32c       =  0x00;
  bool                          cancel_draws = false;
  bool                          active       = false;
  int                           num_draws    =     0;

  // The slot used has meaning, but I think we can ignore it for now...
  //std::unordered_map <UINT, ID3D11ShaderResourceView *> used_views;

  std::unordered_set <ID3D11ShaderResourceView *> used_views;

//  std::unordered_set <uint32_t>    used_textures;
//                      uint32_t  current_textures [16];

  //std::vector <IDirect3DBaseTexture9 *> samplers;

  IUnknown*                     shader_obj  = nullptr;
//  ID3DXConstantTable*           ctable      = nullptr;

//  struct shader_constant_s
//  {
//    char                Name [128];
//    D3DXREGISTER_SET    RegisterSet;
//    UINT                RegisterIndex;
//    UINT                RegisterCount;
//    D3DXPARAMETER_CLASS Class;
//    D3DXPARAMETER_TYPE  Type;
//    UINT                Rows;
//    UINT                Columns;
//    UINT                Elements;
//    std::vector <shader_constant_s>
//                        struct_members;
//    bool                Override;
//    float               Data [4]; // TEMP HACK
//  };

//  std::vector <shader_constant_s> constants;
} tracked_vs, tracked_ps, tracked_gs,
  tracked_hs, tracked_ds, tracked_cs;

void
shader_tracking_s::use (IUnknown* pShader)
{
  ++num_draws;
}


class iSK_PixelShaderD3D11 : public
      ID3D11PixelShader
{
public:
    iSK_PixelShaderD3D11 (       ID3D11PixelShader  **ppShader,
                                 ID3D11ClassLinkage  *pLinkage,
                           const uint8_t             *pShaderBytecode,
                                 SIZE_T               BytecodeLength )
    {
      SK_D3D11_ShaderDesc desc;

      desc.type   = SK_D3D11_ShaderType::Pixel;
      desc.crc32c = crc32c (0, (const uint8_t *)pShaderBytecode, BytecodeLength);

      if (SK_D3D11_Shaders.pixel.descs.count (desc.crc32c))
      {
        rDesc = SK_D3D11_Shaders.pixel.descs [desc.crc32c];
        SK_D3D11_Shaders.pixel.descs [desc.crc32c].usage.refs++;

        SK_D3D11_Shaders.pixel.all.emplace    (std::make_pair (rDesc.crc32c, this));

        SK_D3D11_Shaders.pixel.newest.erase   (rDesc.crc32c);
        SK_D3D11_Shaders.pixel.newest.emplace (std::make_pair (rDesc.crc32c, this));
      } 

      else
      {
        desc.usage.last_frame = SK_GetFramesDrawn ();
        _time64 (&desc.usage.last_time);

        SK_D3D11_Shaders.pixel.all.emplace    (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.pixel.newest.emplace (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.pixel.descs.insert   (std::make_pair (desc.crc32c, desc));

        for (UINT i = 0; i < BytecodeLength; i++)
          SK_D3D11_Shaders.pixel.descs [desc.crc32c].bytecode.push_back (pShaderBytecode [i]);

        rDesc = SK_D3D11_Shaders.pixel.descs [desc.crc32c];
      }

      InterlockedExchange  (&refs,          1);
      InterlockedIncrement (&rDesc.usage.refs);

      pShader   = *ppShader;
      *ppShader = this;
    };


    /*** IUnknown methods ***/
    STDMETHOD (QueryInterface) (THIS_ REFIID riid, void** ppvObj)
    {
      if ( IsEqualGUID (riid, IID_SKPixelShaderD3D11) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }
      
      if ( IsEqualGUID (riid, IID_IUnknown)           ||
           IsEqualGUID (riid, IID_ID3D11PixelShader)  ||
           IsEqualGUID (riid, IID_ID3D11DeviceChild) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }

      return pShader->QueryInterface (riid, ppvObj);
    }

    STDMETHOD_ (ULONG,AddRef) (THIS)
    {
      ULONG ret = InterlockedIncrement (&refs);

      pShader->AddRef ();

      //can_free = false;

      return ret;
    }

    STDMETHOD_ (ULONG,Release) (THIS)
    {
      ULONG ret = pShader->Release ();

      if (ret == 0)
      {
        for ( auto it  = SK_D3D11_Shaders.pixel.all.begin ();
                   it != SK_D3D11_Shaders.pixel.all.end   ();
                 ++it )
        {
          if (it->second == this)
          {
            if (SK_D3D11_Shaders.pixel.newest [rDesc.crc32c] == this)
            {
              auto it_next = it; ++it_next;
              auto it_prev = it; --it_prev;

              SK_D3D11_Shaders.pixel.newest.erase (it->first);

              if (it_next != SK_D3D11_Shaders.pixel.all.end ())
              {
                SK_D3D11_Shaders.pixel.newest.emplace (std::make_pair (it_next->first, it_next->second));
              }

              else if (it_prev != SK_D3D11_Shaders.pixel.all.end ())
              {
                SK_D3D11_Shaders.pixel.newest.emplace (std::make_pair (it_prev->first, it_prev->second));
              }
            }

            SK_D3D11_Shaders.pixel.all.erase (it);
            break;
          }
        }

        SK_D3D11_Shaders.pixel.descs [rDesc.crc32c].usage.refs--;
      }

      return ret;
    }

    /*** ID3D11DeviceChild methods ***/
    STDMETHOD_ (void,GetDevice) (_Out_ ID3D11Device **ppDevice)
    {
      return pShader->GetDevice (ppDevice);
    }

    STDMETHOD (GetPrivateData) (
         _In_                                 REFGUID  guid,
         _Inout_                              UINT    *pDataSize,
         _Out_writes_bytes_opt_( *pDataSize ) void    *pData )
    {
      return pShader->GetPrivateData (guid, pDataSize, pData);
    }
        
    STDMETHOD (SetPrivateData) (
         _In_                                   REFGUID  guid,
         _In_                                   UINT     DataSize,
         _In_reads_bytes_opt_( DataSize ) const void    *pData )
    {
      return pShader->SetPrivateData (guid, DataSize, pData);
    }

    STDMETHOD (SetPrivateDataInterface) ( _In_           REFGUID   guid,
                                          _In_opt_ const IUnknown *pData )
    {
      return pShader->SetPrivateDataInterface (guid, pData);
    }


    SK_D3D11_ShaderDesc& getDesc       (void) { return rDesc;   }
    ID3D11PixelShader*   getRealShader (void) { return pShader; }

protected:
private:
           SK_D3D11_ShaderDesc rDesc;

           ID3D11PixelShader*   pShader = nullptr;
  volatile ULONG                refs    = 0;
};

class iSK_VertexShaderD3D11 : public
      ID3D11VertexShader
{
public:
    iSK_VertexShaderD3D11 (      ID3D11VertexShader **ppShader,
                                 ID3D11ClassLinkage  *pLinkage,
                           const uint8_t             *pShaderBytecode,
                                 SIZE_T               BytecodeLength )
    {
      SK_D3D11_ShaderDesc desc;

      desc.type   = SK_D3D11_ShaderType::Vertex;
      desc.crc32c = crc32c (0, (const uint8_t *)pShaderBytecode, BytecodeLength);

      if (SK_D3D11_Shaders.vertex.descs.count (desc.crc32c))
      {
        rDesc = SK_D3D11_Shaders.vertex.descs [desc.crc32c];
        rDesc.usage.refs++;

        SK_D3D11_Shaders.vertex.all.emplace    (std::make_pair (rDesc.crc32c, this));

        SK_D3D11_Shaders.vertex.newest.erase   (rDesc.crc32c);
        SK_D3D11_Shaders.vertex.newest.emplace (std::make_pair (rDesc.crc32c, this));
      } 

      else
      {
        desc.usage.last_frame = SK_GetFramesDrawn ();
        _time64 (&desc.usage.last_time);

        SK_D3D11_Shaders.vertex.all.emplace    (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.vertex.newest.emplace (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.vertex.descs.insert   (std::make_pair (desc.crc32c, desc));

        for (UINT i = 0; i < BytecodeLength; i++)
          SK_D3D11_Shaders.vertex.descs [desc.crc32c].bytecode.push_back (pShaderBytecode [i]);

        rDesc = SK_D3D11_Shaders.vertex.descs [desc.crc32c];
      }

      InterlockedExchange  (&refs,          1);
      InterlockedIncrement (&rDesc.usage.refs);

      pShader   = *ppShader;
      *ppShader = this;
    };


    /*** IUnknown methods ***/
    STDMETHOD (QueryInterface) (THIS_ REFIID riid, void** ppvObj)
    {
      if ( IsEqualGUID (riid, IID_SKVertexShaderD3D11) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }
      
      if ( IsEqualGUID (riid, IID_IUnknown)           ||
           IsEqualGUID (riid, IID_ID3D11VertexShader) ||
           IsEqualGUID (riid, IID_ID3D11DeviceChild) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }

      return pShader->QueryInterface (riid, ppvObj);
    }

    STDMETHOD_ (ULONG,AddRef) (THIS)
    {
      ULONG ret = InterlockedIncrement (&refs);

      pShader->AddRef ();

      //can_free = false;

      return ret;
    }

    STDMETHOD_ (ULONG,Release) (THIS)
    {
      ULONG ret = pShader->Release ();

      if (ret == 0)
      {
        for ( auto it  = SK_D3D11_Shaders.vertex.all.begin ();
                   it != SK_D3D11_Shaders.vertex.all.end   ();
                 ++it )
        {
          if (it->second == this)
          {
            if (SK_D3D11_Shaders.vertex.newest [rDesc.crc32c] == this)
            {
              auto it_next = it; ++it_next;
              auto it_prev = it; --it_prev;

              SK_D3D11_Shaders.vertex.newest.erase (it->first);

              if (it_next != SK_D3D11_Shaders.vertex.all.end ())
              {
                SK_D3D11_Shaders.vertex.newest.emplace (std::make_pair (it_next->first, it_next->second));
              }

              else if (it_prev != SK_D3D11_Shaders.vertex.all.end ())
              {
                SK_D3D11_Shaders.vertex.newest.emplace (std::make_pair (it_prev->first, it_prev->second));
              }
            }

            SK_D3D11_Shaders.vertex.all.erase (it);
            break;
          }
        }

        SK_D3D11_Shaders.vertex.descs [rDesc.crc32c].usage.refs--;
      }

      return ret;
    }

    /*** ID3D11DeviceChild methods ***/
    STDMETHOD_ (void,GetDevice) (_Out_ ID3D11Device **ppDevice)
    {
      return pShader->GetDevice (ppDevice);
    }

    STDMETHOD (GetPrivateData) (
         _In_                                 REFGUID  guid,
         _Inout_                              UINT    *pDataSize,
         _Out_writes_bytes_opt_( *pDataSize ) void    *pData )
    {
      return pShader->GetPrivateData (guid, pDataSize, pData);
    }
        
    STDMETHOD (SetPrivateData) (
         _In_                                   REFGUID  guid,
         _In_                                   UINT     DataSize,
         _In_reads_bytes_opt_( DataSize ) const void    *pData )
    {
      return pShader->SetPrivateData (guid, DataSize, pData);
    }

    STDMETHOD (SetPrivateDataInterface) ( _In_           REFGUID   guid,
                                          _In_opt_ const IUnknown *pData )
    {
      return pShader->SetPrivateDataInterface (guid, pData);
    }


    SK_D3D11_ShaderDesc& getDesc       (void) { return rDesc;   }
    ID3D11VertexShader*  getRealShader (void) { return pShader; }

protected:
private:
           SK_D3D11_ShaderDesc  rDesc;

           ID3D11VertexShader*  pShader = nullptr;
  volatile ULONG                refs    = 0;
};


class iSK_GeometryShaderD3D11 : public
      ID3D11GeometryShader
{
public:
    iSK_GeometryShaderD3D11 (    ID3D11GeometryShader **ppShader,
                                 ID3D11ClassLinkage    *pLinkage,
                           const uint8_t               *pShaderBytecode,
                                 SIZE_T                 BytecodeLength )
    {
      SK_D3D11_ShaderDesc desc;

      desc.type   = SK_D3D11_ShaderType::Geometry;
      desc.crc32c = crc32c (0, (const uint8_t *)pShaderBytecode, BytecodeLength);

      if (SK_D3D11_Shaders.geometry.descs.count (desc.crc32c))
      {
        rDesc = SK_D3D11_Shaders.geometry.descs [desc.crc32c];
        rDesc.usage.refs++;

        SK_D3D11_Shaders.geometry.all.emplace    (std::make_pair (rDesc.crc32c, this));

        SK_D3D11_Shaders.geometry.newest.erase   (rDesc.crc32c);
        SK_D3D11_Shaders.geometry.newest.emplace (std::make_pair (rDesc.crc32c, this));
      } 

      else
      {
        desc.usage.last_frame = SK_GetFramesDrawn ();
        _time64 (&desc.usage.last_time);

        SK_D3D11_Shaders.geometry.all.emplace    (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.geometry.newest.emplace (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.geometry.descs.insert   (std::make_pair (desc.crc32c, desc));

        for (UINT i = 0; i < BytecodeLength; i++)
          SK_D3D11_Shaders.geometry.descs [desc.crc32c].bytecode.push_back (pShaderBytecode [i]);

        rDesc = SK_D3D11_Shaders.geometry.descs [desc.crc32c];
      }

      InterlockedExchange  (&refs,          1);
      InterlockedIncrement (&rDesc.usage.refs);

      pShader   = *ppShader;
      *ppShader = this;
    };


    /*** IUnknown methods ***/
    STDMETHOD (QueryInterface) (THIS_ REFIID riid, void** ppvObj)
    {
      if ( IsEqualGUID (riid, IID_SKGeometryShaderD3D11) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }
      
      if ( IsEqualGUID (riid, IID_IUnknown)             ||
           IsEqualGUID (riid, IID_ID3D11GeometryShader) ||
           IsEqualGUID (riid, IID_ID3D11DeviceChild) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }

      return pShader->QueryInterface (riid, ppvObj);
    }

    STDMETHOD_ (ULONG,AddRef) (THIS)
    {
      ULONG ret = InterlockedIncrement (&refs);

      pShader->AddRef ();

      //can_free = false;

      return ret;
    }

    STDMETHOD_ (ULONG,Release) (THIS)
    {
      ULONG ret = pShader->Release ();

      if (ret == 0)
      {
        for ( auto it  = SK_D3D11_Shaders.geometry.all.begin ();
                   it != SK_D3D11_Shaders.geometry.all.end   ();
                 ++it )
        {
          if (it->second == this)
          {
            if (SK_D3D11_Shaders.geometry.newest [rDesc.crc32c] == this)
            {
              auto it_next = it; ++it_next;
              auto it_prev = it; --it_prev;

              SK_D3D11_Shaders.geometry.newest.erase (it->first);

              if (it_next != SK_D3D11_Shaders.geometry.all.end ())
              {
                SK_D3D11_Shaders.geometry.newest.emplace (std::make_pair (it_next->first, it_next->second));
              }

              else if (it_prev != SK_D3D11_Shaders.geometry.all.end ())
              {
                SK_D3D11_Shaders.geometry.newest.emplace (std::make_pair (it_prev->first, it_prev->second));
              }
            }

            SK_D3D11_Shaders.geometry.all.erase (it);
            break;
          }
        }

        SK_D3D11_Shaders.geometry.descs [rDesc.crc32c].usage.refs--;
      }

      return ret;
    }

    /*** ID3D11DeviceChild methods ***/
    STDMETHOD_ (void,GetDevice) (_Out_ ID3D11Device **ppDevice)
    {
      return pShader->GetDevice (ppDevice);
    }

    STDMETHOD (GetPrivateData) (
         _In_                                 REFGUID  guid,
         _Inout_                              UINT    *pDataSize,
         _Out_writes_bytes_opt_( *pDataSize ) void    *pData )
    {
      return pShader->GetPrivateData (guid, pDataSize, pData);
    }
        
    STDMETHOD (SetPrivateData) (
         _In_                                   REFGUID  guid,
         _In_                                   UINT     DataSize,
         _In_reads_bytes_opt_( DataSize ) const void    *pData )
    {
      return pShader->SetPrivateData (guid, DataSize, pData);
    }

    STDMETHOD (SetPrivateDataInterface) ( _In_           REFGUID   guid,
                                          _In_opt_ const IUnknown *pData )
    {
      return pShader->SetPrivateDataInterface (guid, pData);
    }


    SK_D3D11_ShaderDesc&  getDesc       (void) { return rDesc;   }
    ID3D11GeometryShader* getRealShader (void) { return pShader; }

protected:
private:
           SK_D3D11_ShaderDesc rDesc;

           ID3D11GeometryShader* pShader = nullptr;
  volatile ULONG                 refs    = 0;
};


class iSK_HullShaderD3D11 : public
      ID3D11HullShader
{
public:
    iSK_HullShaderD3D11 (    ID3D11HullShader   **ppShader,
                             ID3D11ClassLinkage  *pLinkage,
                       const uint8_t             *pShaderBytecode,
                             SIZE_T               BytecodeLength )
    {
      SK_D3D11_ShaderDesc desc;

      desc.type = SK_D3D11_ShaderType::Hull;
      desc.crc32c = crc32c (0, (const uint8_t *)pShaderBytecode, BytecodeLength);

      if (SK_D3D11_Shaders.hull.descs.count (desc.crc32c))
      {
        rDesc = SK_D3D11_Shaders.hull.descs [desc.crc32c];
        rDesc.usage.refs++;

        SK_D3D11_Shaders.hull.all.emplace    (std::make_pair (rDesc.crc32c, this));

        SK_D3D11_Shaders.hull.newest.erase   (rDesc.crc32c);
        SK_D3D11_Shaders.hull.newest.emplace (std::make_pair (rDesc.crc32c, this));
      } 

      else
      {
        desc.usage.last_frame = SK_GetFramesDrawn ();
        _time64 (&desc.usage.last_time);

        SK_D3D11_Shaders.hull.all.emplace    (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.hull.newest.emplace (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.hull.descs.insert   (std::make_pair (desc.crc32c, desc));

        for (UINT i = 0; i < BytecodeLength; i++)
          SK_D3D11_Shaders.hull.descs [desc.crc32c].bytecode.push_back (pShaderBytecode [i]);

        rDesc = SK_D3D11_Shaders.hull.descs [desc.crc32c];
      }

      InterlockedExchange  (&refs,          1);
      InterlockedIncrement (&rDesc.usage.refs);

      pShader   = *ppShader;
      *ppShader = this;
    };


    /*** IUnknown methods ***/
    STDMETHOD (QueryInterface) (THIS_ REFIID riid, void** ppvObj)
    {
      if ( IsEqualGUID (riid, IID_SKHullShaderD3D11) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }
      
      if ( IsEqualGUID (riid, IID_IUnknown)         ||
           IsEqualGUID (riid, IID_ID3D11HullShader) ||
           IsEqualGUID (riid, IID_ID3D11DeviceChild) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }

      return pShader->QueryInterface (riid, ppvObj);
    }

    STDMETHOD_ (ULONG,AddRef) (THIS)
    {
      ULONG ret = InterlockedIncrement (&refs);

      pShader->AddRef ();

      //can_free = false;

      return ret;
    }

    STDMETHOD_ (ULONG,Release) (THIS)
    {
      ULONG ret = pShader->Release ();

      if (ret == 0)
      {
        for ( auto it  = SK_D3D11_Shaders.hull.all.begin ();
                   it != SK_D3D11_Shaders.hull.all.end   ();
                 ++it )
        {
          if (it->second == this)
          {
            if (SK_D3D11_Shaders.hull.newest [rDesc.crc32c] == this)
            {
              auto it_next = it; ++it_next;
              auto it_prev = it; --it_prev;

              SK_D3D11_Shaders.hull.newest.erase (it->first);

              if (it_next != SK_D3D11_Shaders.hull.all.end ())
              {
                SK_D3D11_Shaders.hull.newest.emplace (std::make_pair (it_next->first, it_next->second));
              }

              else if (it_prev != SK_D3D11_Shaders.hull.all.end ())
              {
                SK_D3D11_Shaders.hull.newest.emplace (std::make_pair (it_prev->first, it_prev->second));
              }
            }

            SK_D3D11_Shaders.hull.all.erase (it);
            break;
          }
        }

        SK_D3D11_Shaders.hull.descs [rDesc.crc32c].usage.refs--;
      }

      return ret;
    }

    /*** ID3D11DeviceChild methods ***/
    STDMETHOD_ (void,GetDevice) (_Out_ ID3D11Device **ppDevice)
    {
      return pShader->GetDevice (ppDevice);
    }

    STDMETHOD (GetPrivateData) (
         _In_                                 REFGUID  guid,
         _Inout_                              UINT    *pDataSize,
         _Out_writes_bytes_opt_( *pDataSize ) void    *pData )
    {
      return pShader->GetPrivateData (guid, pDataSize, pData);
    }
        
    STDMETHOD (SetPrivateData) (
         _In_                                   REFGUID  guid,
         _In_                                   UINT     DataSize,
         _In_reads_bytes_opt_( DataSize ) const void    *pData )
    {
      return pShader->SetPrivateData (guid, DataSize, pData);
    }

    STDMETHOD (SetPrivateDataInterface) ( _In_           REFGUID   guid,
                                          _In_opt_ const IUnknown *pData )
    {
      return pShader->SetPrivateDataInterface (guid, pData);
    }


    SK_D3D11_ShaderDesc& getDesc       (void) { return rDesc;   }
    ID3D11HullShader*    getRealShader (void) { return pShader; }

protected:
private:
           SK_D3D11_ShaderDesc rDesc;

           ID3D11HullShader*  pShader = nullptr;
  volatile ULONG              refs    = 0;
};


class iSK_DomainShaderD3D11 : public
      ID3D11DomainShader
{
public:
    iSK_DomainShaderD3D11 (      ID3D11DomainShader **ppShader,
                                 ID3D11ClassLinkage  *pLinkage,
                           const uint8_t             *pShaderBytecode,
                                 SIZE_T               BytecodeLength )
    {
      SK_D3D11_ShaderDesc desc;

      desc.type   = SK_D3D11_ShaderType::Domain;
      desc.crc32c = crc32c (0, (const uint8_t *)pShaderBytecode, BytecodeLength);

      if (SK_D3D11_Shaders.domain.descs.count (desc.crc32c))
      {
        rDesc = SK_D3D11_Shaders.domain.descs [desc.crc32c];
        rDesc.usage.refs++;

        SK_D3D11_Shaders.domain.all.emplace    (std::make_pair (rDesc.crc32c, this));

        SK_D3D11_Shaders.domain.newest.erase   (rDesc.crc32c);
        SK_D3D11_Shaders.domain.newest.emplace (std::make_pair (rDesc.crc32c, this));
      } 

      else
      {
        desc.usage.last_frame = SK_GetFramesDrawn ();
        _time64 (&desc.usage.last_time);

        SK_D3D11_Shaders.domain.all.emplace    (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.domain.newest.emplace (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.domain.descs.insert   (std::make_pair (desc.crc32c, desc));

        for (UINT i = 0; i < BytecodeLength; i++)
          SK_D3D11_Shaders.domain.descs [desc.crc32c].bytecode.push_back (pShaderBytecode [i]);

        rDesc = SK_D3D11_Shaders.domain.descs [desc.crc32c];
      }

      InterlockedExchange  (&refs,          1);
      InterlockedIncrement (&rDesc.usage.refs);

      pShader   = *ppShader;
      *ppShader = this;
    };


    /*** IUnknown methods ***/
    STDMETHOD (QueryInterface) (THIS_ REFIID riid, void** ppvObj)
    {
      if ( IsEqualGUID (riid, IID_SKDomainShaderD3D11) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }
      
      if ( IsEqualGUID (riid, IID_IUnknown)           ||
           IsEqualGUID (riid, IID_ID3D11DomainShader) ||
           IsEqualGUID (riid, IID_ID3D11DeviceChild) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }

      return pShader->QueryInterface (riid, ppvObj);
    }

    STDMETHOD_ (ULONG,AddRef) (THIS)
    {
      ULONG ret = InterlockedIncrement (&refs);

      pShader->AddRef ();

      //can_free = false;

      return ret;
    }

    STDMETHOD_ (ULONG,Release) (THIS)
    {
      ULONG ret = pShader->Release ();

      if (ret == 0)
      {
        for ( auto it  = SK_D3D11_Shaders.domain.all.begin ();
                   it != SK_D3D11_Shaders.domain.all.end   ();
                 ++it )
        {
          if (it->second == this)
          {
            if (SK_D3D11_Shaders.domain.newest [rDesc.crc32c] == this)
            {
              auto it_next = it; ++it_next;
              auto it_prev = it; --it_prev;

              SK_D3D11_Shaders.domain.newest.erase (it->first);

              if (it_next != SK_D3D11_Shaders.domain.all.end ())
              {
                SK_D3D11_Shaders.domain.newest.emplace (std::make_pair (it_next->first, it_next->second));
              }

              else if (it_prev != SK_D3D11_Shaders.domain.all.end ())
              {
                SK_D3D11_Shaders.domain.newest.emplace (std::make_pair (it_prev->first, it_prev->second));
              }
            }

            SK_D3D11_Shaders.domain.all.erase (it);
            break;
          }
        }

        SK_D3D11_Shaders.domain.descs [rDesc.crc32c].usage.refs--;
      }

      return ret;
    }

    /*** ID3D11DeviceChild methods ***/
    STDMETHOD_ (void,GetDevice) (_Out_ ID3D11Device **ppDevice)
    {
      return pShader->GetDevice (ppDevice);
    }

    STDMETHOD (GetPrivateData) (
         _In_                                 REFGUID  guid,
         _Inout_                              UINT    *pDataSize,
         _Out_writes_bytes_opt_( *pDataSize ) void    *pData )
    {
      return pShader->GetPrivateData (guid, pDataSize, pData);
    }
        
    STDMETHOD (SetPrivateData) (
         _In_                                   REFGUID  guid,
         _In_                                   UINT     DataSize,
         _In_reads_bytes_opt_( DataSize ) const void    *pData )
    {
      return pShader->SetPrivateData (guid, DataSize, pData);
    }

    STDMETHOD (SetPrivateDataInterface) ( _In_           REFGUID   guid,
                                          _In_opt_ const IUnknown *pData )
    {
      return pShader->SetPrivateDataInterface (guid, pData);
    }


    SK_D3D11_ShaderDesc& getDesc       (void) { return rDesc;   }
    ID3D11DomainShader*  getRealShader (void) { return pShader; }

protected:
private:
           SK_D3D11_ShaderDesc rDesc;

           ID3D11DomainShader* pShader = nullptr;
  volatile ULONG               refs    = 0;
};


class iSK_ComputeShaderD3D11 : public
      ID3D11ComputeShader
{
public:
    iSK_ComputeShaderD3D11 (    ID3D11ComputeShader **ppShader,
                                ID3D11ClassLinkage   *pLinkage,
                          const uint8_t              *pShaderBytecode,
                                SIZE_T                BytecodeLength )
    {
      SK_D3D11_ShaderDesc desc;

      desc.type = SK_D3D11_ShaderType::Compute;
      desc.crc32c = crc32c (0, (const uint8_t *)pShaderBytecode, BytecodeLength);

      if (SK_D3D11_Shaders.compute.descs.count (desc.crc32c))
      {
        rDesc = SK_D3D11_Shaders.compute.descs [desc.crc32c];
        rDesc.usage.refs++;

        SK_D3D11_Shaders.compute.all.emplace    (std::make_pair (rDesc.crc32c, this));

        SK_D3D11_Shaders.compute.newest.erase   (rDesc.crc32c);
        SK_D3D11_Shaders.compute.newest.emplace (std::make_pair (rDesc.crc32c, this));
      } 

      else
      {
        desc.usage.last_frame = SK_GetFramesDrawn ();
        _time64 (&desc.usage.last_time);

        SK_D3D11_Shaders.compute.all.emplace    (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.compute.newest.emplace (std::make_pair (desc.crc32c, this));
        SK_D3D11_Shaders.compute.descs.insert   (std::make_pair (desc.crc32c, desc));

        SK_D3D11_Shaders.compute.descs [desc.crc32c].crc32c = desc.crc32c;

        for (UINT i = 0; i < BytecodeLength; i++)
          SK_D3D11_Shaders.compute.descs [desc.crc32c].bytecode.push_back (pShaderBytecode [i]);

        rDesc = SK_D3D11_Shaders.compute.descs [desc.crc32c];
      }

      InterlockedExchange  (&refs,          1);
      InterlockedIncrement (&rDesc.usage.refs);

      pShader   = *ppShader;
      *ppShader = this;
    };


    /*** IUnknown methods ***/
    STDMETHOD (QueryInterface) (THIS_ REFIID riid, void** ppvObj)
    {
      if ( IsEqualGUID (riid, IID_SKComputeShaderD3D11) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }
      
      if ( IsEqualGUID (riid, IID_IUnknown)            ||
           IsEqualGUID (riid, IID_ID3D11ComputeShader) ||
           IsEqualGUID (riid, IID_ID3D11DeviceChild) )
      {
        AddRef ();

        *ppvObj = this;

        return S_OK;
      }

      return pShader->QueryInterface (riid, ppvObj);
    }

    STDMETHOD_ (ULONG,AddRef) (THIS)
    {
      ULONG ret = InterlockedIncrement (&refs);

      pShader->AddRef ();

      //can_free = false;

      return ret;
    }

    STDMETHOD_ (ULONG,Release) (THIS)
    {
      ULONG ret = pShader->Release ();

      if (ret == 0)
      {
        for ( auto it  = SK_D3D11_Shaders.compute.all.begin ();
                   it != SK_D3D11_Shaders.compute.all.end   ();
                 ++it )
        {
          if (it->second == this)
          {
            if (SK_D3D11_Shaders.compute.newest [rDesc.crc32c] == this)
            {
              auto it_next = it; ++it_next;
              auto it_prev = it; --it_prev;

              SK_D3D11_Shaders.compute.newest.erase (it->first);

              if (it_next != SK_D3D11_Shaders.compute.all.end ())
              {
                SK_D3D11_Shaders.compute.newest.emplace (std::make_pair (it_next->first, it_next->second));
              }

              else if (it_prev != SK_D3D11_Shaders.compute.all.end ())
              {
                SK_D3D11_Shaders.compute.newest.emplace (std::make_pair (it_prev->first, it_prev->second));
              }
            }

            SK_D3D11_Shaders.compute.all.erase (it);
            break;
          }
        }

        SK_D3D11_Shaders.compute.descs [rDesc.crc32c].usage.refs--;
      }

      return ret;
    }

    /*** ID3D11DeviceChild methods ***/
    STDMETHOD_ (void,GetDevice) (_Out_ ID3D11Device **ppDevice)
    {
      return pShader->GetDevice (ppDevice);
    }

    STDMETHOD (GetPrivateData) (
         _In_                                 REFGUID  guid,
         _Inout_                              UINT    *pDataSize,
         _Out_writes_bytes_opt_( *pDataSize ) void    *pData )
    {
      return pShader->GetPrivateData (guid, pDataSize, pData);
    }
        
    STDMETHOD (SetPrivateData) (
         _In_                                   REFGUID  guid,
         _In_                                   UINT     DataSize,
         _In_reads_bytes_opt_( DataSize ) const void    *pData )
    {
      return pShader->SetPrivateData (guid, DataSize, pData);
    }

    STDMETHOD (SetPrivateDataInterface) ( _In_           REFGUID   guid,
                                          _In_opt_ const IUnknown *pData )
    {
      return pShader->SetPrivateDataInterface (guid, pData);
    }


    SK_D3D11_ShaderDesc& getDesc       (void) { return rDesc;   }
    ID3D11ComputeShader* getRealShader (void) { return pShader; }

protected:
private:
           SK_D3D11_ShaderDesc rDesc;

           ID3D11ComputeShader* pShader = nullptr;
  volatile ULONG                refs    = 0;
};


bool drawing_cpl = false;


__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreateVertexShader_Override (
  _In_            ID3D11Device        *This,
  _In_      const void                *pShaderBytecode,
  _In_            SIZE_T               BytecodeLength,
  _In_opt_        ID3D11ClassLinkage  *pClassLinkage,
  _Out_opt_       ID3D11VertexShader **ppVertexShader )
{
  HRESULT hr =
    D3D11Dev_CreateVertexShader_Original (This, pShaderBytecode, BytecodeLength, pClassLinkage, ppVertexShader);

  if (SUCCEEDED (hr) && ppVertexShader)
  {
    new iSK_VertexShaderD3D11 (ppVertexShader, pClassLinkage, (const uint8_t *)pShaderBytecode, BytecodeLength);
  }

  return hr;
}

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreatePixelShader_Override (
  _In_            ID3D11Device        *This,
  _In_      const void                *pShaderBytecode,
  _In_            SIZE_T               BytecodeLength,
  _In_opt_        ID3D11ClassLinkage  *pClassLinkage,
  _Out_opt_       ID3D11PixelShader  **ppPixelShader )
{
  HRESULT hr =
    D3D11Dev_CreatePixelShader_Original (This, pShaderBytecode, BytecodeLength, pClassLinkage, ppPixelShader);

  if (SUCCEEDED (hr) && ppPixelShader)
  {
    new iSK_PixelShaderD3D11 (ppPixelShader, pClassLinkage, (const uint8_t *)pShaderBytecode, BytecodeLength);
  }

  return hr;
}

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreateGeometryShader_Override (
  _In_            ID3D11Device          *This,
  _In_      const void                  *pShaderBytecode,
  _In_            SIZE_T                 BytecodeLength,
  _In_opt_        ID3D11ClassLinkage    *pClassLinkage,
  _Out_opt_       ID3D11GeometryShader **ppGeometryShader )
{
  HRESULT hr =
    D3D11Dev_CreateGeometryShader_Original (This, pShaderBytecode, BytecodeLength, pClassLinkage, ppGeometryShader);

  if (SUCCEEDED (hr) && ppGeometryShader)
  {
    new iSK_GeometryShaderD3D11 (ppGeometryShader, pClassLinkage, (const uint8_t *)pShaderBytecode, BytecodeLength);
  }

  return hr;
}

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreateHullShader_Override (
  _In_            ID3D11Device        *This,
  _In_      const void                *pShaderBytecode,
  _In_            SIZE_T               BytecodeLength,
  _In_opt_        ID3D11ClassLinkage  *pClassLinkage,
  _Out_opt_       ID3D11HullShader   **ppHullShader )
{
  HRESULT hr =
    D3D11Dev_CreateHullShader_Original (This, pShaderBytecode, BytecodeLength, pClassLinkage, ppHullShader);

  if (SUCCEEDED (hr) && ppHullShader)
  {
    new iSK_HullShaderD3D11 (ppHullShader, pClassLinkage, (const uint8_t *)pShaderBytecode, BytecodeLength);
  }

  return hr;
}

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreateDomainShader_Override (
  _In_            ID3D11Device        *This,
  _In_      const void                *pShaderBytecode,
  _In_            SIZE_T               BytecodeLength,
  _In_opt_        ID3D11ClassLinkage  *pClassLinkage,
  _Out_opt_       ID3D11DomainShader **ppDomainShader )
{
  HRESULT hr =
    D3D11Dev_CreateDomainShader_Original (This, pShaderBytecode, BytecodeLength, pClassLinkage, ppDomainShader);

  if (SUCCEEDED (hr) && ppDomainShader)
  {
    new iSK_DomainShaderD3D11 (ppDomainShader, pClassLinkage, (const uint8_t *)pShaderBytecode, BytecodeLength);
  }

  return hr;
}

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreateComputeShader_Override (
  _In_            ID3D11Device         *This,
  _In_      const void                 *pShaderBytecode,
  _In_            SIZE_T                BytecodeLength,
  _In_opt_        ID3D11ClassLinkage   *pClassLinkage,
  _Out_opt_       ID3D11ComputeShader **ppComputeShader )
{
  HRESULT hr =
    D3D11Dev_CreateComputeShader_Original (This, pShaderBytecode, BytecodeLength, pClassLinkage, ppComputeShader);

  if (SUCCEEDED (hr) && ppComputeShader)
  {
    new iSK_ComputeShaderD3D11 (ppComputeShader, pClassLinkage, (const uint8_t *)pShaderBytecode, BytecodeLength);
  }

  return hr;
}



__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_VSSetShader_Override (
 _In_     ID3D11DeviceContext        *This,
 _In_opt_ ID3D11VertexShader         *pVertexShader,
 _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
          UINT                        NumClassInstances )
{
  CComPtr <iSK_VertexShaderD3D11> known_vs = nullptr;

  if (pVertexShader)
  {
    if (SUCCEEDED (pVertexShader->QueryInterface (IID_SKVertexShaderD3D11, (void **)&known_vs)))
    {
      ++SK_D3D11_Shaders.vertex.changes_last_frame;

      SK_D3D11_ShaderDesc& rDesc = known_vs->getDesc ();

      current_vs = rDesc.crc32c;

      rDesc.usage.last_frame = SK_GetFramesDrawn ();
      _time64 (&rDesc.usage.last_time);

      SK_D3D11_Shaders.vertex.descs [rDesc.crc32c].usage.last_frame = SK_GetFramesDrawn ();

      tracked_vs.active = (rDesc.crc32c == tracked_vs.crc32c);

      D3D11_VSSetShader_Original (This, known_vs->getRealShader (), ppClassInstances, NumClassInstances);

      return;
    }
  }

  tracked_vs.active = false;
  current_vs        = 0x0;

  D3D11_VSSetShader_Original (This, pVertexShader, ppClassInstances, NumClassInstances);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_PSSetShader_Override (
 _In_     ID3D11DeviceContext        *This,
 _In_opt_ ID3D11PixelShader          *pPixelShader,
 _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
          UINT                        NumClassInstances )
{
  CComPtr <iSK_PixelShaderD3D11> known_ps = nullptr;

  if (pPixelShader)
  {
    if (SUCCEEDED (pPixelShader->QueryInterface (IID_SKPixelShaderD3D11, (void **)&known_ps)))
    {
      ++SK_D3D11_Shaders.pixel.changes_last_frame;

      SK_D3D11_ShaderDesc& rDesc = known_ps->getDesc ();

      current_ps = rDesc.crc32c;

      rDesc.usage.last_frame = SK_GetFramesDrawn ();
      _time64 (&rDesc.usage.last_time);

      SK_D3D11_Shaders.pixel.descs [rDesc.crc32c].usage.last_frame = SK_GetFramesDrawn ();

      tracked_ps.active = (rDesc.crc32c == tracked_ps.crc32c);

      D3D11_PSSetShader_Original (This, known_ps->getRealShader (), ppClassInstances, NumClassInstances);

      return;
    }
  }

  tracked_ps.active = false;
  current_ps        = 0x0;

  D3D11_PSSetShader_Original (This, pPixelShader, ppClassInstances, NumClassInstances);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_GSSetShader_Override (
 _In_     ID3D11DeviceContext        *This,
 _In_opt_ ID3D11GeometryShader       *pGeometryShader,
 _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
          UINT                        NumClassInstances )
{
  CComPtr <iSK_GeometryShaderD3D11> known_gs = nullptr;

  if (pGeometryShader)
  {
    if (SUCCEEDED (pGeometryShader->QueryInterface (IID_SKGeometryShaderD3D11, (void **)&known_gs)))
    {
      ++SK_D3D11_Shaders.geometry.changes_last_frame;

      SK_D3D11_ShaderDesc& rDesc = known_gs->getDesc ();

      current_gs = rDesc.crc32c;

      rDesc.usage.last_frame = SK_GetFramesDrawn ();
      _time64 (&rDesc.usage.last_time);

      SK_D3D11_Shaders.geometry.descs [rDesc.crc32c].usage.last_frame = SK_GetFramesDrawn ();

      tracked_gs.active = (rDesc.crc32c == tracked_gs.crc32c);

      D3D11_GSSetShader_Original (This, known_gs->getRealShader (), ppClassInstances, NumClassInstances);

      return;
    }
  }

  tracked_gs.active = false;
  current_gs        = 0x0;

  D3D11_GSSetShader_Original (This, pGeometryShader, ppClassInstances, NumClassInstances);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_HSSetShader_Override (
 _In_     ID3D11DeviceContext        *This,
 _In_opt_ ID3D11HullShader           *pHullShader,
 _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
          UINT                        NumClassInstances )
{
  CComPtr <iSK_HullShaderD3D11> known_hs = nullptr;

  if (pHullShader)
  {
    if (SUCCEEDED (pHullShader->QueryInterface (IID_SKHullShaderD3D11, (void **)&known_hs)))
    {
      ++SK_D3D11_Shaders.hull.changes_last_frame;

      SK_D3D11_ShaderDesc& rDesc = known_hs->getDesc ();

      current_hs = rDesc.crc32c;

      rDesc.usage.last_frame = SK_GetFramesDrawn ();
      _time64 (&rDesc.usage.last_time);

      SK_D3D11_Shaders.hull.descs [rDesc.crc32c].usage.last_frame = SK_GetFramesDrawn ();

      tracked_hs.active = (rDesc.crc32c == tracked_hs.crc32c);

      D3D11_HSSetShader_Original (This, known_hs->getRealShader (), ppClassInstances, NumClassInstances);

      return;
    }
  }

  tracked_hs.active = false;
  current_hs        = 0x0;

  D3D11_HSSetShader_Original (This, pHullShader, ppClassInstances, NumClassInstances);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DSSetShader_Override (
 _In_     ID3D11DeviceContext        *This,
 _In_opt_ ID3D11DomainShader         *pDomainShader,
 _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
          UINT                        NumClassInstances )
{
  CComPtr <iSK_DomainShaderD3D11> known_ds = nullptr;

  if (pDomainShader)
  {
    if (SUCCEEDED (pDomainShader->QueryInterface (IID_SKDomainShaderD3D11, (void **)&known_ds)))
    {
      ++SK_D3D11_Shaders.domain.changes_last_frame;

      SK_D3D11_ShaderDesc& rDesc = known_ds->getDesc ();

      current_ds = rDesc.crc32c;

      rDesc.usage.last_frame = SK_GetFramesDrawn ();
      _time64 (&rDesc.usage.last_time);

      tracked_ds.active = (rDesc.crc32c == tracked_ds.crc32c);

      SK_D3D11_Shaders.domain.descs [rDesc.crc32c].usage.last_frame = SK_GetFramesDrawn ();

      D3D11_DSSetShader_Original (This, known_ds->getRealShader (), ppClassInstances, NumClassInstances);

      return;
    }
  }

  tracked_ds.active = false;
  current_ds        = 0x0;

  D3D11_DSSetShader_Original (This, pDomainShader, ppClassInstances, NumClassInstances);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_CSSetShader_Override (
 _In_     ID3D11DeviceContext        *This,
 _In_opt_ ID3D11ComputeShader        *pComputeShader,
 _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
          UINT                        NumClassInstances )
{
  CComPtr <iSK_ComputeShaderD3D11> known_cs = nullptr;

  if (pComputeShader)
  {
    if (SUCCEEDED (pComputeShader->QueryInterface (IID_SKComputeShaderD3D11, (void **)&known_cs)))
    {
      ++SK_D3D11_Shaders.compute.changes_last_frame;

      SK_D3D11_ShaderDesc& rDesc = known_cs->getDesc ();

      current_cs = rDesc.crc32c;

      rDesc.usage.last_frame = SK_GetFramesDrawn ();
      _time64 (&rDesc.usage.last_time);

      tracked_cs.active = (rDesc.crc32c == tracked_cs.crc32c);

      SK_D3D11_Shaders.compute.descs [rDesc.crc32c].usage.last_frame = SK_GetFramesDrawn ();

      D3D11_CSSetShader_Original (This, known_cs->getRealShader (), ppClassInstances, NumClassInstances);

      return;
    }
  }

  tracked_cs.active = false;
  current_cs        = 0x0;

  D3D11_CSSetShader_Original (This, pComputeShader, ppClassInstances, NumClassInstances);
}

typedef void (STDMETHODCALLTYPE *D3D11_PSSetShader_pfn)(ID3D11DeviceContext        *This,
                                               _In_opt_ ID3D11PixelShader          *pPixelShader,
                                               _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
                                                        UINT                        NumClassInstances);

typedef void (STDMETHODCALLTYPE *D3D11_GSSetShader_pfn)(ID3D11DeviceContext        *This,
                                               _In_opt_ ID3D11GeometryShader       *pGeometryShader,
                                               _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
                                                        UINT                        NumClassInstances);

typedef void (STDMETHODCALLTYPE *D3D11_HSSetShader_pfn)(ID3D11DeviceContext        *This,
                                               _In_opt_ ID3D11HullShader           *pHullShader,
                                               _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
                                                        UINT                        NumClassInstances);

typedef void (STDMETHODCALLTYPE *D3D11_DSSetShader_pfn)(ID3D11DeviceContext        *This,
                                               _In_opt_ ID3D11DomainShader         *pDomainShader,
                                               _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
                                                        UINT                        NumClassInstances);

typedef void (STDMETHODCALLTYPE *D3D11_CSSetShader_pfn)(ID3D11DeviceContext        *This,
                                               _In_opt_ ID3D11ComputeShader        *pComputeShader,
                                               _In_opt_ ID3D11ClassInstance *const *ppClassInstances,
                                                        UINT                        NumClassInstances);


__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_RSSetScissorRects_Override (
        ID3D11DeviceContext *This,
        UINT                 NumRects,
  const D3D11_RECT          *pRects )
{
  return D3D11_RSSetScissorRects_Original (This, NumRects, pRects);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_VSSetConstantBuffers_Override (
  ID3D11DeviceContext*  This,
  UINT                  StartSlot,
  UINT                  NumBuffers,
  ID3D11Buffer *const  *ppConstantBuffers )
{
  //dll_log.Log (L"[   DXGI   ] [!]D3D11_VSSetConstantBuffers (%lu, %lu, ...)", StartSlot, NumBuffers);
  return D3D11_VSSetConstantBuffers_Original (This, StartSlot, NumBuffers, ppConstantBuffers );
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_VSSetShaderResources_Override (
  _In_           ID3D11DeviceContext             *This,
  _In_           UINT                             StartSlot,
  _In_           UINT                             NumViews,
  _In_opt_       ID3D11ShaderResourceView* const *ppShaderResourceViews )
{
  D3D11_VSSetShaderResources_Original (This, StartSlot, NumViews, ppShaderResourceViews);

  if (NumViews > 0 && ppShaderResourceViews)
  {
    for (UINT i = 0; i < NumViews; i++)
    {
      if (tracked_vs.crc32c != 0 && tracked_vs.active && ppShaderResourceViews [i] && (! tracked_vs.used_views.count (ppShaderResourceViews [i])))
      {
        ppShaderResourceViews [i]->AddRef ();

        tracked_vs.used_views.insert (ppShaderResourceViews [i]);
      }
    }
  }
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_PSSetShaderResources_Override (
  _In_           ID3D11DeviceContext             *This,
  _In_           UINT                             StartSlot,
  _In_           UINT                             NumViews,
  _In_opt_       ID3D11ShaderResourceView* const *ppShaderResourceViews )
{
  D3D11_PSSetShaderResources_Original (This, StartSlot, NumViews, ppShaderResourceViews);

  if (NumViews > 0 && ppShaderResourceViews)
  {
    for (UINT i = 0; i < NumViews; i++)
    {
      if (tracked_ps.crc32c != 0 && tracked_ps.active && ppShaderResourceViews [i] && (! tracked_ps.used_views.count (ppShaderResourceViews [i])))
      {
        ppShaderResourceViews [i]->AddRef ();

        tracked_ps.used_views.insert (ppShaderResourceViews [i]);
      }
    }
  }
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_GSSetShaderResources_Override (
  _In_           ID3D11DeviceContext             *This,
  _In_           UINT                             StartSlot,
  _In_           UINT                             NumViews,
  _In_opt_       ID3D11ShaderResourceView* const *ppShaderResourceViews )
{
  D3D11_GSSetShaderResources_Original (This, StartSlot, NumViews, ppShaderResourceViews);

  if (NumViews > 0 && ppShaderResourceViews)
  {
    for (UINT i = 0; i < NumViews; i++)
    {
      if (tracked_gs.crc32c != 0 && tracked_gs.active && ppShaderResourceViews [i] && (! tracked_gs.used_views.count (ppShaderResourceViews [i])))
      {
        ppShaderResourceViews [i]->AddRef ();

        tracked_gs.used_views.insert (ppShaderResourceViews [i]);
      }
    }
  }
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_HSSetShaderResources_Override (
  _In_           ID3D11DeviceContext             *This,
  _In_           UINT                             StartSlot,
  _In_           UINT                             NumViews,
  _In_opt_       ID3D11ShaderResourceView* const *ppShaderResourceViews )
{
  D3D11_HSSetShaderResources_Original (This, StartSlot, NumViews, ppShaderResourceViews);

  if (NumViews > 0 && ppShaderResourceViews)
  {
    for (UINT i = 0; i < NumViews; i++)
    {
      if (tracked_hs.crc32c != 0 && tracked_hs.active && ppShaderResourceViews [i] && (! tracked_hs.used_views.count (ppShaderResourceViews [i])))
      {
        ppShaderResourceViews [i]->AddRef ();

        tracked_hs.used_views.insert (ppShaderResourceViews [i]);
      }
    }
  }
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DSSetShaderResources_Override (
  _In_           ID3D11DeviceContext             *This,
  _In_           UINT                             StartSlot,
  _In_           UINT                             NumViews,
  _In_opt_       ID3D11ShaderResourceView* const *ppShaderResourceViews )
{
  D3D11_DSSetShaderResources_Original (This, StartSlot, NumViews, ppShaderResourceViews);

  if (NumViews > 0 && ppShaderResourceViews)
  {
    for (UINT i = 0; i < NumViews; i++)
    {
      if (tracked_ds.crc32c != 0 && tracked_ds.active && ppShaderResourceViews [i] && (! tracked_ds.used_views.count (ppShaderResourceViews [i])))
      {
        ppShaderResourceViews [i]->AddRef ();

        tracked_ds.used_views.insert (ppShaderResourceViews [i]);
      }
    }
  }
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_CSSetShaderResources_Override (
  _In_           ID3D11DeviceContext             *This,
  _In_           UINT                             StartSlot,
  _In_           UINT                             NumViews,
  _In_opt_       ID3D11ShaderResourceView* const *ppShaderResourceViews )
{
  D3D11_CSSetShaderResources_Original (This, StartSlot, NumViews, ppShaderResourceViews);

  if (NumViews > 0 && ppShaderResourceViews)
  {
    for (UINT i = 0; i < NumViews; i++)
    {
      if (tracked_cs.crc32c != 0 && tracked_cs.active && ppShaderResourceViews [i] && (! tracked_cs.used_views.count (ppShaderResourceViews [i])))
      {
        ppShaderResourceViews [i]->AddRef ();

        tracked_cs.used_views.insert (ppShaderResourceViews [i]);
      }
    }
  }
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_UpdateSubresource_Override (
        ID3D11DeviceContext* This,
        ID3D11Resource      *pDstResource,
        UINT                 DstSubresource,
  const D3D11_BOX           *pDstBox,
  const void                *pSrcData,
        UINT                 SrcRowPitch,
        UINT                 SrcDepthPitch)
{
  //dll_log.Log (L"[   DXGI   ] [!]D3D11_UpdateSubresource (%ph, %lu, %ph, %ph, %lu, %lu)", pDstResource, DstSubresource, pDstBox, pSrcData, SrcRowPitch, SrcDepthPitch);

  CComPtr <ID3D11Texture2D> pTex = nullptr;

  if (            pDstResource != nullptr &&
       SUCCEEDED (pDstResource->QueryInterface (IID_PPV_ARGS (&pTex))) )
  {
    if (SK_D3D11_TextureIsCached (pTex))
    {
      dll_log.Log (L"[DX11TexMgr] Cached texture was updated... removing from cache!");
      SK_D3D11_RemoveTexFromCache (pTex);
    }

    else
    {
      D3D11_UpdateSubresource_Original (This, pDstResource, DstSubresource, pDstBox, pSrcData, SrcRowPitch, SrcDepthPitch);
      //dll_log.Log (L"[DX11TexMgr] Updated 2D texture...");
      return;
    }
  }

  return D3D11_UpdateSubresource_Original (This, pDstResource, DstSubresource, pDstBox, pSrcData, SrcRowPitch, SrcDepthPitch);
}

__declspec (noinline,nothrow)
HRESULT
STDMETHODCALLTYPE
D3D11_Map_Override (
   _In_ ID3D11DeviceContext      *This,
   _In_ ID3D11Resource           *pResource,
   _In_ UINT                      Subresource,
   _In_ D3D11_MAP                 MapType,
   _In_ UINT                      MapFlags,
_Out_opt_ D3D11_MAPPED_SUBRESOURCE *pMappedResource )
{
  if (pResource == nullptr)
    return S_OK;

  CComPtr <ID3D11Texture2D> pTex = nullptr;

  if (            pResource != nullptr &&
       SUCCEEDED (pResource->QueryInterface (IID_PPV_ARGS (&pTex))) )
  {
    if (SK_D3D11_TextureIsCached (pTex))
    {
      dll_log.Log (L"[DX11TexMgr] Cached texture was updated... removing from cache!");
      SK_D3D11_RemoveTexFromCache (pTex);
    }

    else
    {
      //dll_log.Log (L"[DX11TexMgr] Mapped 2D texture...");
    }
  }

  return D3D11_Map_Original (This, pResource, Subresource, MapType, MapFlags, pMappedResource);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_CopyResource_Override (
       ID3D11DeviceContext *This,
  _In_ ID3D11Resource      *pDstResource,
  _In_ ID3D11Resource      *pSrcResource )
{
  //CComPtr <ID3D11Texture2D> pSrcTex;
  CComPtr <ID3D11Texture2D> pDstTex = nullptr;

  if (            pDstResource != nullptr && 
       SUCCEEDED (pDstResource->QueryInterface (IID_PPV_ARGS (&pDstTex))) )
  {
    if (SK_D3D11_TextureIsCached (pDstTex)) {
      dll_log.Log (L"[DX11TexMgr] Cached texture was modified... removing from cache!");
      SK_D3D11_RemoveTexFromCache (pDstTex);
    }
  }

  return D3D11_CopyResource_Original (This, pDstResource, pSrcResource);
}


bool
SK_D3D11_DrawHandler (void)
{
  if (tracked_vs.active) { tracked_vs.use (nullptr); }
  if (tracked_ps.active) { tracked_ps.use (nullptr); }
  if (tracked_gs.active) { tracked_gs.use (nullptr); }
  if (tracked_hs.active) { tracked_hs.use (nullptr); }
  if (tracked_ds.active) { tracked_ds.use (nullptr); }

  if (tracked_vs.active && tracked_vs.cancel_draws) return true;
  if (tracked_ps.active && tracked_ps.cancel_draws) return true;
  if (tracked_gs.active && tracked_gs.cancel_draws) return true;
  if (tracked_hs.active && tracked_hs.cancel_draws) return true;
  if (tracked_ds.active && tracked_ds.cancel_draws) return true;

  if (SK_D3D11_Blacklist_VS.count (current_vs)) return true;
  if (SK_D3D11_Blacklist_PS.count (current_ps)) return true;
  if (SK_D3D11_Blacklist_GS.count (current_gs)) return true;
  if (SK_D3D11_Blacklist_HS.count (current_hs)) return true;
  if (SK_D3D11_Blacklist_DS.count (current_ds)) return true;

  return false;
}

bool
SK_D3D11_DispatchHandler (void)
{
  if (tracked_cs.active) { tracked_cs.use (nullptr); }

  if (tracked_cs.active && tracked_cs.cancel_draws) return true;

  if (SK_D3D11_Blacklist_CS.count (current_cs)) return true;

  return false;
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DrawAuto_Override (_In_ ID3D11DeviceContext *This)
{
  if (SK_D3D11_DrawHandler ())
    return;

  return D3D11_DrawAuto_Original (This);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DrawIndexed_Override (
  _In_ ID3D11DeviceContext *This,
  _In_ UINT                 IndexCount,
  _In_ UINT                 StartIndexLocation,
  _In_ INT                  BaseVertexLocation )
{
  if (SK_D3D11_DrawHandler ())
    return;

  return D3D11_DrawIndexed_Original ( This, IndexCount,
                                              StartIndexLocation,
                                                BaseVertexLocation );
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_Draw_Override (
  _In_ ID3D11DeviceContext *This,
  _In_ UINT                 VertexCount,
  _In_ UINT                 StartVertexLocation )
{
  if (SK_D3D11_DrawHandler ())
    return;

  return D3D11_Draw_Original ( This, VertexCount, StartVertexLocation );
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DrawIndexedInstanced_Override (
  _In_ ID3D11DeviceContext *This,
  _In_ UINT                 IndexCountPerInstance,
  _In_ UINT                 InstanceCount,
  _In_ UINT                 StartIndexLocation,
  _In_ INT                  BaseVertexLocation,
  _In_ UINT                 StartInstanceLocation )
{
  if (SK_D3D11_DrawHandler ())
    return;

  return D3D11_DrawIndexedInstanced_Original (This, IndexCountPerInstance, InstanceCount, StartIndexLocation, BaseVertexLocation, StartInstanceLocation);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DrawIndexedInstancedIndirect_Override (
  _In_ ID3D11DeviceContext *This,
  _In_ ID3D11Buffer        *pBufferForArgs,
  _In_ UINT                 AlignedByteOffsetForArgs )
{
  if (SK_D3D11_DrawHandler ())
    return;

  return D3D11_DrawIndexedInstancedIndirect_Original (This, pBufferForArgs, AlignedByteOffsetForArgs);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DrawInstanced_Override (
  _In_ ID3D11DeviceContext *This,
  _In_ UINT                 VertexCountPerInstance,
  _In_ UINT                 InstanceCount,
  _In_ UINT                 StartVertexLocation,
  _In_ UINT                 StartInstanceLocation )
{
  if (SK_D3D11_DrawHandler ())
    return;

  return D3D11_DrawInstanced_Original (This, VertexCountPerInstance, InstanceCount, StartVertexLocation, StartInstanceLocation);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DrawInstancedIndirect_Override (
  _In_ ID3D11DeviceContext *This,
  _In_ ID3D11Buffer        *pBufferForArgs,
  _In_ UINT                 AlignedByteOffsetForArgs )
{
  if (SK_D3D11_DrawHandler ())
    return;

  return D3D11_DrawInstancedIndirect_Original (This, pBufferForArgs, AlignedByteOffsetForArgs);
}


__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_Dispatch_Override ( _In_ ID3D11DeviceContext *This,
                          _In_ UINT                 ThreadGroupCountX,
                          _In_ UINT                 ThreadGroupCountY,
                          _In_ UINT                 ThreadGroupCountZ )
{
  if (SK_D3D11_DispatchHandler ())
    return;

  return D3D11_Dispatch_Original (This, ThreadGroupCountX, ThreadGroupCountY, ThreadGroupCountZ);
}

__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_DispatchIndirect_Override ( _In_ ID3D11DeviceContext *This,
                                  _In_ ID3D11Buffer        *pBufferForArgs,
                                  _In_ UINT                 AlignedByteOffsetForArgs )
{
  if (SK_D3D11_DispatchHandler ())
    return;

  return D3D11_DispatchIndirect_Original (This, pBufferForArgs, AlignedByteOffsetForArgs);
}



__declspec (noinline,nothrow)
void
STDMETHODCALLTYPE
D3D11_RSSetViewports_Override (
        ID3D11DeviceContext* This,
        UINT                 NumViewports,
  const D3D11_VIEWPORT*      pViewports )
{
  return D3D11_RSSetViewports_Original (This, NumViewports, pViewports);
}


LPVOID pfnD3D11CreateDevice             = nullptr;
LPVOID pfnD3D11CreateDeviceAndSwapChain = nullptr;



struct cache_params_t {
  uint32_t max_entries       = 4096;
  uint32_t min_entries       = 1024;
   int32_t max_size          = 2048L;
   int32_t min_size          = 512L;
  uint32_t min_evict         = 16;
  uint32_t max_evict         = 64;
      bool ignore_non_mipped = false;
} cache_opts;

CRITICAL_SECTION tex_cs;
CRITICAL_SECTION hash_cs;
CRITICAL_SECTION dump_cs;
CRITICAL_SECTION cache_cs;
CRITICAL_SECTION inject_cs;

void WINAPI SK_D3D11_SetResourceRoot      (const wchar_t* root);
void WINAPI SK_D3D11_EnableTexDump        (bool enable);
void WINAPI SK_D3D11_EnableTexInject      (bool enable);
void WINAPI SK_D3D11_EnableTexCache       (bool enable);
void WINAPI SK_D3D11_PopulateResourceList (void);

#include <unordered_set>
#include <unordered_map>
#include <map>

std::unordered_map <uint32_t, std::wstring> tex_hashes;
std::unordered_map <uint32_t, std::wstring> tex_hashes_ex;

std::unordered_map <uint32_t, std::wstring> tex_hashes_ffx;

std::unordered_set <uint32_t>               dumped_textures;
std::unordered_set <uint32_t>               dumped_collisions;
std::unordered_set <uint32_t>               injectable_textures;
std::unordered_set <uint32_t>               injected_collisions;

std::unordered_set <uint32_t>               injectable_ffx; // HACK FOR FFX


SK_D3D11_TexMgr SK_D3D11_Textures;

class SK_D3D11_TexCacheMgr {
public:
};


std::string
SK_D3D11_SummarizeTexCache (void)
{
  char szOut [512];

  snprintf ( szOut, 512, "  Tex Cache: %#5zu MiB   Entries:   %#7zu\n"
                         "  Hits:      %#5lu       Time Saved: %#7.01lf ms\n"
                         "  Evictions: %#5lu",
               SK_D3D11_Textures.AggregateSize_2D >> 20ULL,
               SK_D3D11_Textures.TexRefs_2D.size (),
               SK_D3D11_Textures.RedundantLoads_2D,
               SK_D3D11_Textures.RedundantTime_2D,
               SK_D3D11_Textures.Evicted_2D );

  return szOut;
}

#include <SpecialK/utility.h>

bool         SK_D3D11_need_tex_reset      = false;
int32_t      SK_D3D11_amount_to_purge     = 0L;

bool         SK_D3D11_dump_textures       = false;// true;
bool         SK_D3D11_inject_textures_ffx = false;
bool         SK_D3D11_inject_textures     = false;
bool         SK_D3D11_cache_textures      = false;
bool         SK_D3D11_mark_textures       = false;
std::wstring SK_D3D11_res_root            = L"";

bool
SK_D3D11_TexMgr::isTexture2D (uint32_t crc32, const D3D11_TEXTURE2D_DESC *pDesc)
{
  if (! SK_D3D11_cache_textures)
    return false;

  SK_AutoCriticalSection critical (&tex_cs);

  if (crc32 != 0x00 && HashMap_2D [pDesc->MipLevels].count (crc32))
    return true;

  return false;
}

void
__stdcall
SK_D3D11_ResetTexCache (void)
{
  SK_D3D11_Textures.reset ();
}

#define PSAPI_VERSION           1

#include <psapi.h>
#pragma comment (lib, "psapi.lib")

void
__stdcall
SK_D3D11_TexCacheCheckpoint (void)
{
  static int       iter               = 0;

  static bool      init               = false;
  static ULONGLONG ullMemoryTotal_KiB = 0;
  static HANDLE    hProc              = nullptr;

  if (! init) {
    hProc = GetCurrentProcess ();
    GetPhysicallyInstalledSystemMemory (&ullMemoryTotal_KiB);
    init = true;
  }

  ++iter;

  if ((iter % 3))
    return;

  PROCESS_MEMORY_COUNTERS pmc    = { 0 };
                          pmc.cb = sizeof pmc;

  GetProcessMemoryInfo (hProc, &pmc, sizeof pmc);

  if ( (SK_D3D11_Textures.AggregateSize_2D >> 20ULL) > (uint32_t)cache_opts.max_size    ||
        SK_D3D11_Textures.TexRefs_2D.size ()         >           cache_opts.max_entries ||
        SK_D3D11_need_tex_reset                                                         ||
       (config.mem.reserve / 100.0f) * ullMemoryTotal_KiB 
                                                     < 
                          (pmc.PagefileUsage >> 10UL) )
  {
    //dll_log.Log (L"[DX11TexMgr] DXGI 1.4 Budget Change: Triggered a texture manager purge...");

    SK_D3D11_amount_to_purge =
      static_cast <int32_t> (
        (pmc.PagefileUsage >> 10UL) - (float)(ullMemoryTotal_KiB >> 10ULL) *
                       (config.mem.reserve / 100.0f)
      );

    SK_D3D11_need_tex_reset = false;
    SK_D3D11_Textures.reset ();
  }
}

void
SK_D3D11_TexMgr::reset (void)
{
  std::vector <SK_D3D11_TexMgr::tex2D_descriptor_s> textures;
  textures.reserve (TexRefs_2D.size ());

  uint32_t count  = 0;
  int64_t  purged = 0;

  {
    SK_AutoCriticalSection critical (&tex_cs);

    for ( ID3D11Texture2D* tex : TexRefs_2D ) {
      if (Textures_2D.count (tex))
        textures.push_back (Textures_2D [tex]);
    }
  }

  std::sort ( textures.begin (),
                textures.end (),
      []( SK_D3D11_TexMgr::tex2D_descriptor_s a,
          SK_D3D11_TexMgr::tex2D_descriptor_s b )
    {
      return a.last_used < b.last_used;
    }
  );

  const uint32_t max_count = cache_opts.max_evict;

  for ( SK_D3D11_TexMgr::tex2D_descriptor_s desc : textures ) {
    int64_t mem_size = (int64_t)(desc.mem_size >> 20);

    if (desc.texture != nullptr) {
#ifdef DS3_REF_TWEAK
      int refs = IUnknown_AddRef_Original (desc.texture) - 1;

      if (refs <= 3 && desc.texture->Release () <= 3) {
#else
      int refs = IUnknown_AddRef_Original (desc.texture) - 1;

      if (refs == 1 && desc.texture->Release () <= 1) {
#endif
        for (int i = 0; i < refs; i++) {
          desc.texture->Release ();
        }

        count++;

        purged += mem_size;

        AggregateSize_2D = std::max ((size_t)0, AggregateSize_2D);

        if ( ( (AggregateSize_2D >> 20ULL) <= (uint32_t)cache_opts.min_size &&
                                     count >=           cache_opts.min_evict ) ||
             (SK_D3D11_amount_to_purge     <=           purged              &&
                                     count >=           cache_opts.min_evict ) ||
                                     count >=           max_count )
        {
          SK_D3D11_amount_to_purge = std::max (0, SK_D3D11_amount_to_purge);
          //dll_log.Log ( L"[DX11TexMgr] Purged %llu MiB of texture "
                          //L"data across %lu textures",
                          //purged >> 20ULL, count );

          break;
        }
      } else {
        desc.texture->Release ();
      }
    }
  }
}

ID3D11Texture2D*
SK_D3D11_TexMgr::getTexture2D ( uint32_t              crc32,
                          const D3D11_TEXTURE2D_DESC* pDesc,
                                size_t*               pMemSize,
                                float*                pTimeSaved )
{
  if (! SK_D3D11_cache_textures)
    return nullptr;

  ID3D11Texture2D* pTex2D = nullptr;

  if (isTexture2D (crc32, pDesc))
  {
#ifdef TEST_COLLISIONS
    std::unordered_map <uint32_t, ID3D11Texture2D *>::iterator it =
      HashMap_2D [pDesc->MipLevels].begin ();

    while (it != HashMap_2D [pDesc->MipLevels].end ())
    {
      if (! SK_D3D11_TextureIsCached (it->second))
      {
        ++it;
        continue;
      }
#else
    auto it = HashMap_2D [pDesc->MipLevels][crc32];
#endif
      tex2D_descriptor_s desc2d;

      {
        SK_AutoCriticalSection critical (&tex_cs);

#ifdef TEST_COLISIONS
        desc2d =
          Textures_2D [it->second];
#else
        desc2d =
          Textures_2D [it];
#endif
      }

      D3D11_TEXTURE2D_DESC desc = desc2d.desc;

      if ( desc2d.crc32        == crc32                 &&
           desc.Format         == pDesc->Format         &&
           desc.Width          == pDesc->Width          &&
           desc.Height         == pDesc->Height         &&
           desc.BindFlags      == pDesc->BindFlags      &&
           desc.CPUAccessFlags == pDesc->CPUAccessFlags &&
           desc.Usage          == pDesc->Usage )
      {
        pTex2D = desc2d.texture;

        size_t   size = desc2d.mem_size;
        uint64_t time = desc2d.load_time;

        float   fTime = (float)time * 1000.0f / (float)PerfFreq.QuadPart;

        if (pMemSize != nullptr) {
          *pMemSize = size;
        }

        if (pTimeSaved != nullptr) {
          *pTimeSaved = fTime;
        }

        desc2d.last_used = SK_QueryPerf ().QuadPart;
        desc2d.hits++;

        RedundantData_2D += size;
        RedundantTime_2D += fTime;
        RedundantLoads_2D++;

        return pTex2D;
      }

#ifdef TEST_COLLISIONS
      else if (desc2d.crc32 == crc32)
      {
        dll_log.Log ( L"[DX11TexMgr] ## Hash Collision for Texture: "
                          L"'%08X'!! ## ",
                            crc32 );
      }

      ++it;
    }
#endif
  }

  return pTex2D;
}

bool
__stdcall
SK_D3D11_TextureIsCached (ID3D11Texture2D* pTex)
{
  if (! SK_D3D11_cache_textures)
    return false;

  SK_AutoCriticalSection critical (&cache_cs);

  return SK_D3D11_Textures.Textures_2D.count (pTex) != 0;
}

void
__stdcall
SK_D3D11_UseTexture (ID3D11Texture2D* pTex)
{
  if (! SK_D3D11_cache_textures)
    return;

  SK_AutoCriticalSection critical (&cache_cs);

  if (SK_D3D11_TextureIsCached (pTex)) {
    SK_D3D11_Textures.Textures_2D [pTex].last_used =
      SK_QueryPerf ().QuadPart;
  }
}

void
__stdcall
SK_D3D11_RemoveTexFromCache (ID3D11Texture2D* pTex)
{
  if (! SK_D3D11_TextureIsCached (pTex))
    return;

  if (pTex != nullptr) {
    SK_AutoCriticalSection critical (&cache_cs);

    uint32_t crc32 = SK_D3D11_Textures.Textures_2D [pTex].crc32;

    SK_D3D11_Textures.AggregateSize_2D -=
      SK_D3D11_Textures.Textures_2D [pTex].mem_size;

    SK_D3D11_Textures.Evicted_2D++;

    D3D11_TEXTURE2D_DESC desc;
    pTex->GetDesc (&desc);

    SK_D3D11_Textures.Textures_2D.erase                 (pTex);
    SK_D3D11_Textures.HashMap_2D [desc.MipLevels].erase (crc32);
    SK_D3D11_Textures.TexRefs_2D.erase                  (pTex);
  }
}

void
SK_D3D11_TexMgr::refTexture2D ( ID3D11Texture2D*      pTex,
                          const D3D11_TEXTURE2D_DESC *pDesc,
                                uint32_t              crc32,
                                size_t                mem_size,
                                uint64_t              load_time )
{
  if (! SK_D3D11_cache_textures)
    return;

  if (pTex == nullptr || crc32 == 0x00)
    return;

  //if (! injectable_textures.count (crc32))
    //return;

  if (SK_D3D11_TextureIsCached (pTex)) {
    dll_log.Log (L"[DX11TexMgr] Texture is already cached?!");
  }

  if (pDesc->Usage != D3D11_USAGE_DEFAULT &&
      pDesc->Usage != D3D11_USAGE_IMMUTABLE) {
//    dll_log.Log ( L"[DX11TexMgr] Texture '%08X' Is Not Cacheable "
//                  L"Due To Usage: %lu",
//                  crc32, pDesc->Usage );
    return;
  }

  if (pDesc->CPUAccessFlags != 0x00) {
//    dll_log.Log ( L"[DX11TexMgr] Texture '%08X' Is Not Cacheable "
//                  L"Due To CPUAccessFlags: 0x%X",
//                  crc32, pDesc->CPUAccessFlags );
    return;
  }

  AggregateSize_2D += mem_size;

  tex2D_descriptor_s desc2d;

  desc2d.desc      = *pDesc;
  desc2d.texture   =  pTex;
  desc2d.load_time =  load_time;
  desc2d.mem_size  =  mem_size;
  desc2d.crc32     =  crc32;


  SK_AutoCriticalSection critical (&cache_cs);

  TexRefs_2D.insert                    (                       pTex);
  HashMap_2D [pDesc->MipLevels].insert (std::make_pair (crc32, pTex));
  Textures_2D.insert                   (std::make_pair (       pTex, desc2d));

  // Hold a reference ourselves so that the game cannot free it
  pTex->AddRef ();
#ifdef DS3_REF_TWEAK
  pTex->AddRef ();
  pTex->AddRef ();
#endif
}

#include <Shlwapi.h>

void
WINAPI
SK_D3D11_PopulateResourceList (void)
{
  static bool init = false;

  if (init || SK_D3D11_res_root.empty ())
    return;

  SK_AutoCriticalSection critical (&tex_cs);

  init = true;

  wchar_t wszTexDumpDir_RAW [ MAX_PATH     ] = { L'\0' };
  wchar_t wszTexDumpDir     [ MAX_PATH + 2 ] = { L'\0' };

  lstrcatW (wszTexDumpDir_RAW, SK_D3D11_res_root.c_str ());
  lstrcatW (wszTexDumpDir_RAW, L"\\dump\\textures\\");
  lstrcatW (wszTexDumpDir_RAW, SK_GetHostApp ());

  wcscpy ( wszTexDumpDir,
             SK_EvalEnvironmentVars (wszTexDumpDir_RAW).c_str () );

  //
  // Walk custom textures so we don't have to query the filesystem on every
  //   texture load to check if a custom one exists.
  //
  if ( GetFileAttributesW (wszTexDumpDir) !=
         INVALID_FILE_ATTRIBUTES ) {
    WIN32_FIND_DATA fd;
    HANDLE          hFind  = INVALID_HANDLE_VALUE;
    unsigned int    files  = 0UL;
    LARGE_INTEGER   liSize = { 0 };

    LARGE_INTEGER   liCompressed   = { 0 };
    LARGE_INTEGER   liUncompressed = { 0 };

    dll_log.LogEx ( true, L"[DX11TexMgr] Enumerating dumped...    " );

    lstrcatW (wszTexDumpDir, L"\\*");

    hFind = FindFirstFileW (wszTexDumpDir, &fd);

    if (hFind != INVALID_HANDLE_VALUE) {
      do {
        if (fd.dwFileAttributes != INVALID_FILE_ATTRIBUTES) {
          // Dumped Metadata has the extension .dds.txt, do not
          //   include these while scanning for textures.
          if (    StrStrIW (fd.cFileName, L".dds")    &&
               (! StrStrIW (fd.cFileName, L".dds.txt") ) ) {
            uint32_t top_crc32 = 0x00;
            uint32_t checksum  = 0x00;

            bool compressed = false;

            if (StrStrIW (fd.cFileName, L"Uncompressed_")) {
              if (StrStrIW (StrStrIW (fd.cFileName, L"_") + 1, L"_")) {
                swscanf ( fd.cFileName,
                            L"Uncompressed_%08X_%08X.dds",
                              &top_crc32,
                                &checksum );
              } else {
                swscanf ( fd.cFileName,
                            L"Uncompressed_%08X.dds",
                              &top_crc32 );
                checksum = 0x00;
              }
            } else {
              if (StrStrIW (StrStrIW (fd.cFileName, L"_") + 1, L"_")) {
                swscanf ( fd.cFileName,
                            L"Compressed_%08X_%08X.dds",
                              &top_crc32,
                                &checksum );
              } else {
                swscanf ( fd.cFileName,
                            L"Compressed_%08X.dds",
                              &top_crc32 );
                checksum = 0x00;
              }
              compressed = true;
            }

            ++files;

            LARGE_INTEGER fsize;

            fsize.HighPart = fd.nFileSizeHigh;
            fsize.LowPart  = fd.nFileSizeLow;

            liSize.QuadPart += fsize.QuadPart;

            if (compressed)
              liCompressed.QuadPart += fsize.QuadPart;
            else
              liUncompressed.QuadPart += fsize.QuadPart;

            if (dumped_textures.count (top_crc32) >= 1)
              dll_log.Log ( L"[DX11TexDmp] >> WARNING: Multiple textures have "
                            L"the same top-level LOD hash (%08X) <<",
                              top_crc32 );

            if (checksum == 0x00)
              dumped_textures.insert (top_crc32);
            else
              dumped_collisions.insert (crc32c (top_crc32, (uint8_t *)&checksum, 4));
          }
        }
      } while (FindNextFileW (hFind, &fd) != 0);

      FindClose (hFind);
    }

    dll_log.LogEx ( false, L" %lu files (%3.1f MiB -- %3.1f:%3.1f MiB Un:Compressed)\n",
                      files, (double)liSize.QuadPart / (1024.0 * 1024.0),
                               (double)liUncompressed.QuadPart / (1024.0 * 1024.0),
                                 (double)liCompressed.QuadPart /  (1024.0 * 1024.0) );
  }

  wchar_t wszTexInjectDir_RAW [ MAX_PATH + 2 ] = { L'\0' };
  wchar_t wszTexInjectDir     [ MAX_PATH + 2 ] = { L'\0' };

  lstrcatW (wszTexInjectDir_RAW, SK_D3D11_res_root.c_str ());
  lstrcatW (wszTexInjectDir_RAW, L"\\inject\\textures");

  wcscpy ( wszTexInjectDir,
             SK_EvalEnvironmentVars (wszTexInjectDir_RAW).c_str () );

  if ( GetFileAttributesW (wszTexInjectDir) !=
         INVALID_FILE_ATTRIBUTES ) {
    WIN32_FIND_DATA fd     = { 0 };
    HANDLE          hFind  = INVALID_HANDLE_VALUE;
    unsigned int    files  =   0;
    LARGE_INTEGER   liSize = { 0 };

    dll_log.LogEx ( true, L"[DX11TexMgr] Enumerating injectable..." );

    lstrcatW (wszTexInjectDir, L"\\*");

    hFind = FindFirstFileW (wszTexInjectDir, &fd);

    if (hFind != INVALID_HANDLE_VALUE) {
      do {
        if (fd.dwFileAttributes != INVALID_FILE_ATTRIBUTES) {
          if (StrStrIW (fd.cFileName, L".dds")) {
            uint32_t top_crc32;
            uint32_t checksum  = 0x00;

            if (StrStrIW (fd.cFileName, L"_")) {
              swscanf (fd.cFileName, L"%08X_%08X.dds", &top_crc32, &checksum);
            } else {
              swscanf (fd.cFileName, L"%08X.dds",    &top_crc32);
            }

            ++files;

            LARGE_INTEGER fsize;

            fsize.HighPart = fd.nFileSizeHigh;
            fsize.LowPart  = fd.nFileSizeLow;

            liSize.QuadPart += fsize.QuadPart;

            injectable_textures.insert (top_crc32);

            if (checksum != 0x00)
              injected_collisions.insert (crc32c (top_crc32, (const uint8_t *)&checksum, 4));
          }
        }
      } while (FindNextFileW (hFind, &fd) != 0);

      FindClose (hFind);
    }

    dll_log.LogEx ( false, L" %lu files (%3.1f MiB)\n",
                      files, (double)liSize.QuadPart / (1024.0 * 1024.0) );
  }

  wchar_t wszTexInjectDir_FFX_RAW [ MAX_PATH     ] = { L'\0' };
  wchar_t wszTexInjectDir_FFX     [ MAX_PATH + 2 ] = { L'\0' };

  lstrcatW (wszTexInjectDir_FFX_RAW, SK_D3D11_res_root.c_str ());
  lstrcatW (wszTexInjectDir_FFX_RAW, L"\\inject\\textures\\UnX_Old");

  wcscpy ( wszTexInjectDir_FFX,
             SK_EvalEnvironmentVars (wszTexInjectDir_FFX_RAW).c_str () );

  if ( GetFileAttributesW (wszTexInjectDir_FFX) !=
         INVALID_FILE_ATTRIBUTES ) {
    WIN32_FIND_DATA fd     = { 0 };
    HANDLE          hFind  = INVALID_HANDLE_VALUE;
    int             files  =   0;
    LARGE_INTEGER   liSize = { 0 };

    dll_log.LogEx ( true, L"[DX11TexMgr] Enumerating FFX inject..." );

    lstrcatW (wszTexInjectDir_FFX, L"\\*");

    hFind = FindFirstFileW (wszTexInjectDir_FFX, &fd);

    if (hFind != INVALID_HANDLE_VALUE) {
      do {
        if (fd.dwFileAttributes != INVALID_FILE_ATTRIBUTES) {
          if (StrStrIW (fd.cFileName, L".dds")) {
            uint32_t ffx_crc32;

            swscanf (fd.cFileName, L"%08X.dds", &ffx_crc32);

            ++files;

            LARGE_INTEGER fsize;

            fsize.HighPart = fd.nFileSizeHigh;
            fsize.LowPart  = fd.nFileSizeLow;

            liSize.QuadPart += fsize.QuadPart;

            injectable_ffx.insert (ffx_crc32);
          }
        }
      } while (FindNextFileW (hFind, &fd) != 0);

      FindClose (hFind);
    }

    dll_log.LogEx ( false, L" %lu files (%3.1f MiB)\n",
                      files, (double)liSize.QuadPart / (1024.0 * 1024.0) );
  }
}

void
WINAPI
SK_D3D11_SetResourceRoot (const wchar_t* root)
{
  // Non-absolute path (e.g. NOT C:\...\...")
  if (! wcsstr (root, L":"))
  {
    SK_D3D11_res_root = SK_GetRootPath ();
    SK_D3D11_res_root += L"\\";
    SK_D3D11_res_root += root;
  }

  else
    SK_D3D11_res_root = root;
}

void
WINAPI
SK_D3D11_EnableTexDump (bool enable)
{
  SK_D3D11_dump_textures = enable;
}

void
WINAPI
SK_D3D11_EnableTexInject (bool enable)
{
  SK_D3D11_inject_textures = enable;
}

void
WINAPI
SK_D3D11_EnableTexInject_FFX (bool enable)
{
  SK_D3D11_inject_textures     = enable;
  SK_D3D11_inject_textures_ffx = enable;
}

void
WINAPI
SK_D3D11_EnableTexCache (bool enable)
{
  SK_D3D11_cache_textures = enable;
}

void
WINAPI
SKX_D3D11_MarkTextures (bool x, bool y, bool z)
{
  UNREFERENCED_PARAMETER (x);
  UNREFERENCED_PARAMETER (y);
  UNREFERENCED_PARAMETER (z);

  return;
}


bool
WINAPI
SK_D3D11_IsTexHashed (uint32_t top_crc32, uint32_t hash)
{
  SK_D3D11_InitTextures ();

  SK_AutoCriticalSection critical (&hash_cs);

  if (tex_hashes_ex.count (crc32c (top_crc32, (const uint8_t *)&hash, 4)) != 0)
    return true;

  return tex_hashes.count (top_crc32) != 0;
}

void
WINAPI
SK_D3D11_AddInjectable (uint32_t top_crc32, uint32_t checksum);

void
WINAPI
SK_D3D11_AddTexHash ( const wchar_t* name, uint32_t top_crc32, uint32_t hash )
{
  // Allow UnX to call this before a device has been created.
  SK_D3D11_InitTextures ();

  if (hash != 0x00) {
    if (! SK_D3D11_IsTexHashed (top_crc32, hash)) {
      SK_AutoCriticalSection critical (&hash_cs);

      tex_hashes_ex.insert (std::make_pair (crc32c (top_crc32, (const uint8_t *)&hash, 4), name));
      SK_D3D11_AddInjectable (top_crc32, hash);
    }
  }

  if (! SK_D3D11_IsTexHashed (top_crc32, 0x00)) {
    SK_AutoCriticalSection critical (&hash_cs);

    tex_hashes.insert (std::make_pair (top_crc32, name));

    if (! SK_D3D11_inject_textures_ffx)
      SK_D3D11_AddInjectable (top_crc32, 0x00);
    else
      injectable_ffx.insert (top_crc32);
  }
}

void
WINAPI
SK_D3D11_RemoveTexHash (uint32_t top_crc32, uint32_t hash)
{
  // Allow UnX to call this before a device has been created.
  SK_D3D11_InitTextures ();

  if (hash != 0x00 && SK_D3D11_IsTexHashed (top_crc32, hash)) {
    SK_AutoCriticalSection critical (&hash_cs);

    tex_hashes_ex.erase (crc32c (top_crc32, (const uint8_t *)&hash, 4));
  }

  else if (hash == 0x00 && SK_D3D11_IsTexHashed (top_crc32, 0x00)) {
    tex_hashes.erase (top_crc32);
 }
}

std::wstring
__stdcall
SK_D3D11_TexHashToName (uint32_t top_crc32, uint32_t hash)
{
  // Allow UnX to call this before a device has been created.
  SK_D3D11_InitTextures ();

  std::wstring ret = L"";

  if (hash != 0x00 && SK_D3D11_IsTexHashed (top_crc32, hash)) {
    SK_AutoCriticalSection critical (&hash_cs);

    ret = tex_hashes_ex [crc32c (top_crc32, (const uint8_t *)&hash, 4)];
  } else if (hash == 0x00 && SK_D3D11_IsTexHashed (top_crc32, 0x00)) {
    SK_AutoCriticalSection critical (&hash_cs);

    ret = tex_hashes [top_crc32];
  }

  return ret;
}

INT
__stdcall
SK_D3D11_BytesPerPixel (DXGI_FORMAT fmt)
{
  switch (fmt)
  {
    case DXGI_FORMAT_R32G32B32A32_TYPELESS:      return 16;
    case DXGI_FORMAT_R32G32B32A32_FLOAT:         return 16;
    case DXGI_FORMAT_R32G32B32A32_UINT:          return 16;
    case DXGI_FORMAT_R32G32B32A32_SINT:          return 16;

    case DXGI_FORMAT_R32G32B32_TYPELESS:         return 12;
    case DXGI_FORMAT_R32G32B32_FLOAT:            return 12;
    case DXGI_FORMAT_R32G32B32_UINT:             return 12;
    case DXGI_FORMAT_R32G32B32_SINT:             return 12;

    case DXGI_FORMAT_R16G16B16A16_TYPELESS:      return 8;
    case DXGI_FORMAT_R16G16B16A16_FLOAT:         return 8;
    case DXGI_FORMAT_R16G16B16A16_UNORM:         return 8;
    case DXGI_FORMAT_R16G16B16A16_UINT:          return 8;
    case DXGI_FORMAT_R16G16B16A16_SNORM:         return 8;
    case DXGI_FORMAT_R16G16B16A16_SINT:          return 8;

    case DXGI_FORMAT_R32G32_TYPELESS:            return 8;
    case DXGI_FORMAT_R32G32_FLOAT:               return 8;
    case DXGI_FORMAT_R32G32_UINT:                return 8;
    case DXGI_FORMAT_R32G32_SINT:                return 8;
    case DXGI_FORMAT_R32G8X24_TYPELESS:          return 8;

    case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:       return 8;
    case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:   return 8;
    case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:    return 8;

    case DXGI_FORMAT_R10G10B10A2_TYPELESS:       return 4;
    case DXGI_FORMAT_R10G10B10A2_UNORM:          return 4;
    case DXGI_FORMAT_R10G10B10A2_UINT:           return 4;
    case DXGI_FORMAT_R11G11B10_FLOAT:            return 4;

    case DXGI_FORMAT_R8G8B8A8_TYPELESS:          return 4;
    case DXGI_FORMAT_R8G8B8A8_UNORM:             return 4;
    case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:        return 4;
    case DXGI_FORMAT_R8G8B8A8_UINT:              return 4;
    case DXGI_FORMAT_R8G8B8A8_SNORM:             return 4;
    case DXGI_FORMAT_R8G8B8A8_SINT:              return 4;

    case DXGI_FORMAT_R16G16_TYPELESS:            return 4;
    case DXGI_FORMAT_R16G16_FLOAT:               return 4;
    case DXGI_FORMAT_R16G16_UNORM:               return 4;
    case DXGI_FORMAT_R16G16_UINT:                return 4;
    case DXGI_FORMAT_R16G16_SNORM:               return 4;
    case DXGI_FORMAT_R16G16_SINT:                return 4;

    case DXGI_FORMAT_R32_TYPELESS:               return 4;
    case DXGI_FORMAT_D32_FLOAT:                  return 4;
    case DXGI_FORMAT_R32_FLOAT:                  return 4;
    case DXGI_FORMAT_R32_UINT:                   return 4;
    case DXGI_FORMAT_R32_SINT:                   return 4;
    case DXGI_FORMAT_R24G8_TYPELESS:             return 4;

    case DXGI_FORMAT_D24_UNORM_S8_UINT:          return 4;
    case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:      return 4;
    case DXGI_FORMAT_X24_TYPELESS_G8_UINT:       return 4;

    case DXGI_FORMAT_R8G8_TYPELESS:              return 2;
    case DXGI_FORMAT_R8G8_UNORM:                 return 2;
    case DXGI_FORMAT_R8G8_UINT:                  return 2;
    case DXGI_FORMAT_R8G8_SNORM:                 return 2;
    case DXGI_FORMAT_R8G8_SINT:                  return 2;

    case DXGI_FORMAT_R16_TYPELESS:               return 2;
    case DXGI_FORMAT_R16_FLOAT:                  return 2;
    case DXGI_FORMAT_D16_UNORM:                  return 2;
    case DXGI_FORMAT_R16_UNORM:                  return 2;
    case DXGI_FORMAT_R16_UINT:                   return 2;
    case DXGI_FORMAT_R16_SNORM:                  return 2;
    case DXGI_FORMAT_R16_SINT:                   return 2;

    case DXGI_FORMAT_R8_TYPELESS:                return 1;
    case DXGI_FORMAT_R8_UNORM:                   return 1;
    case DXGI_FORMAT_R8_UINT:                    return 1;
    case DXGI_FORMAT_R8_SNORM:                   return 1;
    case DXGI_FORMAT_R8_SINT:                    return 1;
    case DXGI_FORMAT_A8_UNORM:                   return 1;
    case DXGI_FORMAT_R1_UNORM:                   return 1;

    case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:         return 4;
    case DXGI_FORMAT_R8G8_B8G8_UNORM:            return 4;
    case DXGI_FORMAT_G8R8_G8B8_UNORM:            return 4;

    case DXGI_FORMAT_BC1_TYPELESS:               return -1;
    case DXGI_FORMAT_BC1_UNORM:                  return -1;
    case DXGI_FORMAT_BC1_UNORM_SRGB:             return -1;
    case DXGI_FORMAT_BC2_TYPELESS:               return -2;
    case DXGI_FORMAT_BC2_UNORM:                  return -2;
    case DXGI_FORMAT_BC2_UNORM_SRGB:             return -2;
    case DXGI_FORMAT_BC3_TYPELESS:               return -2;
    case DXGI_FORMAT_BC3_UNORM:                  return -2;
    case DXGI_FORMAT_BC3_UNORM_SRGB:             return -2;
    case DXGI_FORMAT_BC4_TYPELESS:               return -1;
    case DXGI_FORMAT_BC4_UNORM:                  return -1;
    case DXGI_FORMAT_BC4_SNORM:                  return -1;
    case DXGI_FORMAT_BC5_TYPELESS:               return -2;
    case DXGI_FORMAT_BC5_UNORM:                  return -2;
    case DXGI_FORMAT_BC5_SNORM:                  return -2;

    case DXGI_FORMAT_B5G6R5_UNORM:               return 2;
    case DXGI_FORMAT_B5G5R5A1_UNORM:             return 2;
    case DXGI_FORMAT_B8G8R8X8_UNORM:             return 4;
    case DXGI_FORMAT_B8G8R8A8_UNORM:             return 4;
    case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM: return 4;
    case DXGI_FORMAT_B8G8R8A8_TYPELESS:          return 4;
    case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:        return 4;
    case DXGI_FORMAT_B8G8R8X8_TYPELESS:          return 4;
    case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:        return 4;

    case DXGI_FORMAT_BC6H_TYPELESS:              return -2;
    case DXGI_FORMAT_BC6H_UF16:                  return -2;
    case DXGI_FORMAT_BC6H_SF16:                  return -2;
    case DXGI_FORMAT_BC7_TYPELESS:               return -2;
    case DXGI_FORMAT_BC7_UNORM:                  return -2;
    case DXGI_FORMAT_BC7_UNORM_SRGB:             return -2;

    case DXGI_FORMAT_AYUV:                       return 0;
    case DXGI_FORMAT_Y410:                       return 0;
    case DXGI_FORMAT_Y416:                       return 0;
    case DXGI_FORMAT_NV12:                       return 0;
    case DXGI_FORMAT_P010:                       return 0;
    case DXGI_FORMAT_P016:                       return 0;
    case DXGI_FORMAT_420_OPAQUE:                 return 0;
    case DXGI_FORMAT_YUY2:                       return 0;
    case DXGI_FORMAT_Y210:                       return 0;
    case DXGI_FORMAT_Y216:                       return 0;
    case DXGI_FORMAT_NV11:                       return 0;
    case DXGI_FORMAT_AI44:                       return 0;
    case DXGI_FORMAT_IA44:                       return 0;
    case DXGI_FORMAT_P8:                         return 1;
    case DXGI_FORMAT_A8P8:                       return 2;
    case DXGI_FORMAT_B4G4R4A4_UNORM:             return 2;

    default: return 0;
  }
}

std::wstring
__stdcall
SK_DXGI_FormatToStr (DXGI_FORMAT fmt)
{
  switch (fmt)
  {
    case DXGI_FORMAT_R32G32B32A32_TYPELESS:      return L"DXGI_FORMAT_R32G32B32A32_TYPELESS";
    case DXGI_FORMAT_R32G32B32A32_FLOAT:         return L"DXGI_FORMAT_R32G32B32A32_FLOAT";
    case DXGI_FORMAT_R32G32B32A32_UINT:          return L"DXGI_FORMAT_R32G32B32A32_UINT";
    case DXGI_FORMAT_R32G32B32A32_SINT:          return L"DXGI_FORMAT_R32G32B32A32_SINT";

    case DXGI_FORMAT_R32G32B32_TYPELESS:         return L"DXGI_FORMAT_R32G32B32_TYPELESS";
    case DXGI_FORMAT_R32G32B32_FLOAT:            return L"DXGI_FORMAT_R32G32B32_FLOAT";
    case DXGI_FORMAT_R32G32B32_UINT:             return L"DXGI_FORMAT_R32G32B32_UINT";
    case DXGI_FORMAT_R32G32B32_SINT:             return L"DXGI_FORMAT_R32G32B32_SINT";

    case DXGI_FORMAT_R16G16B16A16_TYPELESS:      return L"DXGI_FORMAT_R16G16B16A16_TYPELESS";
    case DXGI_FORMAT_R16G16B16A16_FLOAT:         return L"DXGI_FORMAT_R16G16B16A16_FLOAT";
    case DXGI_FORMAT_R16G16B16A16_UNORM:         return L"DXGI_FORMAT_R16G16B16A16_UNORM";
    case DXGI_FORMAT_R16G16B16A16_UINT:          return L"DXGI_FORMAT_R16G16B16A16_UINT";
    case DXGI_FORMAT_R16G16B16A16_SNORM:         return L"DXGI_FORMAT_R16G16B16A16_SNORM";
    case DXGI_FORMAT_R16G16B16A16_SINT:          return L"DXGI_FORMAT_R16G16B16A16_SINT";

    case DXGI_FORMAT_R32G32_TYPELESS:            return L"DXGI_FORMAT_R32G32_TYPELESS";
    case DXGI_FORMAT_R32G32_FLOAT:               return L"DXGI_FORMAT_R32G32_FLOAT";
    case DXGI_FORMAT_R32G32_UINT:                return L"DXGI_FORMAT_R32G32_UINT";
    case DXGI_FORMAT_R32G32_SINT:                return L"DXGI_FORMAT_R32G32_SINT";
    case DXGI_FORMAT_R32G8X24_TYPELESS:          return L"DXGI_FORMAT_R32G8X24_TYPELESS";

    case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:       return L"DXGI_FORMAT_D32_FLOAT_S8X24_UINT";
    case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:   return L"DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS";
    case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:    return L"DXGI_FORMAT_X32_TYPELESS_G8X24_UINT";

    case DXGI_FORMAT_R10G10B10A2_TYPELESS:       return L"DXGI_FORMAT_R10G10B10A2_TYPELESS";
    case DXGI_FORMAT_R10G10B10A2_UNORM:          return L"DXGI_FORMAT_R10G10B10A2_UNORM";
    case DXGI_FORMAT_R10G10B10A2_UINT:           return L"DXGI_FORMAT_R10G10B10A2_UINT";
    case DXGI_FORMAT_R11G11B10_FLOAT:            return L"DXGI_FORMAT_R11G11B10_FLOAT";

    case DXGI_FORMAT_R8G8B8A8_TYPELESS:          return L"DXGI_FORMAT_R8G8B8A8_TYPELESS";
    case DXGI_FORMAT_R8G8B8A8_UNORM:             return L"DXGI_FORMAT_R8G8B8A8_UNORM";
    case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:        return L"DXGI_FORMAT_R8G8B8A8_UNORM_SRGB";
    case DXGI_FORMAT_R8G8B8A8_UINT:              return L"DXGI_FORMAT_R8G8B8A8_UINT";
    case DXGI_FORMAT_R8G8B8A8_SNORM:             return L"DXGI_FORMAT_R8G8B8A8_SNORM";
    case DXGI_FORMAT_R8G8B8A8_SINT:              return L"DXGI_FORMAT_R8G8B8A8_SINT";

    case DXGI_FORMAT_R16G16_TYPELESS:            return L"DXGI_FORMAT_R16G16_TYPELESS";
    case DXGI_FORMAT_R16G16_FLOAT:               return L"DXGI_FORMAT_R16G16_FLOAT";
    case DXGI_FORMAT_R16G16_UNORM:               return L"DXGI_FORMAT_R16G16_UNORM";
    case DXGI_FORMAT_R16G16_UINT:                return L"DXGI_FORMAT_R16G16_UINT";
    case DXGI_FORMAT_R16G16_SNORM:               return L"DXGI_FORMAT_R16G16_SNORM";
    case DXGI_FORMAT_R16G16_SINT:                return L"DXGI_FORMAT_R16G16_SINT";

    case DXGI_FORMAT_R32_TYPELESS:               return L"DXGI_FORMAT_R32_TYPELESS";
    case DXGI_FORMAT_D32_FLOAT:                  return L"DXGI_FORMAT_D32_FLOAT";
    case DXGI_FORMAT_R32_FLOAT:                  return L"DXGI_FORMAT_R32_FLOAT";
    case DXGI_FORMAT_R32_UINT:                   return L"DXGI_FORMAT_R32_UINT";
    case DXGI_FORMAT_R32_SINT:                   return L"DXGI_FORMAT_R32_SINT";
    case DXGI_FORMAT_R24G8_TYPELESS:             return L"DXGI_FORMAT_R24G8_TYPELESS";

    case DXGI_FORMAT_D24_UNORM_S8_UINT:          return L"DXGI_FORMAT_D24_UNORM_S8_UINT";
    case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:      return L"DXGI_FORMAT_R24_UNORM_X8_TYPELESS";
    case DXGI_FORMAT_X24_TYPELESS_G8_UINT:       return L"DXGI_FORMAT_X24_TYPELESS_G8_UINT";

    case DXGI_FORMAT_R8G8_TYPELESS:              return L"DXGI_FORMAT_R8G8_TYPELESS";
    case DXGI_FORMAT_R8G8_UNORM:                 return L"DXGI_FORMAT_R8G8_UNORM";
    case DXGI_FORMAT_R8G8_UINT:                  return L"DXGI_FORMAT_R8G8_UINT";
    case DXGI_FORMAT_R8G8_SNORM:                 return L"DXGI_FORMAT_R8G8_SNORM";
    case DXGI_FORMAT_R8G8_SINT:                  return L"DXGI_FORMAT_R8G8_SINT";

    case DXGI_FORMAT_R16_TYPELESS:               return L"DXGI_FORMAT_R16_TYPELESS";
    case DXGI_FORMAT_R16_FLOAT:                  return L"DXGI_FORMAT_R16_FLOAT";
    case DXGI_FORMAT_D16_UNORM:                  return L"DXGI_FORMAT_D16_UNORM";
    case DXGI_FORMAT_R16_UNORM:                  return L"DXGI_FORMAT_R16_UNORM";
    case DXGI_FORMAT_R16_UINT:                   return L"DXGI_FORMAT_R16_UINT";
    case DXGI_FORMAT_R16_SNORM:                  return L"DXGI_FORMAT_R16_SNORM";
    case DXGI_FORMAT_R16_SINT:                   return L"DXGI_FORMAT_R16_SINT";

    case DXGI_FORMAT_R8_TYPELESS:                return L"DXGI_FORMAT_R8_TYPELESS";
    case DXGI_FORMAT_R8_UNORM:                   return L"DXGI_FORMAT_R8_UNORM";
    case DXGI_FORMAT_R8_UINT:                    return L"DXGI_FORMAT_R8_UINT";
    case DXGI_FORMAT_R8_SNORM:                   return L"DXGI_FORMAT_R8_SNORM";
    case DXGI_FORMAT_R8_SINT:                    return L"DXGI_FORMAT_R8_SINT";
    case DXGI_FORMAT_A8_UNORM:                   return L"DXGI_FORMAT_A8_UNORM";
    case DXGI_FORMAT_R1_UNORM:                   return L"DXGI_FORMAT_R1_UNORM";

    case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:         return L"DXGI_FORMAT_R9G9B9E5_SHAREDEXP";
    case DXGI_FORMAT_R8G8_B8G8_UNORM:            return L"DXGI_FORMAT_R8G8_B8G8_UNORM";
    case DXGI_FORMAT_G8R8_G8B8_UNORM:            return L"DXGI_FORMAT_G8R8_G8B8_UNORM";

    case DXGI_FORMAT_BC1_TYPELESS:               return L"DXGI_FORMAT_BC1_TYPELESS";
    case DXGI_FORMAT_BC1_UNORM:                  return L"DXGI_FORMAT_BC1_UNORM";
    case DXGI_FORMAT_BC1_UNORM_SRGB:             return L"DXGI_FORMAT_BC1_UNORM_SRGB";
    case DXGI_FORMAT_BC2_TYPELESS:               return L"DXGI_FORMAT_BC2_TYPELESS";
    case DXGI_FORMAT_BC2_UNORM:                  return L"DXGI_FORMAT_BC2_UNORM";
    case DXGI_FORMAT_BC2_UNORM_SRGB:             return L"DXGI_FORMAT_BC2_UNORM_SRGB";
    case DXGI_FORMAT_BC3_TYPELESS:               return L"DXGI_FORMAT_BC3_TYPELESS";
    case DXGI_FORMAT_BC3_UNORM:                  return L"DXGI_FORMAT_BC3_UNORM";
    case DXGI_FORMAT_BC3_UNORM_SRGB:             return L"DXGI_FORMAT_BC3_UNORM_SRGB";
    case DXGI_FORMAT_BC4_TYPELESS:               return L"DXGI_FORMAT_BC4_TYPELESS";
    case DXGI_FORMAT_BC4_UNORM:                  return L"DXGI_FORMAT_BC4_UNORM";
    case DXGI_FORMAT_BC4_SNORM:                  return L"DXGI_FORMAT_BC4_SNORM";
    case DXGI_FORMAT_BC5_TYPELESS:               return L"DXGI_FORMAT_BC5_TYPELESS";
    case DXGI_FORMAT_BC5_UNORM:                  return L"DXGI_FORMAT_BC5_UNORM";
    case DXGI_FORMAT_BC5_SNORM:                  return L"DXGI_FORMAT_BC5_SNORM";

    case DXGI_FORMAT_B5G6R5_UNORM:               return L"DXGI_FORMAT_B5G6R5_UNORM";
    case DXGI_FORMAT_B5G5R5A1_UNORM:             return L"DXGI_FORMAT_B5G5R5A1_UNORM";
    case DXGI_FORMAT_B8G8R8X8_UNORM:             return L"DXGI_FORMAT_B8G8R8X8_UNORM";
    case DXGI_FORMAT_B8G8R8A8_UNORM:             return L"DXGI_FORMAT_B8G8R8A8_UNORM";
    case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM: return L"DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM";
    case DXGI_FORMAT_B8G8R8A8_TYPELESS:          return L"DXGI_FORMAT_B8G8R8A8_TYPELESS";
    case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:        return L"DXGI_FORMAT_B8G8R8A8_UNORM_SRGB";
    case DXGI_FORMAT_B8G8R8X8_TYPELESS:          return L"DXGI_FORMAT_B8G8R8X8_TYPELESS";
    case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:        return L"DXGI_FORMAT_B8G8R8X8_UNORM_SRGB";

    case DXGI_FORMAT_BC6H_TYPELESS:              return L"DXGI_FORMAT_BC6H_TYPELESS";
    case DXGI_FORMAT_BC6H_UF16:                  return L"DXGI_FORMAT_BC6H_UF16";
    case DXGI_FORMAT_BC6H_SF16:                  return L"DXGI_FORMAT_BC6H_SF16";
    case DXGI_FORMAT_BC7_TYPELESS:               return L"DXGI_FORMAT_BC7_TYPELESS";
    case DXGI_FORMAT_BC7_UNORM:                  return L"DXGI_FORMAT_BC7_UNORM";
    case DXGI_FORMAT_BC7_UNORM_SRGB:             return L"DXGI_FORMAT_BC7_UNORM_SRGB";

    case DXGI_FORMAT_AYUV:                       return L"DXGI_FORMAT_AYUV";
    case DXGI_FORMAT_Y410:                       return L"DXGI_FORMAT_Y410";
    case DXGI_FORMAT_Y416:                       return L"DXGI_FORMAT_Y416";
    case DXGI_FORMAT_NV12:                       return L"DXGI_FORMAT_NV12";
    case DXGI_FORMAT_P010:                       return L"DXGI_FORMAT_P010";
    case DXGI_FORMAT_P016:                       return L"DXGI_FORMAT_P016";
    case DXGI_FORMAT_420_OPAQUE:                 return L"DXGI_FORMAT_420_OPAQUE";
    case DXGI_FORMAT_YUY2:                       return L"DXGI_FORMAT_YUY2";
    case DXGI_FORMAT_Y210:                       return L"DXGI_FORMAT_Y210";
    case DXGI_FORMAT_Y216:                       return L"DXGI_FORMAT_Y216";
    case DXGI_FORMAT_NV11:                       return L"DXGI_FORMAT_NV11";
    case DXGI_FORMAT_AI44:                       return L"DXGI_FORMAT_AI44";
    case DXGI_FORMAT_IA44:                       return L"DXGI_FORMAT_IA44";
    case DXGI_FORMAT_P8:                         return L"DXGI_FORMAT_P8";
    case DXGI_FORMAT_A8P8:                       return L"DXGI_FORMAT_A8P8";
    case DXGI_FORMAT_B4G4R4A4_UNORM:             return L"DXGI_FORMAT_B4G4R4A4_UNORM";

    default:                                     return L"UNKNONW";
  }
}

__declspec (noinline)
uint32_t
__cdecl
crc32_tex (  _In_      const D3D11_TEXTURE2D_DESC   *pDesc,
             _In_      const D3D11_SUBRESOURCE_DATA *pInitialData,
             _Out_opt_       size_t                 *pSize,
             _Out_opt_       uint32_t               *pLOD0_CRC32 )
{
  // Ignore Cubemaps for Now
  if (pDesc->MiscFlags == 0x04)
  {
//    dll_log.Log (L"[ Tex Hash ] >> Will not hash cubemap");
    if (pLOD0_CRC32)
      *pLOD0_CRC32 = 0x0000;

    if (pSize)
      *pSize       = 0;

    return 0;
  }

  if (pDesc->MiscFlags != 0x00)
  {
    dll_log.Log ( L"[ Tex Hash ] >> Hashing texture with unexpected MiscFlags: "
                   L"0x%04X",
                     pDesc->MiscFlags );
  }

  uint32_t checksum = 0;

  bool compressed = false;

  if ( (pDesc->Format >= DXGI_FORMAT_BC1_TYPELESS  &&
        pDesc->Format <= DXGI_FORMAT_BC5_SNORM)    ||
       (pDesc->Format >= DXGI_FORMAT_BC6H_TYPELESS &&
        pDesc->Format <= DXGI_FORMAT_BC7_UNORM_SRGB) )
    compressed = true;

  const int bpp = ( (pDesc->Format >= DXGI_FORMAT_BC1_TYPELESS &&
                     pDesc->Format <= DXGI_FORMAT_BC1_UNORM_SRGB) ||
                    (pDesc->Format >= DXGI_FORMAT_BC4_TYPELESS &&
                     pDesc->Format <= DXGI_FORMAT_BC4_SNORM) ) ? 0 : 1;

  unsigned int width  = pDesc->Width;
  unsigned int height = pDesc->Height;

        size_t size   = 0UL;

  if (compressed)
  {
    for (unsigned int i = 0; i < pDesc->MipLevels; i++)
    {
      char* pData    = (char *)pInitialData [i].pSysMem;
      UINT stride = bpp == 0 ?
             std::max (1UL, ((width + 3UL) / 4UL) ) * 8UL :
             std::max (1UL, ((width + 3UL) / 4UL) ) * 16UL;

      // Fast path:  Data is tightly packed and alignment agrees with
      //               convention...
      if (stride == pInitialData [i].SysMemPitch)
      {
        unsigned int lod_size = stride * (height / 4 +
                                          height % 4);

        checksum = crc32c (checksum, (const uint8_t *)pData, lod_size);
        size    += lod_size;
      }

      else
      {
        // We are running through the compressed image block-by-block,
        //  the lines we are "scanning" actually represent 4 rows of image data.
        for (unsigned int j = 0; j < height; j += 4)
        {
          checksum =
            crc32c (checksum, (const uint8_t *)pData, stride);

          // Respect the engine's reported stride, while making sure to
          //   only read the 4x4 blocks that have legal data. Any padding
          //     the engine uses will not be included in our hash since the
          //       values are undefined.
          pData += pInitialData [i].SysMemPitch;
          size  += stride;
        }
      }

      if (i == 0 && pLOD0_CRC32 != nullptr)
        *pLOD0_CRC32 = checksum;

      if (width  > 1) width  >>= 1UL;
      if (height > 1) height >>= 1UL;
    }
  }

  else
  {
    for (unsigned int i = 0; i < pDesc->MipLevels; i++)
    {
      char* pData      = (char *)pInitialData [i].pSysMem;
      UINT  scanlength = SK_D3D11_BytesPerPixel (pDesc->Format) * width;

      // Fast path:  Data is tightly packed and alignment agrees with
      //               convention...
      if (scanlength == pInitialData [i].SysMemPitch) 
      {
        unsigned int lod_size = (scanlength * height);

        checksum = crc32c (checksum, (const uint8_t *)pData, lod_size);
        size    += lod_size;
      }

      else
      {
        for (unsigned int j = 0; j < height; j++)
        {
          checksum =
            crc32c (checksum, (const uint8_t *)pData, scanlength);

          pData += pInitialData [i].SysMemPitch;
          size  += scanlength;
        }
      }

      if (i == 0 && pLOD0_CRC32 != nullptr)
        *pLOD0_CRC32 = checksum;

      if (width  > 1) width  >>= 1UL;
      if (height > 1) height >>= 1UL;
    }
  }

  if (pSize != nullptr)
    *pSize = size;

  return checksum;
}

//
// OLD, BUGGY Algorithm... must remain here for compatibility with UnX :(
//
__declspec (noinline)
uint32_t
__cdecl
crc32_ffx (  _In_      const D3D11_TEXTURE2D_DESC   *pDesc,
             _In_      const D3D11_SUBRESOURCE_DATA *pInitialData,
             _Out_opt_       size_t                 *pSize )
{
  uint32_t checksum = 0;

  bool compressed = false;

  if (pDesc->Format >= DXGI_FORMAT_BC1_TYPELESS && pDesc->Format <= DXGI_FORMAT_BC5_SNORM)
    compressed = true;

  if (pDesc->Format >= DXGI_FORMAT_BC6H_TYPELESS && pDesc->Format <= DXGI_FORMAT_BC7_UNORM_SRGB)
    compressed = true;

  int block_size = pDesc->Format == DXGI_FORMAT_BC1_UNORM ? 8 : 16;

//int width      = pDesc->Width;
  int height     = pDesc->Height;

  size_t size = 0;

  for (unsigned int i = 0; i < pDesc->MipLevels; i++) {
    if (compressed) {
      size += (pInitialData [i].SysMemPitch / block_size) * (height >> i);

      checksum =
        crc32 (checksum, (const char *)pInitialData [i].pSysMem, (pInitialData [i].SysMemPitch / block_size) * (height >> i));
    } else {
      size += (pInitialData [i].SysMemPitch) * (height >> i);

      checksum =
        crc32 (checksum, (const char *)pInitialData [i].pSysMem, (pInitialData [i].SysMemPitch) * (height >> i));
    }
  }

  if (pSize != nullptr)
    *pSize = size;

  return checksum;
}


bool
__stdcall
SK_D3D11_IsDumped (uint32_t top_crc32, uint32_t checksum)
{
  SK_AutoCriticalSection critical (&dump_cs);

  if (config.textures.d3d11.precise_hash && dumped_collisions.count (crc32c (top_crc32, (uint8_t *)&checksum, 4)))
    return true;
  if (! config.textures.d3d11.precise_hash)
    return dumped_textures.count (top_crc32) != 0;

  return false;
}

void
__stdcall
SK_D3D11_AddDumped (uint32_t top_crc32, uint32_t checksum)
{
  SK_AutoCriticalSection critical (&dump_cs);

  if (! config.textures.d3d11.precise_hash)
    dumped_textures.insert (top_crc32);

  dumped_collisions.insert (crc32c (top_crc32, (uint8_t *)&checksum, 4));
}

bool
__stdcall
SK_D3D11_IsInjectable (uint32_t top_crc32, uint32_t checksum)
{
  SK_AutoCriticalSection critical (&inject_cs);

  if (checksum != 0x00) {
    if (injected_collisions.count (crc32c (top_crc32, (uint8_t *)&checksum, 4)))
      return true;
    return false;
  }

  return injectable_textures.count (top_crc32) != 0;
}

bool
__stdcall
SK_D3D11_IsInjectable_FFX (uint32_t top_crc32)
{
  SK_AutoCriticalSection critical (&inject_cs);

  return injectable_ffx.count (top_crc32) != 0;
}


void
__stdcall
SK_D3D11_AddInjectable (uint32_t top_crc32, uint32_t checksum)
{
  SK_AutoCriticalSection critical (&inject_cs);

  if (checksum != 0x00)
    injected_collisions.insert (crc32c (top_crc32, (uint8_t *)&checksum, 4));

  injectable_textures.insert (top_crc32);
}

#include <DirectXTex/DirectXTex.h>

HRESULT
__stdcall
SK_D3D11_DumpTexture2D (  _In_ const D3D11_TEXTURE2D_DESC   *pDesc,
                          _In_ const D3D11_SUBRESOURCE_DATA *pInitialData,
                          _In_       uint32_t                top_crc32,
                          _In_       uint32_t                checksum )
{
  dll_log.Log ( L"[DX11TexDmp] Dumping Texture: %08x::%08x... (fmt=%03lu, "
                    L"BindFlags=0x%04x, Usage=0x%04x, CPUAccessFlags"
                    L"=0x%02x, Misc=0x%02x, MipLODs=%02lu, ArraySize=%02lu)",
                  top_crc32,
                    checksum,
                      pDesc->Format,
                        pDesc->BindFlags,
                          pDesc->Usage,
                            pDesc->CPUAccessFlags,
                              pDesc->MiscFlags,
                                pDesc->MipLevels,
                                  pDesc->ArraySize );

  SK_D3D11_AddDumped (top_crc32, checksum);

  DirectX::TexMetadata mdata;

  mdata.width      = pDesc->Width;
  mdata.height     = pDesc->Height;
  mdata.depth      = 1;
  mdata.arraySize  = pDesc->ArraySize;
  mdata.mipLevels  = pDesc->MipLevels;
  mdata.miscFlags  = (pDesc->MiscFlags & D3D11_RESOURCE_MISC_TEXTURECUBE) ? 
                        DirectX::TEX_MISC_TEXTURECUBE : 0;
  mdata.miscFlags2 = 0;
  mdata.format     = pDesc->Format;
  mdata.dimension  = DirectX::TEX_DIMENSION_TEXTURE2D;

  DirectX::ScratchImage image;
  image.Initialize (mdata);

  bool error = false;

  for (size_t slice = 0; slice < mdata.arraySize; ++slice) {
    size_t height = mdata.height;

    for (size_t lod = 0; lod < mdata.mipLevels; ++lod) {
      const DirectX::Image* img =
        image.GetImage (lod, slice, 0);

      if (! (img && img->pixels)) {
        error = true;
        break;
      }

      const size_t lines =
        DirectX::ComputeScanlines (mdata.format, height);

      if  (! lines) {
        error = true;
        break;
      }

      auto sptr =
        static_cast <const uint8_t *>(
          pInitialData [lod].pSysMem
        );

      uint8_t* dptr =
        img->pixels;

      for (size_t h = 0; h < lines; ++h) {
        size_t msize =
          std::min <size_t> (img->rowPitch, pInitialData [lod].SysMemPitch);

        memcpy_s (dptr, img->rowPitch, sptr, msize);

        sptr += pInitialData [lod].SysMemPitch;
        dptr += img->rowPitch;
      }

      if (height > 1) height >>= 1;
    }

    if (error)
      break;
  }

  wchar_t wszPath [ MAX_PATH + 2 ] = { L'\0' };

  wcscpy ( wszPath,
             SK_EvalEnvironmentVars (SK_D3D11_res_root.c_str ()).c_str () );

  lstrcatW (wszPath, L"/dump/textures/");
  lstrcatW (wszPath, SK_GetHostApp ());
  lstrcatW (wszPath, L"/");

  SK_CreateDirectories (wszPath);

  bool compressed = false;

  if ( ( pDesc->Format >= DXGI_FORMAT_BC1_TYPELESS &&
         pDesc->Format <= DXGI_FORMAT_BC5_SNORM )  ||
       ( pDesc->Format >= DXGI_FORMAT_BC6H_TYPELESS &&
         pDesc->Format <= DXGI_FORMAT_BC7_UNORM_SRGB ) )
    compressed = true;

  wchar_t wszOutPath [MAX_PATH + 2] = { L'\0' };
  wchar_t wszOutName [MAX_PATH + 2] = { L'\0' };

  wcscpy ( wszOutPath,
             SK_EvalEnvironmentVars (SK_D3D11_res_root.c_str ()).c_str () );

  lstrcatW (wszOutPath, L"\\dump\\textures\\");
  lstrcatW (wszOutPath, SK_GetHostApp ());

  if (compressed && config.textures.d3d11.precise_hash) {
    _swprintf ( wszOutName, L"%s\\Compressed_%08X_%08X.dds",
                  wszOutPath, top_crc32, checksum );
  } else if (compressed) {
    _swprintf ( wszOutName, L"%s\\Compressed_%08X.dds",
                  wszOutPath, top_crc32 );
  } else if (config.textures.d3d11.precise_hash) {
    _swprintf ( wszOutName, L"%s\\Uncompressed_%08X_%08X.dds",
                  wszOutPath, top_crc32, checksum );
  } else {
    _swprintf ( wszOutName, L"%s\\Uncompressed_%08X.dds",
                  wszOutPath, top_crc32 );
  }

  if ((! error) && wcslen (wszOutName)) {
    if (GetFileAttributes (wszOutName) == INVALID_FILE_ATTRIBUTES) {
      dll_log.Log ( L"[DX11TexDmp]  >> File: '%s' (top: %x, full: %x)",
                      wszOutName,
                        top_crc32,
                          checksum );

      wchar_t wszMetaFilename [ MAX_PATH + 2 ] = { L'\0' };

      _swprintf (wszMetaFilename, L"%s.txt", wszOutName);

      FILE* fMetaData = _wfopen (wszMetaFilename, L"w+");

      if (fMetaData != nullptr) {
        fprintf ( fMetaData,
                  "Dumped Name:    %ws\n"
                  "Texture:        %08x::%08x\n"
                  "Dimensions:     (%lux%lu)\n"
                  "Format:         %03lu\n"
                  "BindFlags:      0x%04x\n"
                  "Usage:          0x%04x\n"
                  "CPUAccessFlags: 0x%02x\n"
                  "Misc:           0x%02x\n"
                  "MipLODs:        %02lu\n"
                  "ArraySize:      %02lu",
                  wszOutName,
                    top_crc32,
                      checksum,
                        pDesc->Width, pDesc->Height,
                        pDesc->Format,
                          pDesc->BindFlags,
                            pDesc->Usage,
                              pDesc->CPUAccessFlags,
                                pDesc->MiscFlags,
                                  pDesc->MipLevels,
                                    pDesc->ArraySize );

        fclose (fMetaData);
      }

      return SaveToDDSFile ( image.GetImages (), image.GetImageCount (),
                               image.GetMetadata (), DirectX::DDS_FLAGS_NONE,
                                 wszOutName );
    }
  }

  return E_FAIL;
}

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreateBuffer_Override (
  _In_           ID3D11Device            *This,
  _In_     const D3D11_BUFFER_DESC       *pDesc,
  _In_opt_ const D3D11_SUBRESOURCE_DATA  *pInitialData,
  _Out_opt_      ID3D11Buffer           **ppBuffer )
{
  return D3D11Dev_CreateBuffer_Original (This, pDesc, pInitialData, ppBuffer);
}


#include <unordered_set>

__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreateShaderResourceView_Override (
  _In_           ID3D11Device                     *This,
  _In_           ID3D11Resource                   *pResource,
  _In_opt_ const D3D11_SHADER_RESOURCE_VIEW_DESC  *pDesc,
  _Out_opt_      ID3D11ShaderResourceView        **ppSRView )
{
  HRESULT hr = D3D11Dev_CreateShaderResourceView_Original (This, pResource, pDesc, ppSRView);

  return hr;
}


__declspec (noinline,nothrow)
HRESULT
WINAPI
D3D11Dev_CreateTexture2D_Override (
  _In_            ID3D11Device           *This,
  _In_      const D3D11_TEXTURE2D_DESC   *pDesc,
  _In_opt_  const D3D11_SUBRESOURCE_DATA *pInitialData,
  _Out_opt_       ID3D11Texture2D        **ppTexture2D )
{
  WaitForInitDXGI ();

  if (InterlockedExchangeAdd (&SK_D3D11_tex_init, 0) == FALSE)
    SK_D3D11_InitTextures ();

  bool early_out = false;

  if ((! (SK_D3D11_cache_textures || SK_D3D11_dump_textures || SK_D3D11_inject_textures)) ||
         SK_D3D11_IsTexInjectThread ())
    early_out = true;

  if (early_out)
    return D3D11Dev_CreateTexture2D_Original (This, pDesc, pInitialData, ppTexture2D);

  LARGE_INTEGER load_start = SK_QueryPerf ();

  uint32_t      checksum  = 0;
  uint32_t      cache_tag = 0;
  size_t        size      = 0;

  ID3D11Texture2D* pCachedTex = nullptr;

  bool cacheable = (pInitialData          != nullptr &&
                    pInitialData->pSysMem != nullptr &&
                    pDesc->Width > 0 && pDesc->Height > 0);

  cacheable &=
    (! ((pDesc->BindFlags & D3D11_BIND_DEPTH_STENCIL) ||
        (pDesc->BindFlags & D3D11_BIND_RENDER_TARGET)) ) &&
        (pDesc->CPUAccessFlags == 0x0);

  cacheable &= (ppTexture2D != nullptr);

  const bool dumpable = 
              cacheable && pDesc->Usage != D3D11_USAGE_DYNAMIC &&
                           pDesc->Usage != D3D11_USAGE_STAGING;

  uint32_t top_crc32 = 0x00;
  uint32_t ffx_crc32 = 0x00;

  if (cacheable) {
    checksum = crc32_tex (pDesc, pInitialData, &size, &top_crc32);

    if (SK_D3D11_inject_textures_ffx) {
      ffx_crc32 = crc32_ffx (pDesc, pInitialData, &size);
    }

    const bool injectable = (
           checksum != 0x00 &&
            ( SK_D3D11_IsInjectable     (top_crc32, checksum) ||
              SK_D3D11_IsInjectable     (top_crc32, 0x00)     ||
              SK_D3D11_IsInjectable_FFX (ffx_crc32)
            )
         );

    if ( checksum != 0x00 &&
         ( SK_D3D11_cache_textures ||
           injectable
         )
       )
    {
      // If this isn't an injectable texture, then filter out non-mipmapped
      //   textures.
      if ((! injectable) && cache_opts.ignore_non_mipped)
        cacheable &= pDesc->MipLevels > 1;

      cache_tag  = crc32c (top_crc32, (uint8_t *)pDesc, sizeof D3D11_TEXTURE2D_DESC);
      pCachedTex = SK_D3D11_Textures.getTexture2D (cache_tag, pDesc);
    } else {
      cacheable = false;
    }
  }

  if (pCachedTex != nullptr) {
    //dll_log.Log ( L"[DX11TexMgr] >> Redundant 2D Texture Load "
                  //L" (Hash=0x%08X [%05.03f MiB]) <<",
                  //checksum, (float)size / (1024.0f * 1024.0f) );
    pCachedTex->AddRef ();
    *ppTexture2D = pCachedTex;
    return S_OK;
  }

  SK_D3D11_Textures.CacheMisses_2D++;

  if (cacheable) {
    if (D3DX11CreateTextureFromFileW != nullptr && SK_D3D11_res_root.length ()) {
      wchar_t wszTex [MAX_PATH + 2] = { L'\0' };

      
      wcscpy ( wszTex,
                SK_EvalEnvironmentVars (SK_D3D11_res_root.c_str ()).c_str () );

      if (SK_D3D11_IsTexHashed (ffx_crc32, 0x00)) {
        SK_LOG4 ( ( L"Caching texture with crc32: %x", ffx_crc32 ),
                    L" Tex Hash " );
        _swprintf ( wszTex, L"%s\\%s",
                      wszTex,
                        SK_D3D11_TexHashToName (ffx_crc32, 0x00).c_str ()
        );
      }

      else if (SK_D3D11_IsTexHashed (top_crc32, checksum)) {
        SK_LOG4 ( ( L"Caching texture with crc32c: %x", top_crc32 ),
                    L"Tex Hash " );
        _swprintf ( wszTex, L"%s\\%s",
                      wszTex,
                        SK_D3D11_TexHashToName (top_crc32,checksum).c_str ()
                  );
      }

      else if (SK_D3D11_IsTexHashed (top_crc32, 0x00)) {
        SK_LOG4 ( ( L"Caching texture with crc32c: %x", top_crc32 ),
                    L" Tex Hash " );
        _swprintf ( wszTex, L"%s\\%s",
                      wszTex,
                        SK_D3D11_TexHashToName (top_crc32, 0x00).c_str ()
                  );
      }

      else if ( /*config.textures.d3d11.precise_hash &&*/
                SK_D3D11_inject_textures           &&
                SK_D3D11_IsInjectable (top_crc32, checksum) ) {
        _swprintf ( wszTex,
                      L"%s\\inject\\textures\\%08X_%08X.dds",
                        wszTex,
                          top_crc32, checksum );
      }

      else if ( SK_D3D11_inject_textures &&
                SK_D3D11_IsInjectable (top_crc32, 0x00) ) {
        SK_LOG4 ( ( L"Caching texture with crc32c: %08X", top_crc32 ),
                    L" Tex Hash " );
        _swprintf ( wszTex,
                      L"%s\\inject\\textures\\%08X.dds",
                        wszTex,
                          top_crc32 );
      }

      else if ( SK_D3D11_inject_textures           &&
                SK_D3D11_IsInjectable_FFX (ffx_crc32) ) {
        SK_LOG4 ( ( L"Caching texture with crc32: %08X", ffx_crc32 ),
                    L" Tex Hash " );
        _swprintf ( wszTex,
                      L"%s\\inject\\textures\\Unx_Old\\%08X.dds",
                        wszTex,
                          ffx_crc32 );
      }

      // Not a hashed texture, not an injectable texture, skip it...
      else *wszTex = L'\0';

      if (                   *wszTex  != L'\0' &&
           GetFileAttributes (wszTex) != INVALID_FILE_ATTRIBUTES )
      {
        SK_AutoCriticalSection inject_critical (&inject_cs);

      //ID3D11Resource* pRes = nullptr;

#define D3DX11_DEFAULT -1

        D3DX11_IMAGE_INFO      img_info   = { 0 };
        D3DX11_IMAGE_LOAD_INFO load_info  = { 0 };

        D3DX11GetImageInfoFromFileW (wszTex, nullptr, &img_info, nullptr);

        load_info.BindFlags      = pDesc->BindFlags;
        load_info.CpuAccessFlags = pDesc->CPUAccessFlags;
        load_info.Depth          = img_info.Depth;//D3DX11_DEFAULT;
        load_info.Filter         = (UINT)D3DX11_DEFAULT;
        load_info.FirstMipLevel  = 0;
        load_info.Format         = pDesc->Format;
        load_info.Height         = img_info.Height;//D3DX11_DEFAULT;
        load_info.MipFilter      = (UINT)D3DX11_DEFAULT;
        load_info.MipLevels      = img_info.MipLevels;//D3DX11_DEFAULT;
        load_info.MiscFlags      = img_info.MiscFlags;//pDesc->MiscFlags;
        load_info.pSrcInfo       = &img_info;
        load_info.Usage          = pDesc->Usage;
        load_info.Width          = img_info.Width;//D3DX11_DEFAULT;

        SK_D3D11_SetTexInjectThread ();

        if ( SUCCEEDED ( D3DX11CreateTextureFromFileW (
                           This, wszTex,
                             &load_info, nullptr,
             (ID3D11Resource**)ppTexture2D, nullptr )
                       )
           )
        {
          LARGE_INTEGER load_end = SK_QueryPerf ();

          SK_D3D11_ClearTexInjectThread ();

          SK_D3D11_Textures.refTexture2D (
            *ppTexture2D,
              pDesc,
                cache_tag,
                  size,
                    load_end.QuadPart - load_start.QuadPart
          );

          return S_OK;
        }

        SK_D3D11_ClearTexInjectThread ();
      }
    }
  }

  HRESULT ret =
    D3D11Dev_CreateTexture2D_Original (This, pDesc, pInitialData, ppTexture2D);

  if (ppTexture2D != nullptr) {
    static volatile ULONG init = FALSE;

    if (! InterlockedCompareExchange (&init, TRUE, FALSE)) {
      DXGI_VIRTUAL_HOOK ( ppTexture2D, 2, "IUnknown::Release",
                          IUnknown_Release,
                          IUnknown_Release_Original,
                          IUnknown_Release_pfn );
      DXGI_VIRTUAL_HOOK ( ppTexture2D, 1, "IUnknown::AddRef",
                          IUnknown_AddRef,
                          IUnknown_AddRef_Original,
                          IUnknown_AddRef_pfn );

      MH_ApplyQueued ();
    }
  }

  LARGE_INTEGER load_end = SK_QueryPerf ();

  if ( SUCCEEDED (ret) &&
          dumpable     &&
      checksum != 0x00 &&
      SK_D3D11_dump_textures )
  {
    if (! SK_D3D11_IsDumped (top_crc32, checksum)) {
      SK_D3D11_DumpTexture2D (pDesc, pInitialData, top_crc32, checksum);
    }
  }

  cacheable &=
    (SK_D3D11_cache_textures || SK_D3D11_IsInjectable (top_crc32, checksum));

  if ( SUCCEEDED (ret) && cacheable ) {
    SK_D3D11_Textures.refTexture2D (
      *ppTexture2D,
        pDesc,
          cache_tag,
            size,
              load_end.QuadPart - load_start.QuadPart
    );
  }

  return ret;
}


void
__stdcall
SK_D3D11_UpdateRenderStats (IDXGISwapChain* pSwapChain)
{
  if (! (pSwapChain && config.render.show))
    return;

  CComPtr <ID3D11Device> dev = nullptr;

  if (SUCCEEDED (pSwapChain->GetDevice (IID_PPV_ARGS (&dev)))) {
    CComPtr <ID3D11DeviceContext> dev_ctx = nullptr;

    dev->GetImmediateContext (&dev_ctx);

    if (dev_ctx == nullptr)
      return;

    SK::DXGI::PipelineStatsD3D11& pipeline_stats =
      SK::DXGI::pipeline_stats_d3d11;

    if (pipeline_stats.query.async != nullptr) {
      if (pipeline_stats.query.active) {
        dev_ctx->End (pipeline_stats.query.async);
        pipeline_stats.query.active = false;
      } else {
        HRESULT hr =
          dev_ctx->GetData ( pipeline_stats.query.async,
                              &pipeline_stats.last_results,
                                sizeof D3D11_QUERY_DATA_PIPELINE_STATISTICS,
                                  0x0 );
        if (hr == S_OK) {
          pipeline_stats.query.async->Release ();
          pipeline_stats.query.async = nullptr;
        }
      }
    }

    else {
      D3D11_QUERY_DESC query_desc {
        D3D11_QUERY_PIPELINE_STATISTICS, 0x00
      };

      if (SUCCEEDED (dev->CreateQuery (&query_desc, &pipeline_stats.query.async))) {
        dev_ctx->Begin (pipeline_stats.query.async);
        pipeline_stats.query.active = true;
      }
    }
  }
}

std::wstring
SK_CountToString (uint64_t count)
{
  wchar_t str [64];

  unsigned int unit = 0;

  if      (count > 1000000000UL) unit = 1000000000UL;
  else if (count > 1000000)      unit = 1000000UL;
  else if (count > 1000)         unit = 1000UL;
  else                           unit = 1UL;

  switch (unit)
  {
    case 1000000000UL:
      _swprintf (str, L"%6.2f Billion ", (float)count / (float)unit);
      break;
    case 1000000UL:
      _swprintf (str, L"%6.2f Million ", (float)count / (float)unit);
      break;
    case 1000UL:
      _swprintf (str, L"%6.2f Thousand", (float)count / (float)unit);
      break;
    case 1UL:
    default:
      _swprintf (str, L"%15llu", count);
      break;
  }

  return str;
}

void
SK_D3D11_SetPipelineStats (void* pData)
{
  memcpy ( (void *)&SK::DXGI::pipeline_stats_d3d11.last_results,
             pData,
               sizeof D3D11_QUERY_DATA_PIPELINE_STATISTICS );
}

std::wstring
SK::DXGI::getPipelineStatsDesc (void)
{
  wchar_t wszDesc [1024];

  D3D11_QUERY_DATA_PIPELINE_STATISTICS& stats =
    pipeline_stats_d3d11.last_results;

  //
  // VERTEX SHADING
  //
  if (stats.VSInvocations > 0) {
    _swprintf ( wszDesc,
                  L"  VERTEX : %s   (%s Verts ==> %s Triangles)\n",
                    SK_CountToString (stats.VSInvocations).c_str (),
                      SK_CountToString (stats.IAVertices).c_str (),
                        SK_CountToString (stats.IAPrimitives).c_str () );
  }

  else
  {
    _swprintf ( wszDesc,
                  L"  VERTEX : <Unused>\n" );
  }

  //
  // GEOMETRY SHADING
  //
  if (stats.GSInvocations > 0)
  {
    _swprintf ( wszDesc,
                  L"%s  GEOM   : %s   (%s Prims)\n",
                    wszDesc,
                      SK_CountToString (stats.GSInvocations).c_str (),
                        SK_CountToString (stats.GSPrimitives).c_str () );
  }

  else
  {
    _swprintf ( wszDesc,
                  L"%s  GEOM   : <Unused>\n",
                    wszDesc );
  }

  //
  // TESSELLATION
  //
  if (stats.HSInvocations > 0 || stats.DSInvocations > 0)
  {
    _swprintf ( wszDesc,
                  L"%s  TESS   : %s Hull ==> %s Domain\n",
                    wszDesc,
                      SK_CountToString (stats.HSInvocations).c_str (),
                        SK_CountToString (stats.DSInvocations).c_str () ) ;
  }

  else
  {
    _swprintf ( wszDesc,
                  L"%s  TESS   : <Unused>\n",
                    wszDesc );
  }

  //
  // RASTERIZATION
  //
  if (stats.CInvocations > 0)
  {
    _swprintf ( wszDesc,
                  L"%s  RASTER : %5.1f%% Filled     (%s Triangles IN )\n",
                    wszDesc, 100.0f *
                        ( (float)stats.CPrimitives /
                          (float)stats.CInvocations ),
                      SK_CountToString (stats.CInvocations).c_str () );
  }

  else
  {
    _swprintf ( wszDesc,
                  L"%s  RASTER : <Unused>\n",
                    wszDesc );
  }

  //
  // PIXEL SHADING
  //
  if (stats.PSInvocations > 0)
  {
    _swprintf ( wszDesc,
                  L"%s  PIXEL  : %s   (%s Triangles OUT)\n",
                    wszDesc,
                      SK_CountToString (stats.PSInvocations).c_str (),
                        SK_CountToString (stats.CPrimitives).c_str () );
  }

  else
  {
    _swprintf ( wszDesc,
                  L"%s  PIXEL  : <Unused>\n",
                    wszDesc );
  }

  //
  // COMPUTE
  //
  if (stats.CSInvocations > 0) {
    _swprintf ( wszDesc,
                  L"%s  COMPUTE: %s\n",
                    wszDesc, SK_CountToString (stats.CSInvocations).c_str () );
  } else {
    _swprintf ( wszDesc,
                  L"%s  COMPUTE: <Unused>\n",
                    wszDesc );
  }

  return wszDesc;
}


void
SK_D3D11_InitTextures (void)
{
  if (! InterlockedCompareExchange (&SK_D3D11_tex_init, TRUE, FALSE))
  {
    if ( StrStrIW (SK_GetHostApp (), L"ffx.exe")   ||
         StrStrIW (SK_GetHostApp (), L"ffx-2.exe") ||
         StrStrIW (SK_GetHostApp (), L"FFX&X-2_Will.exe") )
      SK_D3D11_inject_textures_ffx = true;

#ifdef NO_TLS
    InitializeCriticalSectionAndSpinCount (&cs_texinject, 0x4000);
#endif

    InitializeCriticalSectionAndSpinCount (&tex_cs,    MAXDWORD);
    InitializeCriticalSectionAndSpinCount (&hash_cs,   0x4000);
    InitializeCriticalSectionAndSpinCount (&dump_cs,   0x0200);
    InitializeCriticalSectionAndSpinCount (&cache_cs,  MAXDWORD);
    InitializeCriticalSectionAndSpinCount (&inject_cs, 0x1000);

    cache_opts.max_entries       = config.textures.cache.max_entries;
    cache_opts.min_entries       = config.textures.cache.min_entries;
    cache_opts.max_evict         = config.textures.cache.max_evict;
    cache_opts.min_evict         = config.textures.cache.min_evict;
    cache_opts.max_size          = config.textures.cache.max_size;
    cache_opts.min_size          = config.textures.cache.min_size;
    cache_opts.ignore_non_mipped = config.textures.cache.ignore_nonmipped;

    //
    // Legacy Hack for Untitled Project X (FFX/FFX-2)
    //
    extern bool SK_D3D11_inject_textures_ffx;
    if (! SK_D3D11_inject_textures_ffx)
    {
      SK_D3D11_EnableTexCache  (config.textures.d3d11.cache);
      SK_D3D11_EnableTexDump   (config.textures.d3d11.dump);
      SK_D3D11_EnableTexInject (config.textures.d3d11.inject);
      SK_D3D11_SetResourceRoot (config.textures.d3d11.res_root.c_str ());
    }

    SK_GetCommandProcessor ()->AddVariable ("TexCache.Enable",
         new SK_IVarStub <bool> ((bool *)&config.textures.d3d11.cache));
    SK_GetCommandProcessor ()->AddVariable ("TexCache.MaxEntries",
         new SK_IVarStub <int> ((int *)&cache_opts.max_entries));
    SK_GetCommandProcessor ()->AddVariable ("TexCache.MinEntries",
         new SK_IVarStub <int> ((int *)&cache_opts.min_entries));
    SK_GetCommandProcessor ()->AddVariable ("TexCache.MaxSize",
         new SK_IVarStub <int> ((int *)&cache_opts.max_size));
    SK_GetCommandProcessor ()->AddVariable ("TexCache.MinSize",
         new SK_IVarStub <int> ((int *)&cache_opts.min_size));
    SK_GetCommandProcessor ()->AddVariable ("TexCache.MinEvict",
         new SK_IVarStub <int> ((int *)&cache_opts.min_evict));
    SK_GetCommandProcessor ()->AddVariable ("TexCache.MaxEvict",
         new SK_IVarStub <int> ((int *)&cache_opts.max_evict));
    SK_GetCommandProcessor ()->AddVariable ("TexCache.IgnoreNonMipped",
         new SK_IVarStub <bool> ((bool *)&cache_opts.ignore_non_mipped));

    if (! SK_D3D11_inject_textures_ffx)
      SK_D3D11_PopulateResourceList ();

    if (hModD3DX11_43 == nullptr)
    {
      hModD3DX11_43 =
        LoadLibraryW_Original (L"d3dx11_43.dll");

      if (hModD3DX11_43 == nullptr)
        hModD3DX11_43 = (HMODULE)1;
    }

    if (D3DX11CreateTextureFromFileW == nullptr && (uintptr_t)hModD3DX11_43 > 1)
    {
      D3DX11CreateTextureFromFileW =
        (D3DX11CreateTextureFromFileW_pfn)
          GetProcAddress (hModD3DX11_43, "D3DX11CreateTextureFromFileW");
    }

    if (D3DX11GetImageInfoFromFileW == nullptr && (uintptr_t)hModD3DX11_43 > 1)
    {
      D3DX11GetImageInfoFromFileW =
        (D3DX11GetImageInfoFromFileW_pfn)
          GetProcAddress (hModD3DX11_43, "D3DX11GetImageInfoFromFileW");
    }
  }
}

volatile LONG SK_D3D11_initialized = FALSE;

bool
SK_D3D11_Init (void)
{
  BOOL success = FALSE;

  if (! InterlockedCompareExchange (&SK_D3D11_initialized, TRUE, FALSE))
  {
    SK::DXGI::hModD3D11 =
      LoadLibraryW_Original (L"d3d11.dll");

    if ( MH_OK ==
           SK_CreateDLLHook2 ( L"d3d11.dll",
                                "D3D11CreateDevice",
                                 D3D11CreateDevice_Detour,
                      (LPVOID *)&D3D11CreateDevice_Import,
                             &pfnD3D11CreateDevice )
       )
    {
      if ( MH_OK ==
             SK_CreateDLLHook2 ( L"d3d11.dll",
                                  "D3D11CreateDeviceAndSwapChain",
                                   D3D11CreateDeviceAndSwapChain_Detour,
                        (LPVOID *)&D3D11CreateDeviceAndSwapChain_Import,
                               &pfnD3D11CreateDeviceAndSwapChain )
         )
      {
        if ( MH_OK == MH_QueueEnableHook (pfnD3D11CreateDevice) &&
             MH_OK == MH_QueueEnableHook (pfnD3D11CreateDeviceAndSwapChain) )
        {
          success = (MH_OK == SK_ApplyQueuedHooks ());
        }
      }
    }
  }

  return success;
}

void
SK_D3D11_Shutdown (void)
{
  if (! InterlockedCompareExchange (&SK_D3D11_initialized, FALSE, TRUE))
    return;

  if (SK_D3D11_Textures.RedundantLoads_2D > 0)
  {
    dll_log.Log ( L"[Perf Stats] At shutdown: %7.2f seconds and %7.2f MiB of"
                  L" CPU->GPU I/O avoided by %lu texture cache hits.",
                    SK_D3D11_Textures.RedundantTime_2D / 1000.0f,
                      (float)SK_D3D11_Textures.RedundantData_2D /
                                 (1024.0f * 1024.0f),
                        SK_D3D11_Textures.RedundantLoads_2D );
  }

#if 0
  SK_D3D11_Textures.reset ();

  // Stop caching while we shutdown
  SK_D3D11_cache_textures = false;

  if (FreeLibrary_Original (SK::DXGI::hModD3D11))
  {
    DeleteCriticalSection (&tex_cs);
    DeleteCriticalSection (&hash_cs);
    DeleteCriticalSection (&dump_cs);
    DeleteCriticalSection (&cache_cs);
    DeleteCriticalSection (&inject_cs);

#ifdef NO_TLS
    DeleteCriticalSection (&cs_texinject);
#endif
  }
#endif
}

void
SK_D3D11_EnableHooks (void)
{
  InterlockedExchange (&__d3d11_ready, TRUE);
}


extern
unsigned int __stdcall HookD3D12                   (LPVOID user);

volatile ULONG __d3d11_hooked = FALSE;

struct sk_hook_d3d11_t {
 ID3D11Device**        ppDevice;
 ID3D11DeviceContext** ppImmediateContext;
};

__declspec (nothrow)
unsigned int
__stdcall
HookD3D11 (LPVOID user)
{
  // Wait for DXGI to boot
  if (CreateDXGIFactory_Import == nullptr)
  {
    static volatile ULONG implicit_init = FALSE;

    // If something called a D3D11 function before DXGI was initialized,
    //   begin the process, but ... only do this once.
    if (! InterlockedCompareExchange (&implicit_init, TRUE, FALSE))
    {
      dll_log.Log (L"[  D3D 11  ]  >> Implicit Initialization Triggered <<");
      SK_BootDXGI ();
    }

    while (CreateDXGIFactory_Import == nullptr)
      Sleep (33);

    // TODO: Handle situation where CreateDXGIFactory is unloadable
  }

  // This only needs to be done once
  if (InterlockedCompareExchange (&__d3d11_hooked, TRUE, FALSE))
    return 0;

  if (! config.apis.dxgi.d3d11.hook)
    return 0;

  bool success = SUCCEEDED (CoInitializeEx (nullptr, COINIT_MULTITHREADED));

  dll_log.Log (L"[  D3D 11  ]   Hooking D3D11");

  sk_hook_d3d11_t* pHooks = 
    (sk_hook_d3d11_t *)user;

  if (pHooks->ppDevice && pHooks->ppImmediateContext)
  {
    if (pHooks->ppDevice != nullptr)
    {
      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 3, "ID3D11Device::CreateBuffer",
                             D3D11Dev_CreateBuffer_Override, D3D11Dev_CreateBuffer_Original,
                             D3D11Dev_CreateBuffer_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 5, "ID3D11Device::CreateTexture2D",
                             D3D11Dev_CreateTexture2D_Override, D3D11Dev_CreateTexture2D_Original,
                             D3D11Dev_CreateTexture2D_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 7, "ID3D11Device::CreateShaderResourceView",
                             D3D11Dev_CreateShaderResourceView_Override, D3D11Dev_CreateShaderResourceView_Original,
                             D3D11Dev_CreateShaderResourceView_pfn);

// Don't need it, so don't hook it.
#if 0
      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 9, "ID3D11Device::CreateRenderTargetView",
                             D3D11Dev_CreateRenderTargetView_Override, D3D11Dev_CreateRenderTargetView_Original,
                             D3D11Dev_CreateRenderTargetView_pfn);
#endif

      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 12, "ID3D11Device::CreateVertexShader",
                             D3D11Dev_CreateVertexShader_Override, D3D11Dev_CreateVertexShader_Original,
                             D3D11Dev_CreateVertexShader_pfn);
      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 13, "ID3D11Device::CreateGeometryShader",
                             D3D11Dev_CreateGeometryShader_Override, D3D11Dev_CreateGeometryShader_Original,
                             D3D11Dev_CreateGeometryShader_pfn);
      //DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 14, "ID3D11Device::CreateGeometryShaderWithStreamOutput",
      //                       D3D11Dev_CreateGeometryShaderWithStreamOutput_Override, D3D11Dev_CreateGeometryShaderWithStreamOutput_Original,
      //                       D3D11Dev_CreateGeometryShaderWithStreamOutput_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 15, "ID3D11Device::CreatePixelShader",
                             D3D11Dev_CreatePixelShader_Override, D3D11Dev_CreatePixelShader_Original,
                             D3D11Dev_CreatePixelShader_pfn);
      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 16, "ID3D11Device::CreateHullShader",
                             D3D11Dev_CreateHullShader_Override, D3D11Dev_CreateHullShader_Original,
                             D3D11Dev_CreateHullShader_pfn);
      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 17, "ID3D11Device::CreateDomainShader",
                             D3D11Dev_CreateDomainShader_Override, D3D11Dev_CreateDomainShader_Original,
                             D3D11Dev_CreateDomainShader_pfn);
      DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 18, "ID3D11Device::CreateComputeShader",
                             D3D11Dev_CreateComputeShader_Override, D3D11Dev_CreateComputeShader_Original,
                             D3D11Dev_CreateComputeShader_pfn);

      //DXGI_VIRTUAL_HOOK (pHooks->ppDevice, 19, "ID3D11Device::CreateClassLinkage",
      //                       D3D11Dev_CreateClassLinkage_Override, D3D11Dev_CreateClassLinkage_Original,
      //                       D3D11Dev_CreateClassLinkage_pfn);
    }

    if (pHooks->ppImmediateContext != nullptr)
    {
      //
      // Third-party software frequently causes these hooks to become corrupted, try installing a new
      //   vftable pointer instead of hooking the function.
      //
#if 0
      DXGI_VIRTUAL_OVERRIDE (pHooks->ppImmediateContext, 7, "ID3D11DeviceContext::VSSetConstantBuffers",
                             D3D11_VSSetConstantBuffers_Override, D3D11_VSSetConstantBuffers_Original,
                             D3D11_VSSetConstantBuffers_pfn);
#else
      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 7, "ID3D11DeviceContext::VSSetConstantBuffers",
                             D3D11_VSSetConstantBuffers_Override, D3D11_VSSetConstantBuffers_Original,
                             D3D11_VSSetConstantBuffers_pfn);
#endif

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 8, "ID3D11DeviceContext::PSSetShaderResources",
                             D3D11_PSSetShaderResources_Override, D3D11_PSSetShaderResources_Original,
                             D3D11_PSSetShaderResources_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 9, "ID3D11DeviceContext::PSSetShader",
                           D3D11_PSSetShader_Override, D3D11_PSSetShader_Original,
                           D3D11_PSSetShader_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 11, "ID3D11DeviceContext::VSSetShader",
                           D3D11_VSSetShader_Override, D3D11_VSSetShader_Original,
                           D3D11_VSSetShader_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 12, "ID3D11DeviceContext::DrawIndexed",
                           D3D11_DrawIndexed_Override, D3D11_DrawIndexed_Original,
                           D3D11_DrawIndexed_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 13, "ID3D11DeviceContext::Draw",
                           D3D11_Draw_Override, D3D11_Draw_Original,
                           D3D11_Draw_pfn);

      //
      // Third-party software frequently causes these hooks to become corrupted, try installing a new
      //   vftable pointer instead of hooking the function.
      //
#if 0
      DXGI_VIRTUAL_OVERRIDE (pHooks->ppImmediateContext, 14, "ID3D11DeviceContext::Map",
                           D3D11_Map_Override, D3D11_Map_Original,
                           D3D11_Map_pfn);
#else
      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 14, "ID3D11DeviceContext::Map",
                           D3D11_Map_Override, D3D11_Map_Original,
                           D3D11_Map_pfn);
#endif

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 20, "ID3D11DeviceContext::DrawIndexedInstanced",
                           D3D11_DrawIndexedInstanced_Override, D3D11_DrawIndexedInstanced_Original,
                           D3D11_DrawIndexedInstanced_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 21, "ID3D11DeviceContext::DrawInstanced",
                           D3D11_DrawInstanced_Override, D3D11_DrawInstanced_Original,
                           D3D11_DrawInstanced_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 23, "ID3D11DeviceContext::GSSetShader",
                           D3D11_GSSetShader_Override, D3D11_GSSetShader_Original,
                           D3D11_GSSetShader_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 25, "ID3D11DeviceContext::VSSetShaderResources",
                             D3D11_VSSetShaderResources_Override, D3D11_VSSetShaderResources_Original,
                             D3D11_VSSetShaderResources_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 31, "ID3D11DeviceContext::GSSetShaderResources",
                             D3D11_GSSetShaderResources_Override, D3D11_GSSetShaderResources_Original,
                             D3D11_GSSetShaderResources_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 38, "ID3D11DeviceContext::DrawAuto",
                           D3D11_DrawAuto_Override, D3D11_DrawAuto_Original,
                           D3D11_DrawAuto_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 39, "ID3D11DeviceContext::DrawIndexedInstancedIndirect",
                           D3D11_DrawIndexedInstancedIndirect_Override, D3D11_DrawIndexedInstancedIndirect_Original,
                           D3D11_DrawIndexedInstancedIndirect_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 40, "ID3D11DeviceContext::DrawInstancedIndirect",
                           D3D11_DrawInstancedIndirect_Override, D3D11_DrawInstancedIndirect_Original,
                           D3D11_DrawInstancedIndirect_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 41, "ID3D11DeviceContext::Dispatch",
                           D3D11_Dispatch_Override, D3D11_Dispatch_Original,
                           D3D11_Dispatch_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 42, "ID3D11DeviceContext::DispatchIndirect",
                           D3D11_DispatchIndirect_Override, D3D11_DispatchIndirect_Original,
                           D3D11_DispatchIndirect_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 44, "ID3D11DeviceContext::RSSetViewports",
                           D3D11_RSSetViewports_Override, D3D11_RSSetViewports_Original,
                           D3D11_RSSetViewports_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 45, "ID3D11DeviceContext::RSSetScissorRects",
                           D3D11_RSSetScissorRects_Override, D3D11_RSSetScissorRects_Original,
                           D3D11_RSSetScissorRects_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 47, "ID3D11DeviceContext::CopyResource",
                           D3D11_CopyResource_Override, D3D11_CopyResource_Original,
                           D3D11_CopyResource_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 48, "ID3D11DeviceContext::UpdateSubresource",
                           D3D11_UpdateSubresource_Override, D3D11_UpdateSubresource_Original,
                           D3D11_UpdateSubresource_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 59, "ID3D11DeviceContext::HSSetShaderResources",
                             D3D11_HSSetShaderResources_Override, D3D11_HSSetShaderResources_Original,
                             D3D11_HSSetShaderResources_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 60, "ID3D11DeviceContext::HSSetShader",
                           D3D11_HSSetShader_Override, D3D11_HSSetShader_Original,
                           D3D11_HSSetShader_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 63, "ID3D11DeviceContext::DSSetShaderResources",
                             D3D11_DSSetShaderResources_Override, D3D11_DSSetShaderResources_Original,
                             D3D11_DSSetShaderResources_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 64, "ID3D11DeviceContext::DSSetShader",
                           D3D11_DSSetShader_Override, D3D11_DSSetShader_Original,
                           D3D11_DSSetShader_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 67, "ID3D11DeviceContext::CSSetShaderResources",
                           D3D11_CSSetShaderResources_Override, D3D11_CSSetShaderResources_Original,
                           D3D11_CSSetShaderResources_pfn);

      DXGI_VIRTUAL_HOOK (pHooks->ppImmediateContext, 69, "ID3D11DeviceContext::CSSetShader",
                           D3D11_CSSetShader_Override, D3D11_CSSetShader_Original,
                           D3D11_CSSetShader_pfn);
    }

    MH_ApplyQueued ();
  }

  if (config.apis.dxgi.d3d12.hook)
    HookD3D12 (nullptr);

  if (success)
    CoUninitialize ();

  return 0;
}





struct shader_disasm_s {
  std::string header;
  std::string code;
  std::string footer;
};

#include <d3d9.h>

//---------------------------------------------------------------------------
// D3DXTX_VERSION:
// --------------
// Version token used to create a procedural texture filler in effects
// Used by D3DXFill[]TX functions
//---------------------------------------------------------------------------
#define D3DXTX_VERSION(_Major,_Minor) (('T' << 24) | ('X' << 16) | ((_Major) << 8) | (_Minor))



//----------------------------------------------------------------------------
// D3DXSHADER flags:
// -----------------
// D3DXSHADER_DEBUG
//   Insert debug file/line/type/symbol information.
//
// D3DXSHADER_SKIPVALIDATION
//   Do not validate the generated code against known capabilities and
//   constraints.  This option is only recommended when compiling shaders
//   you KNOW will work.  (ie. have compiled before without this option.)
//   Shaders are always validated by D3D before they are set to the device.
//
// D3DXSHADER_SKIPOPTIMIZATION 
//   Instructs the compiler to skip optimization steps during code generation.
//   Unless you are trying to isolate a problem in your code using this option 
//   is not recommended.
//
// D3DXSHADER_PACKMATRIX_ROWMAJOR
//   Unless explicitly specified, matrices will be packed in row-major order
//   on input and output from the shader.
//
// D3DXSHADER_PACKMATRIX_COLUMNMAJOR
//   Unless explicitly specified, matrices will be packed in column-major 
//   order on input and output from the shader.  This is generally more 
//   efficient, since it allows vector-matrix multiplication to be performed
//   using a series of dot-products.
//
// D3DXSHADER_PARTIALPRECISION
//   Force all computations in resulting shader to occur at partial precision.
//   This may result in faster evaluation of shaders on some hardware.
//
// D3DXSHADER_FORCE_VS_SOFTWARE_NOOPT
//   Force compiler to compile against the next highest available software
//   target for vertex shaders.  This flag also turns optimizations off, 
//   and debugging on.  
//
// D3DXSHADER_FORCE_PS_SOFTWARE_NOOPT
//   Force compiler to compile against the next highest available software
//   target for pixel shaders.  This flag also turns optimizations off, 
//   and debugging on.
//
// D3DXSHADER_NO_PRESHADER
//   Disables Preshaders. Using this flag will cause the compiler to not 
//   pull out static expression for evaluation on the host cpu
//
// D3DXSHADER_AVOID_FLOW_CONTROL
//   Hint compiler to avoid flow-control constructs where possible.
//
// D3DXSHADER_PREFER_FLOW_CONTROL
//   Hint compiler to prefer flow-control constructs where possible.
//
//----------------------------------------------------------------------------

#define D3DXSHADER_DEBUG                          (1 << 0)
#define D3DXSHADER_SKIPVALIDATION                 (1 << 1)
#define D3DXSHADER_SKIPOPTIMIZATION               (1 << 2)
#define D3DXSHADER_PACKMATRIX_ROWMAJOR            (1 << 3)
#define D3DXSHADER_PACKMATRIX_COLUMNMAJOR         (1 << 4)
#define D3DXSHADER_PARTIALPRECISION               (1 << 5)
#define D3DXSHADER_FORCE_VS_SOFTWARE_NOOPT        (1 << 6)
#define D3DXSHADER_FORCE_PS_SOFTWARE_NOOPT        (1 << 7)
#define D3DXSHADER_NO_PRESHADER                   (1 << 8)
#define D3DXSHADER_AVOID_FLOW_CONTROL             (1 << 9)
#define D3DXSHADER_PREFER_FLOW_CONTROL            (1 << 10)
#define D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY (1 << 12)
#define D3DXSHADER_IEEE_STRICTNESS                (1 << 13)
#define D3DXSHADER_USE_LEGACY_D3DX9_31_DLL        (1 << 16)


// optimization level flags
#define D3DXSHADER_OPTIMIZATION_LEVEL0            (1 << 14)
#define D3DXSHADER_OPTIMIZATION_LEVEL1            0
#define D3DXSHADER_OPTIMIZATION_LEVEL2            ((1 << 14) | (1 << 15))
#define D3DXSHADER_OPTIMIZATION_LEVEL3            (1 << 15)



//----------------------------------------------------------------------------
// D3DXCONSTTABLE flags:
// -------------------

#define D3DXCONSTTABLE_LARGEADDRESSAWARE          (1 << 17)



//----------------------------------------------------------------------------
// D3DXHANDLE:
// -----------
// Handle values used to efficiently reference shader and effect parameters.
// Strings can be used as handles.  However, handles are not always strings.
//----------------------------------------------------------------------------

#ifndef D3DXFX_LARGEADDRESS_HANDLE
typedef LPCSTR D3DXHANDLE;
#else
typedef UINT_PTR D3DXHANDLE;
#endif
typedef D3DXHANDLE *LPD3DXHANDLE;


//----------------------------------------------------------------------------
// D3DXMACRO:
// ----------
// Preprocessor macro definition.  The application pass in a NULL-terminated
// array of this structure to various D3DX APIs.  This enables the application
// to #define tokens at runtime, before the file is parsed.
//----------------------------------------------------------------------------

typedef struct _D3DXMACRO
{
    LPCSTR Name;
    LPCSTR Definition;

} D3DXMACRO, *LPD3DXMACRO;


//----------------------------------------------------------------------------
// D3DXSEMANTIC:
//----------------------------------------------------------------------------

typedef struct _D3DXSEMANTIC
{
    UINT Usage;
    UINT UsageIndex;

} D3DXSEMANTIC, *LPD3DXSEMANTIC;



//----------------------------------------------------------------------------
// D3DXREGISTER_SET:
//----------------------------------------------------------------------------

typedef enum _D3DXREGISTER_SET
{
    D3DXRS_BOOL,
    D3DXRS_INT4,
    D3DXRS_FLOAT4,
    D3DXRS_SAMPLER,

    // force 32-bit size enum
    D3DXRS_FORCE_DWORD = 0x7fffffff

} D3DXREGISTER_SET, *LPD3DXREGISTER_SET;


//----------------------------------------------------------------------------
// D3DXPARAMETER_CLASS:
//----------------------------------------------------------------------------

typedef enum _D3DXPARAMETER_CLASS
{
    D3DXPC_SCALAR,
    D3DXPC_VECTOR,
    D3DXPC_MATRIX_ROWS,
    D3DXPC_MATRIX_COLUMNS,
    D3DXPC_OBJECT,
    D3DXPC_STRUCT,

    // force 32-bit size enum
    D3DXPC_FORCE_DWORD = 0x7fffffff

} D3DXPARAMETER_CLASS, *LPD3DXPARAMETER_CLASS;


//----------------------------------------------------------------------------
// D3DXPARAMETER_TYPE:
//----------------------------------------------------------------------------

typedef enum _D3DXPARAMETER_TYPE
{
    D3DXPT_VOID,
    D3DXPT_BOOL,
    D3DXPT_INT,
    D3DXPT_FLOAT,
    D3DXPT_STRING,
    D3DXPT_TEXTURE,
    D3DXPT_TEXTURE1D,
    D3DXPT_TEXTURE2D,
    D3DXPT_TEXTURE3D,
    D3DXPT_TEXTURECUBE,
    D3DXPT_SAMPLER,
    D3DXPT_SAMPLER1D,
    D3DXPT_SAMPLER2D,
    D3DXPT_SAMPLER3D,
    D3DXPT_SAMPLERCUBE,
    D3DXPT_PIXELSHADER,
    D3DXPT_VERTEXSHADER,
    D3DXPT_PIXELFRAGMENT,
    D3DXPT_VERTEXFRAGMENT,
    D3DXPT_UNSUPPORTED,

    // force 32-bit size enum
    D3DXPT_FORCE_DWORD = 0x7fffffff

} D3DXPARAMETER_TYPE, *LPD3DXPARAMETER_TYPE;


//----------------------------------------------------------------------------
// D3DXCONSTANTTABLE_DESC:
//----------------------------------------------------------------------------

typedef struct _D3DXCONSTANTTABLE_DESC
{
    LPCSTR Creator;                     // Creator string
    DWORD Version;                      // Shader version
    UINT Constants;                     // Number of constants

} D3DXCONSTANTTABLE_DESC, *LPD3DXCONSTANTTABLE_DESC;


//----------------------------------------------------------------------------
// D3DXCONSTANT_DESC:
//----------------------------------------------------------------------------

typedef struct _D3DXCONSTANT_DESC
{
    LPCSTR Name;                        // Constant name

    D3DXREGISTER_SET RegisterSet;       // Register set
    UINT RegisterIndex;                 // Register index
    UINT RegisterCount;                 // Number of registers occupied

    D3DXPARAMETER_CLASS Class;          // Class
    D3DXPARAMETER_TYPE Type;            // Component type

    UINT Rows;                          // Number of rows
    UINT Columns;                       // Number of columns
    UINT Elements;                      // Number of array elements
    UINT StructMembers;                 // Number of structure member sub-parameters

    UINT Bytes;                         // Data size, in bytes
    LPCVOID DefaultValue;               // Pointer to default value

} D3DXCONSTANT_DESC, *LPD3DXCONSTANT_DESC;



//----------------------------------------------------------------------------
// ID3DXConstantTable:
//----------------------------------------------------------------------------

typedef interface ID3DXConstantTable ID3DXConstantTable;
typedef interface ID3DXConstantTable *LPD3DXCONSTANTTABLE;

// {AB3C758F-093E-4356-B762-4DB18F1B3A01}
DEFINE_GUID(IID_ID3DXConstantTable, 
0xab3c758f, 0x93e, 0x4356, 0xb7, 0x62, 0x4d, 0xb1, 0x8f, 0x1b, 0x3a, 0x1);


#undef INTERFACE
#define INTERFACE ID3DXConstantTable

DECLARE_INTERFACE_(ID3DXConstantTable, IUnknown)
{
    // IUnknown
    STDMETHOD(QueryInterface)(THIS_ REFIID iid, LPVOID *ppv) PURE;
    STDMETHOD_(ULONG, AddRef)(THIS) PURE;
    STDMETHOD_(ULONG, Release)(THIS) PURE;

    // Buffer
    STDMETHOD_(LPVOID, GetBufferPointer)(THIS) PURE;
    STDMETHOD_(DWORD, GetBufferSize)(THIS) PURE;

    // Descs
    STDMETHOD(GetDesc)(THIS_ D3DXCONSTANTTABLE_DESC *pDesc) PURE;
    STDMETHOD(GetConstantDesc)(THIS_ D3DXHANDLE hConstant, D3DXCONSTANT_DESC *pConstantDesc, UINT *pCount) PURE;
    STDMETHOD_(UINT, GetSamplerIndex)(THIS_ D3DXHANDLE hConstant) PURE;

    // Handle operation
    STDMETHOD_(D3DXHANDLE, GetConstant)(THIS_ D3DXHANDLE hConstant, UINT Index) PURE;
    STDMETHOD_(D3DXHANDLE, GetConstantByName)(THIS_ D3DXHANDLE hConstant, LPCSTR pName) PURE;
    STDMETHOD_(D3DXHANDLE, GetConstantElement)(THIS_ D3DXHANDLE hConstant, UINT Index) PURE;

    // Set Constants
    STDMETHOD(SetDefaults)(THIS_ LPDIRECT3DDEVICE9 pDevice) PURE;
    STDMETHOD(SetValue)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, LPCVOID pData, UINT Bytes) PURE;
    STDMETHOD(SetBool)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, BOOL b) PURE;
    STDMETHOD(SetBoolArray)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST BOOL* pb, UINT Count) PURE;
    STDMETHOD(SetInt)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, INT n) PURE;
    STDMETHOD(SetIntArray)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST INT* pn, UINT Count) PURE;
    STDMETHOD(SetFloat)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, FLOAT f) PURE;
    STDMETHOD(SetFloatArray)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST FLOAT* pf, UINT Count) PURE;
    STDMETHOD(SetVector)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST LPVOID pVector) PURE;
    STDMETHOD(SetVectorArray)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST LPVOID pVector, UINT Count) PURE;
    STDMETHOD(SetMatrix)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST LPVOID pMatrix) PURE;
    STDMETHOD(SetMatrixArray)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST LPVOID pMatrix, UINT Count) PURE;
    STDMETHOD(SetMatrixPointerArray)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST LPVOID* ppMatrix, UINT Count) PURE;
    STDMETHOD(SetMatrixTranspose)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST LPVOID pMatrix) PURE;
    STDMETHOD(SetMatrixTransposeArray)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST LPVOID pMatrix, UINT Count) PURE;
    STDMETHOD(SetMatrixTransposePointerArray)(THIS_ LPDIRECT3DDEVICE9 pDevice, D3DXHANDLE hConstant, CONST LPVOID* ppMatrix, UINT Count) PURE;
};


typedef interface ID3DXBuffer ID3DXBuffer;
typedef interface ID3DXBuffer *LPD3DXBUFFER;

// {8BA5FB08-5195-40e2-AC58-0D989C3A0102}
DEFINE_GUID(IID_ID3DXBuffer, 
0x8ba5fb08, 0x5195, 0x40e2, 0xac, 0x58, 0xd, 0x98, 0x9c, 0x3a, 0x1, 0x2);

#undef INTERFACE
#define INTERFACE ID3DXBuffer

DECLARE_INTERFACE_(ID3DXBuffer, IUnknown)
{
    // IUnknown
    STDMETHOD  (        QueryInterface)   (THIS_ REFIID iid, LPVOID *ppv) PURE;
    STDMETHOD_ (ULONG,  AddRef)           (THIS) PURE;
    STDMETHOD_ (ULONG,  Release)          (THIS) PURE;

    // ID3DXBuffer
    STDMETHOD_ (LPVOID, GetBufferPointer) (THIS) PURE;
    STDMETHOD_ (DWORD,  GetBufferSize)    (THIS) PURE;
};

typedef HRESULT (WINAPI *D3DXGetShaderConstantTable_pfn)
( _In_  const DWORD                *pFunction,
  _Out_       LPD3DXCONSTANTTABLE *ppConstantTable
);

#include <imgui/imgui.h>
#include <imgui/backends/imgui_d3d11.h>

enum class sk_shader_class {
  Unknown  = 0x00,
  Vertex   = 0x01,
  Pixel    = 0x02,
  Geometry = 0x04,
  Hull     = 0x08,
  Domain   = 0x10,
  Compute  = 0x20
};

std::unordered_map <uint32_t, shader_disasm_s> vs_disassembly;
std::unordered_map <uint32_t, shader_disasm_s> ps_disassembly;
std::unordered_map <uint32_t, shader_disasm_s> gs_disassembly;
std::unordered_map <uint32_t, shader_disasm_s> hs_disassembly;
std::unordered_map <uint32_t, shader_disasm_s> ds_disassembly;
std::unordered_map <uint32_t, shader_disasm_s> cs_disassembly;

void
SK_LiveShaderClassView (sk_shader_class shader_type, bool& can_scroll)
{
  if (hModD3DX11_43 == nullptr)
  {
    hModD3DX11_43 =
      LoadLibraryW_Original (L"d3dx11_43.dll");

    if (hModD3DX11_43 == nullptr)
      hModD3DX11_43 = (HMODULE)1;

    else
    {
      D3DXGetShaderConstantTable_pfn D3DXGetShaderConstantTable =
        (D3DXGetShaderConstantTable_pfn)
          GetProcAddress ( hModD3DX11_43,
                             "D3DXGetShaderConstantTable" );
    }
  }

  ImGui::BeginGroup ();

  static float last_width = 256.0f;
  const  float font_size  = ImGui::GetFont ()->FontSize * ImGui::GetIO ().FontGlobalScale;

  struct shader_class_imp_s
  {
    std::vector <std::string> contents;
    bool                      dirty      = true;
    uint32_t                  last_sel   =    0;
    int                            sel   =   -1;
    float                     last_ht    = 256.0f;
    ImVec2                    last_min   = ImVec2 (0.0f, 0.0f);
    ImVec2                    last_max   = ImVec2 (0.0f, 0.0f);
  };

  struct {
    shader_class_imp_s vs;
    shader_class_imp_s ps;
    shader_class_imp_s gs;
    shader_class_imp_s hs;
    shader_class_imp_s ds;
    shader_class_imp_s cs;
  } static list_base;

  auto GetShaderList =
    [](sk_shader_class& type) ->
      shader_class_imp_s*
      {
        switch (type)
        {
          case sk_shader_class::Vertex:   return &list_base.vs;
          case sk_shader_class::Pixel:    return &list_base.ps;
          case sk_shader_class::Geometry: return &list_base.gs;
          case sk_shader_class::Hull:     return &list_base.hs;
          case sk_shader_class::Domain:   return &list_base.ds;
          case sk_shader_class::Compute:  return &list_base.cs;
        }

        assert (false);

        return nullptr;
      };

  shader_class_imp_s*
    list = GetShaderList (shader_type);

  auto GetShaderTracker =
    [](sk_shader_class& type) ->
      shader_tracking_s*
      {
        switch (type)
        {
          case sk_shader_class::Vertex:   return &tracked_vs;
          case sk_shader_class::Pixel:    return &tracked_ps;
          case sk_shader_class::Geometry: return &tracked_gs;
          case sk_shader_class::Hull:     return &tracked_hs;
          case sk_shader_class::Domain:   return &tracked_ds;
          case sk_shader_class::Compute:  return &tracked_cs;
        }

        assert (false);

        return nullptr;
      };

  shader_tracking_s*
    tracker = GetShaderTracker (shader_type);

  auto GetShaderVector =
    [](sk_shader_class& type) ->
      std::vector <uint32_t>
      {
        std::vector <uint32_t> vec;
        vec.reserve (256);

        switch (type)
        {
          case sk_shader_class::Vertex:
          {
            for (auto const& vertex_shader : SK_D3D11_Shaders.vertex.newest)
            {
              if (SK_D3D11_Shaders.vertex.descs [vertex_shader.first].usage.last_frame >= SK_GetFramesDrawn () - 5)
              {
                // Ignore ImGui / CEGUI shaders
                if ( vertex_shader.first != 0xb42ede74 &&
                     vertex_shader.first != 0x1f8c62dc )
                {
                  vec.emplace_back (vertex_shader.first);
                }
              }
            }
          } break;

          case sk_shader_class::Pixel:
          {
            for (auto const& pixel_shader : SK_D3D11_Shaders.pixel.newest)
            {
              if (SK_D3D11_Shaders.pixel.descs [pixel_shader.first].usage.last_frame >= SK_GetFramesDrawn () - 5)
              {
                // Ignore ImGui / CEGUI shaders
                if ( pixel_shader.first != 0xd3af3aa0 &&
                     pixel_shader.first != 0xb04a90ba )
                {
                  vec.emplace_back (pixel_shader.first);
                }
              }
            }
          } break;

          case sk_shader_class::Geometry:
          {
            for (auto const& geometry_shader : SK_D3D11_Shaders.geometry.newest)
            {
              if (SK_D3D11_Shaders.geometry.descs [geometry_shader.first].usage.last_frame >= SK_GetFramesDrawn () - 5)
              {
                vec.emplace_back (geometry_shader.first);
              }
            }
          } break;

          case sk_shader_class::Hull:
          {
            for (auto const& hull_shader : SK_D3D11_Shaders.hull.newest)
            {
              if (SK_D3D11_Shaders.hull.descs [hull_shader.first].usage.last_frame >= SK_GetFramesDrawn () - 5)
              {
                vec.emplace_back (hull_shader.first);
              }
            }
          } break;

          case sk_shader_class::Domain:
          {
            for (auto const& domain_shader : SK_D3D11_Shaders.domain.newest)
            {
              if (SK_D3D11_Shaders.domain.descs [domain_shader.first].usage.last_frame >= SK_GetFramesDrawn () - 5)
              {
                vec.emplace_back (domain_shader.first);
              }
            }
          } break;

          case sk_shader_class::Compute:
          {
            for (auto const& compute_shader : SK_D3D11_Shaders.compute.newest)
            {
              if (SK_D3D11_Shaders.compute.descs [compute_shader.first].usage.last_frame >= SK_GetFramesDrawn () - 5)
              {
                vec.emplace_back (compute_shader.first);
              }
            }
          } break;
        }

        assert (false);

        return vec;
      };

  std::vector <uint32_t>
    shaders   ( GetShaderVector (shader_type) );

  auto GetShaderDisasm =
    [](sk_shader_class& type) ->
      std::unordered_map <uint32_t, shader_disasm_s>*
      {
        switch (type)
        {
          case sk_shader_class::Vertex:   return &vs_disassembly;
          default:
          case sk_shader_class::Pixel:    return &ps_disassembly;
          case sk_shader_class::Geometry: return &gs_disassembly;
          case sk_shader_class::Hull:     return &hs_disassembly;
          case sk_shader_class::Domain:   return &ds_disassembly;
          case sk_shader_class::Compute:  return &cs_disassembly;
        }
      };

  std::unordered_map <uint32_t, shader_disasm_s>*
    disassembly = GetShaderDisasm (shader_type);

  auto GetShaderWord =
    [](sk_shader_class& type) ->
      const char*
      {
        switch (type)
        {
          case sk_shader_class::Vertex:   return "Vertex";
          case sk_shader_class::Pixel:    return "Pixel";
          case sk_shader_class::Geometry: return "Geometry";
          case sk_shader_class::Hull:     return "Hull";
          case sk_shader_class::Domain:   return "Domain";
          case sk_shader_class::Compute:  return "Compute";
          default:                        return "Unknown";
        }
      };

  const char*
    szShaderWord = GetShaderWord (shader_type);

  if (list->dirty)
  {
        list->sel = -1;
    int idx    =  0;
        list->contents.clear ();

    // The underlying list is unsorted for speed, but that's not at all
    //   intuitive to humans, so sort the thing when we have the RT view open.
    std::sort ( shaders.begin (),
                shaders.end   () );



    for ( auto it : shaders )
    {
      char szDesc [16] = { };

      sprintf (szDesc, "%08lx", it);

      list->contents.emplace_back (szDesc);

      if ((uint32_t)it == list->last_sel)
      {
        list->sel = idx;
        //tbf::RenderFix::tracked_rt.tracking_tex = render_textures [sel];
        tracker->crc32c = it;
      }

      ++idx;
    }
  }

  if (ImGui::IsMouseHoveringRect (list->last_min, list->last_max))
  {
         if (ImGui::GetIO ().KeysDown [VK_OEM_4] && ImGui::GetIO ().KeysDownDuration [VK_OEM_4] == 0.0f) { list->sel--;  ImGui::GetIO ().WantCaptureKeyboard = true; }
    else if (ImGui::GetIO ().KeysDown [VK_OEM_6] && ImGui::GetIO ().KeysDownDuration [VK_OEM_6] == 0.0f) { list->sel++;  ImGui::GetIO ().WantCaptureKeyboard = true; }
  }

  ImGui::PushStyleVar   (ImGuiStyleVar_ChildWindowRounding, 0.0f);
  ImGui::PushStyleColor (ImGuiCol_Border, ImVec4 (0.9f, 0.7f, 0.5f, 1.0f));

  ImGui::BeginChild ( szShaderWord,
                      ImVec2 ( font_size * 7.0f, std::max (font_size * 15.0f, list->last_ht)),
                        true, ImGuiWindowFlags_AlwaysAutoResize );

  if (ImGui::IsWindowHovered ())
  {
    can_scroll = false;

    ImGui::BeginTooltip ();
    ImGui::TextColored  (ImVec4 (0.9f, 0.6f, 0.2f, 1.0f), "You can cancel all render passes using the selected %s shader to disable an effect", szShaderWord);
    ImGui::Separator    ();
    ImGui::BulletText   ("Press [ while the mouse is hovering this list to select the previous shader");
    ImGui::BulletText   ("Press ] while the mouse is hovering this list to select the next shader");
    ImGui::EndTooltip   ();

         if (ImGui::GetIO ().KeysDown [VK_OEM_4] && ImGui::GetIO ().KeysDownDuration [VK_OEM_4] == 0.0f) { list->sel--;  ImGui::GetIO ().WantCaptureKeyboard = true; }
    else if (ImGui::GetIO ().KeysDown [VK_OEM_6] && ImGui::GetIO ().KeysDownDuration [VK_OEM_6] == 0.0f) { list->sel++;  ImGui::GetIO ().WantCaptureKeyboard = true; }
  }

  if (shaders.size ())
  {
    struct {
      int  last_sel    = 0;
      bool sel_changed = false;
    } static shader_state [3];

    int&  last_sel    = shader_state [(int)shader_type].last_sel;
    bool& sel_changed = shader_state [(int)shader_type].sel_changed;

    if (list->sel != last_sel)
      sel_changed = true;

    last_sel = list->sel;

    for ( UINT line = 0; line < shaders.size (); line++ )
    {
      if (line == list->sel)
      {
        bool selected    = true;

        ImGui::Selectable (list->contents [line].c_str (), &selected);

        if (sel_changed)
        {
          ImGui::SetScrollHere (0.5f);

          sel_changed     = false;
          list->last_sel  = (uint32_t)shaders [list->sel];
          tracker->crc32c = (uint32_t)shaders [list->sel];
        }
      }

      else
      {
        bool selected    = false;

        if (ImGui::Selectable (list->contents [line].c_str (), &selected))
        {
          sel_changed     = true;
          list->sel       =  line;
          list->last_sel  = (uint32_t)shaders [list->sel];
          tracker->crc32c = (uint32_t)shaders [list->sel];
        }
      }
    }

    CComPtr <ID3DBlob> pDisasm = nullptr;

    HRESULT hr = E_FAIL;

    if (tracker->crc32c != 0)
    {
      switch (shader_type)
      {
        case sk_shader_class::Vertex:
            hr = D3DDisassemble ( SK_D3D11_Shaders.vertex.descs [tracker->crc32c].bytecode.data (), SK_D3D11_Shaders.vertex.descs [tracker->crc32c].bytecode.size (),
                                    D3D_DISASM_ENABLE_DEFAULT_VALUE_PRINTS, "", &pDisasm);
          break;

        case sk_shader_class::Pixel:
            hr = D3DDisassemble ( SK_D3D11_Shaders.pixel.descs [tracker->crc32c].bytecode.data (), SK_D3D11_Shaders.pixel.descs [tracker->crc32c].bytecode.size (),
                                    D3D_DISASM_ENABLE_DEFAULT_VALUE_PRINTS, "", &pDisasm);
          break;

        case sk_shader_class::Geometry:
            hr = D3DDisassemble ( SK_D3D11_Shaders.geometry.descs [tracker->crc32c].bytecode.data (), SK_D3D11_Shaders.geometry.descs [tracker->crc32c].bytecode.size (), 
                                    D3D_DISASM_ENABLE_DEFAULT_VALUE_PRINTS, "", &pDisasm);
          break;

        case sk_shader_class::Hull:
            hr = D3DDisassemble ( SK_D3D11_Shaders.hull.descs [tracker->crc32c].bytecode.data (), SK_D3D11_Shaders.hull.descs [tracker->crc32c].bytecode.size (), 
                                    D3D_DISASM_ENABLE_DEFAULT_VALUE_PRINTS, "", &pDisasm);
          break;

        case sk_shader_class::Domain:
            hr = D3DDisassemble ( SK_D3D11_Shaders.domain.descs [tracker->crc32c].bytecode.data (), SK_D3D11_Shaders.domain.descs [tracker->crc32c].bytecode.size (), 
                                    D3D_DISASM_ENABLE_DEFAULT_VALUE_PRINTS, "", &pDisasm);
          break;

        case sk_shader_class::Compute:
            hr = D3DDisassemble ( SK_D3D11_Shaders.compute.descs [tracker->crc32c].bytecode.data (), SK_D3D11_Shaders.compute.descs [tracker->crc32c].bytecode.size (), 
                                    D3D_DISASM_ENABLE_DEFAULT_VALUE_PRINTS, "", &pDisasm);
          break;
      }

      if (SUCCEEDED (hr) && strlen ((const char *)pDisasm->GetBufferPointer ()))
      {
        char* szDisasm = _strdup ((const char *)pDisasm->GetBufferPointer ());

        char* comments_end  =                strstr (szDisasm,          "\nvs");
        if (! comments_end)
          comments_end      =                strstr (szDisasm,          "\nps");
        if (! comments_end)
          comments_end      =                strstr (szDisasm,          "\ngs");
        if (! comments_end)
          comments_end      =                strstr (szDisasm,          "\nhs");
        if (! comments_end)
          comments_end      =                strstr (szDisasm,          "\nds");
        if (! comments_end)
          comments_end      =                strstr (szDisasm,          "\ncs");
        char* footer_begins = comments_end ? strstr (comments_end + 1, "\n//") : nullptr;

        if (comments_end)  *comments_end  = '\0'; else (comments_end  = "  ");
        if (footer_begins) *footer_begins = '\0'; else (footer_begins = "  ");

        disassembly->emplace ( tracker->crc32c, shader_disasm_s {
                                                  szDisasm,
                                                    comments_end + 1,
                                                      footer_begins + 1
                                                }
                            );

        free (szDisasm);
      }
    }
  }

  ImGui::EndChild      ();
  ImGui::PopStyleColor ();

  ImGui::SameLine      ();
  ImGui::BeginGroup    ();

  if (ImGui::IsItemHoveredRect ()) {
         if (ImGui::GetIO ().KeysDownDuration [VK_OEM_4] == 0.0f) list->sel--;
    else if (ImGui::GetIO ().KeysDownDuration [VK_OEM_6] == 0.0f) list->sel++;
  }

  if (tracker->crc32c != 0x00)
  {
    ImGui::BeginGroup ();
    switch (shader_type)
    {
      case sk_shader_class::Vertex:   ImGui::Checkbox ( "Cancel Draws Using Selected Vertex Shader",       &tracker->cancel_draws ); break;
      case sk_shader_class::Pixel:    ImGui::Checkbox ( "Cancel Draws Using Selected Pixel Shader",        &tracker->cancel_draws ); break;
      case sk_shader_class::Geometry: ImGui::Checkbox ( "Cancel Draws Using Selected Geometry Shader",     &tracker->cancel_draws ); break;
      case sk_shader_class::Hull:     ImGui::Checkbox ( "Cancel Draws Using Selected Hull Shader",         &tracker->cancel_draws ); break;
      case sk_shader_class::Domain:   ImGui::Checkbox ( "Cancel Draws Using Selected Domain Shader",       &tracker->cancel_draws ); break;
      case sk_shader_class::Compute:  ImGui::Checkbox ( "Cancel Dispatches Using Selected Compute Shader", &tracker->cancel_draws ); break;
    }
    ImGui::SameLine ( );


    int used_textures = 0;

    if (tracker->used_views.size ())
    {
      for ( auto it : tracker->used_views )
      {
        D3D11_SHADER_RESOURCE_VIEW_DESC rsv_desc;

        it->GetDesc (&rsv_desc);

        if (rsv_desc.ViewDimension == D3D_SRV_DIMENSION_TEXTURE2D)
        {
          ++used_textures;
        }
      }
    }


    if (shader_type != sk_shader_class::Compute)
    {
      if (tracker->cancel_draws)
        ImGui::TextDisabled ("%lu Skipped Draw%sLast Frame (%lu textures)", tracker->num_draws, tracker->num_draws != 1 ? "s " : " ", used_textures );
      else
        ImGui::TextDisabled ("%lu Draw%sLast Frame         (%lu textures)", tracker->num_draws, tracker->num_draws != 1 ? "s " : " ", used_textures );
    }

    else
    {
      if (tracker->cancel_draws)
        ImGui::TextDisabled ("%lu Skipped Dispatch%sLast Frame (%lu textures)", tracker->num_draws, tracker->num_draws != 1 ? "es " : " ", used_textures );
      else
        ImGui::TextDisabled ("%lu Dispatch%sLast Frame         (%lu textures)", tracker->num_draws, tracker->num_draws != 1 ? "es " : " ", used_textures );
    }

    ImGui::Separator      ();
    ImGui::EndGroup       ();

    if (ImGui::IsItemHoveredRect () && tracker->used_views.size ())
    {
      ImGui::BeginTooltip ();
    
      DXGI_FORMAT fmt = DXGI_FORMAT_UNKNOWN;
    
      for ( auto it : tracker->used_views )
      {
        D3D11_SHADER_RESOURCE_VIEW_DESC rsv_desc;

        it->GetDesc (&rsv_desc);

        if (rsv_desc.ViewDimension == D3D_SRV_DIMENSION_TEXTURE2D)
        {
          CComPtr <ID3D11Resource>  pRes = nullptr;
          CComPtr <ID3D11Texture2D> pTex = nullptr;

          it->GetResource (&pRes);

          if (pRes && SUCCEEDED (pRes->QueryInterface <ID3D11Texture2D> (&pTex)) && pTex)
          {
            D3D11_TEXTURE2D_DESC desc;

            pTex->GetDesc (&desc);

            fmt = desc.Format;

            if (desc.Height > 0 && desc.Width > 0)
            {
              ImGui::Image ( it,         ImVec2  ( std::max (64.0f, (float)desc.Width / 16.0f),
        ((float)desc.Height / (float)desc.Width) * std::max (64.0f, (float)desc.Width / 16.0f) ),
                                         ImVec2  (0,0),             ImVec2  (1,1),
                                         ImColor (255,255,255,255), ImColor (242,242,13,255) );
            }

            ImGui::SameLine ( );

            ImGui::BeginGroup ();
            ImGui::Text       ("Texture: %08lx", pTex);
            ImGui::Text       ("Format:  %ws",   SK_DXGI_FormatToStr (fmt).c_str ());
            ImGui::EndGroup   ();
          }
        }
      }
    
      ImGui::EndTooltip ();
    }

    ImGui::PushFont (ImGui::GetIO ().Fonts->Fonts [1]); // Fixed-width font

    ImGui::PushStyleColor (ImGuiCol_Text, ImVec4 (0.80f, 0.80f, 1.0f, 1.0f));
    ImGui::TextWrapped    ((*disassembly) [tracker->crc32c].header.c_str ());
    
    ImGui::SameLine       ();
    ImGui::BeginGroup     ();
    ImGui::TreePush       ("");
    ImGui::Spacing        (); ImGui::Spacing ();
    ImGui::PushStyleColor (ImGuiCol_Text, ImVec4 (0.666f, 0.666f, 0.666f, 1.0f));
    
    char szName    [192] = { '\0' };
    char szOrdinal [64 ] = { '\0' };
    char szOrdEl   [ 96] = { '\0' };
    
    int  el = 0;
    
    ImGui::PushItemWidth (font_size * 25);
    
#if 0
    for ( auto&& it : tracker->constants )
    {
      if (it.struct_members.size ())
      {
        ImGui::PushStyleColor (ImGuiCol_Text, ImVec4 (0.9f, 0.1f, 0.7f, 1.0f));
        ImGui::Text           (it.Name);
        ImGui::PopStyleColor  ();
    
        for ( auto&& it2 : it.struct_members )
        {
          snprintf ( szOrdinal, 64, " (%c%-3lu) ",
                        it2.RegisterSet != D3DXRS_SAMPLER ? 'c' : 's',
                          it2.RegisterIndex );
          snprintf ( szOrdEl,  96,  "%s::%lu %c", // Uniquely identify parameters that share registers
                       szOrdinal, el++, shader_type == tbf_shader_class::Pixel ? 'p' : 'v' );
          snprintf ( szName, 192, "[%s] %-24s :%s",
                       shader_type == tbf_shader_class::Pixel ? "ps" :
                                                                "vs",
                         it2.Name, szOrdinal );
    
          if (it2.Type == D3DXPT_FLOAT && it2.Class == D3DXPC_VECTOR)
          {
            ImGui::Checkbox    (szName,  &it2.Override); ImGui::SameLine ();
            ImGui::InputFloat4 (szOrdEl,  it2.Data);
          }
          else {
            ImGui::TreePush (""); ImGui::TextColored (ImVec4 (0.45f, 0.75f, 0.45f, 1.0f), szName); ImGui::TreePop ();
          }
        }
    
        ImGui::Separator ();
      }
    
      else
      {
        snprintf ( szOrdinal, 64, " (%c%-3lu) ",
                     it.RegisterSet != D3DXRS_SAMPLER ? 'c' : 's',
                        it.RegisterIndex );
        snprintf ( szOrdEl,  96,  "%s::%lu %c", // Uniquely identify parameters that share registers
                       szOrdinal, el++, shader_type == tbf_shader_class::Pixel ? 'p' : 'v' );
        snprintf ( szName, 192, "[%s] %-24s :%s",
                     shader_type == tbf_shader_class::Pixel ? "ps" :
                                                              "vs",
                         it.Name, szOrdinal );
    
        if (it.Type == D3DXPT_FLOAT && it.Class == D3DXPC_VECTOR)
        {
          ImGui::Checkbox    (szName,  &it.Override); ImGui::SameLine ();
          ImGui::InputFloat4 (szOrdEl,  it.Data);
        } else {
          ImGui::TreePush (""); ImGui::TextColored (ImVec4 (0.45f, 0.75f, 0.45f, 1.0f), szName); ImGui::TreePop ();
        }
      }
    }
#endif
    ImGui::PopItemWidth ();
    ImGui::TreePop      ();
    ImGui::EndGroup     ();
    
    ImGui::Separator      ();
    
    ImGui::PushStyleColor (ImGuiCol_Text, ImVec4 (0.99f, 0.99f, 0.01f, 1.0f));
    ImGui::TextWrapped    ((*disassembly) [tracker->crc32c].code.c_str ());
    
    ImGui::Separator      ();
    
    ImGui::PushStyleColor (ImGuiCol_Text, ImVec4 (0.5f, 0.95f, 0.5f, 1.0f));
    ImGui::TextWrapped    ((*disassembly) [tracker->crc32c].footer.c_str ());

    ImGui::PopFont        ();

    ImGui::PopStyleColor (4);
  }
  else
    tracker->cancel_draws = false;

  ImGui::EndGroup      ();

  list->last_ht    = ImGui::GetItemRectSize ().y;

  list->last_min   = ImGui::GetItemRectMin ();
  list->last_max   = ImGui::GetItemRectMax ();

  ImGui::PopStyleVar   ();
  ImGui::EndGroup      ();
}

void
SK_D3D11_EndFrame (void)
{
  tracked_vs.clear ();
  tracked_ps.clear ();
  tracked_gs.clear ();
  tracked_hs.clear ();
  tracked_ds.clear ();
  tracked_cs.clear ();

  SK_D3D11_Shaders.vertex.changes_last_frame   = 0;
  SK_D3D11_Shaders.pixel.changes_last_frame    = 0;
  SK_D3D11_Shaders.geometry.changes_last_frame = 0;
  SK_D3D11_Shaders.hull.changes_last_frame     = 0;
  SK_D3D11_Shaders.domain.changes_last_frame   = 0;
  SK_D3D11_Shaders.compute.changes_last_frame  = 0;
}

bool
SK_D3D11_ShaderModDlg (void)
{
  const float font_size = ImGui::GetFont ()->FontSize * ImGui::GetIO ().FontGlobalScale;

  bool show_dlg = true;

  ImGui::SetNextWindowSizeConstraints ( ImVec2 (256.0f, 384.0f), ImVec2 ( ImGui::GetIO ().DisplaySize.x * 0.75f, ImGui::GetIO ().DisplaySize.y * 0.75f ) );

  ImGui::Begin ( "D3D11 Shader Mod Toolkit",
                   &show_dlg,
                     ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_ShowBorders );

  bool can_scroll = ImGui::IsWindowFocused () && ImGui::IsMouseHoveringRect ( ImVec2 (ImGui::GetWindowPos ().x,                             ImGui::GetWindowPos ().y),
                                                                              ImVec2 (ImGui::GetWindowPos ().x + ImGui::GetWindowSize ().x, ImGui::GetWindowPos ().y + ImGui::GetWindowSize ().y) );

  ImGui::PushItemWidth (ImGui::GetWindowWidth () * 0.666f);

  if (ImGui::CollapsingHeader ("Live Shader View", ImGuiTreeNodeFlags_DefaultOpen))
  {
    ImGui::TreePush ("");

    if (SK_D3D11_Shaders.vertex.changes_last_frame > 0 && ImGui::CollapsingHeader ("Vertex Shaders"))
      SK_LiveShaderClassView (sk_shader_class::Vertex, can_scroll);

    if (SK_D3D11_Shaders.pixel.changes_last_frame > 0 && ImGui::CollapsingHeader ("Pixel Shaders"))
      SK_LiveShaderClassView (sk_shader_class::Pixel, can_scroll);

    if (SK_D3D11_Shaders.geometry.changes_last_frame > 0 && ImGui::CollapsingHeader ("Geometry Shaders"))
      SK_LiveShaderClassView (sk_shader_class::Geometry, can_scroll);

    if (SK_D3D11_Shaders.hull.changes_last_frame > 0 && ImGui::CollapsingHeader ("Hull Shaders"))
      SK_LiveShaderClassView (sk_shader_class::Hull, can_scroll);

    if (SK_D3D11_Shaders.domain.changes_last_frame > 0 && ImGui::CollapsingHeader ("Domain Shaders"))
      SK_LiveShaderClassView (sk_shader_class::Domain, can_scroll);

    if (SK_D3D11_Shaders.compute.changes_last_frame > 0 && ImGui::CollapsingHeader ("Compute Shaders"))
      SK_LiveShaderClassView (sk_shader_class::Compute, can_scroll);

    ImGui::TreePop ();
  }

  ImGui::End ();

  return show_dlg;
}